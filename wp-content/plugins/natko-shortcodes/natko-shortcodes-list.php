<?php 

///////////////////////////////////////////////////
// Columns
///////////////////////////////////////////////////

// 2 columns (left)
if (!function_exists('natko_two_col')) {
	function natko_two_col($atts, $content = null) {
		return '<div class="two_col">'.$content.'</div>';
	}

	add_shortcode('two_col', 'natko_two_col');
}

// 2 columns (right)
if (!function_exists('natko_two_col_last')) {
	function natko_two_col_last($atts, $content = null) {
		return '<div class="two_col last">'.$content.'</div><div class = "clear"></div>';
	}

	add_shortcode('two_col_last', 'natko_two_col_last');
}

// 3 columns (two on the left side)
if (!function_exists('natko_three_col')) {
	function natko_three_col($atts, $content = null) {
		return '<div class="three_col">'.$content.'</div>';
	}

	add_shortcode('three_col', 'natko_three_col');
}

// 3 columns (last one on the right)
if (!function_exists('natko_three_col_last')) {
	function natko_three_col_last($atts, $content = null) {
		return '<div class="three_col last">'.$content.'</div><div class = "clear"></div>';
	}

	add_shortcode('three_col_last', 'natko_three_col_last');
}

///////////////////////////////////////////////////
// Accordion
///////////////////////////////////////////////////

if (!function_exists('natko_accordion')) {
	function natko_accordion($atts, $content = null) {
		extract(shortcode_atts(array(
			'title' => '',
		), $atts));
		return '<div class="accordion-wrapper"><h4>'.$title.'</h4><div class="accordion-content">'.do_shortcode($content).'</div></div>';
	}

	add_shortcode('accordion', 'natko_accordion');
}

///////////////////////////////////////////////////
// Notification boxes
///////////////////////////////////////////////////

// Yellow (alert)
if (!function_exists('natko_yellow_box')) {
	function natko_yellow_box($atts, $content = null) {
		return '<div class="notification-box yellow">'.$content.'</div>';
	}

	add_shortcode('yellow_box', 'natko_yellow_box');
}

// Red (error)
if (!function_exists('natko_red_box')) {
	function natko_red_box($atts, $content = null) {
		return '<div class="notification-box red">'.$content.'</div>';
	}

	add_shortcode('red_box', 'natko_red_box');
}

// Blue (info)
if (!function_exists('natko_blue_box')) {
	function natko_blue_box($atts, $content = null) {
		return '<div class="notification-box blue">'.$content.'</div>';
	}

	add_shortcode('blue_box', 'natko_blue_box');
}

// Green (success)
if (!function_exists('natko_green_box')) {
	function natko_green_box($atts, $content = null) {
		return '<div class="notification-box green">'.$content.'</div>';
	}

	add_shortcode('green_box', 'natko_green_box');
}

// Gray (simple standout message)
if (!function_exists('natko_gray_box')) {
	function natko_gray_box($atts, $content = null) {
		return '<div class="notification-box gray">'.$content.'</div>';
	}

	add_shortcode('gray_box', 'natko_gray_box');
}

///////////////////////////////////////////////////
// Google Maps
///////////////////////////////////////////////////

if (!function_exists('natko_google_maps')) {
	function natko_google_maps($atts, $content = null) {
		extract(shortcode_atts(array(
			'width' => '1200',
			'height' => '400',
		), $atts));
		return '<div class="google-maps"><iframe width="'.$width.'" height="'.$height.'" src="'.$content.'&output=embed" ></iframe></div>';
	}

	add_shortcode('google_map', 'natko_google_maps');
}

///////////////////////////////////////////////////
// Splitters
///////////////////////////////////////////////////

// Splitter Line
if (!function_exists('natko_splitter_line')) {
	function natko_splitter_line($atts, $content = null) {
		return '<div class="splitter-line">'.$content.'</div>';
	}

	add_shortcode('splitter-line', 'natko_splitter_line');
}

// Splitter Dashed
if (!function_exists('natko_splitter_dashed')) {
	function natko_splitter_dashed($atts, $content = null) {
		return '<div class="splitter-dashed">'.$content.'</div>';
	}

	add_shortcode('splitter-dashed', 'natko_splitter_dashed');
}

?>