<?php 
/*
Plugin Name: Natko Shortcodes
Plugin URI: http://natko.com
Description: Shortcodes for all themes designed and coded by Natko.
Author: Natko Hasić
Author URI: http://natko.com
Version: 1.0
*/

if (!function_exists( 'add_action' )){
	exit;
}

class NatkoShortcodes{

	/////////////////////////////////////////////////////////////
	// Include all scripts and style
	/////////////////////////////////////////////////////////////

	public function __construct() {

		// Include the shortcodes
		require_once( DIRNAME(__FILE__) . '/natko-shortcodes-list.php' );

		// Load plugin text domain
		load_plugin_textdomain( 'natko-shortcodes', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		// Add shortcode TinyMCE button
		add_action('init', array(&$this, 'natko_admin_init'));

		// Include scripts
		add_action('admin_enqueue_scripts', array(&$this, 'natko_shortcodes_admin_scripts'));

	}

	/////////////////////////////////////////////////////////////
	// Check the user permissions and add the button
	/////////////////////////////////////////////////////////////

	public function natko_admin_init() {
		if (!current_user_can('edit_posts') && !current_user_can('edit_pages')){
			return;
		}

		if (get_user_option('rich_editing') == 'true' && is_admin()){
			add_filter( 'mce_external_plugins', array(&$this, 'add_rich_plugins') );
			add_filter( 'mce_buttons', array(&$this, 'register_rich_buttons') );
		}
	}

	/////////////////////////////////////////////////////////////
	// Call the JS file that handles the select box
	/////////////////////////////////////////////////////////////

	public function add_rich_plugins($plugin_array){
		$plugin_array['NatkoShortcodes'] = plugin_dir_url(__FILE__) . 'script/editor-plugin.js';
		return $plugin_array;
	}

	public function register_rich_buttons($buttons){
		array_push( $buttons, '|', 'natko_button' );
		return $buttons;
	}

	/////////////////////////////////////////////////////////////
	// Admin hooks
	/////////////////////////////////////////////////////////////

	public function natko_shortcodes_admin_scripts(){
		wp_localize_script( 'jquery', 'NatkoShortcodes', 
			array(
				'plugin_url' => plugin_dir_url(__FILE__),
				'two_col'=> __('2 Columns', 'natko-shortcodes'),
				'three_col'=> __('3 Column', 'natko-shortcodes'),
				'accordion'=> __('Accordion', 'natko-shortcodes'),
				'red_box'=> __('Red Box', 'natko-shortcodes'),
				'blue_box'=> __('Blue Box', 'natko-shortcodes'),
				'yellow_box'=> __('Yellow Box', 'natko-shortcodes'),
				'green_box'=> __('Green Box', 'natko-shortcodes'),
				'gray_box'=> __('Gray Box', 'natko-shortcodes'),
				'google_map'=> __('Google Map', 'natko-shortcodes'),
				'splitter_line'=> __('Splitter Line', 'natko-shortcodes'),
				'splitter_dashed'=> __('Splitter Dashed', 'natko-shortcodes'),
				'left_column_content'=> __('Write the left column content here.', 'natko-shortcodes'),
				'middle_column_content'=> __('Write the middle column content here.', 'natko-shortcodes'),
				'right_column_content'=> __('Write the right column content here.', 'natko-shortcodes'),
				'accordion_title_content'=> __('Write the accordion title here.', 'natko-shortcodes'),
				'accordion_content'=> __('Write the accordion content here.', 'natko-shortcodes'),
				'box_content' => __('Write the box content here.', 'natko-shortcodes'),
				'google_map_content' => __('Paste the maps link here.', 'natko-shortcodes'),
			)
		);
	}

}

new NatkoShortcodes();

function natko_shortcodes_exists_check(){
	return;
}

?>