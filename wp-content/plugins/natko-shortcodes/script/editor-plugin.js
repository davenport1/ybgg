//////////////////////////////////////////////////////
// NatkoShortcodes editor plugin
//////////////////////////////////////////////////////

(function () {

	tinymce.create("tinymce.plugins.NatkoShortcodes", {
	
		createControl: function ( btn, e ) {
			if ( btn == "natko_button" ) {	
				
				var a = this;
				
				// Create the button
				var btn = e.createSplitButton("natko_button", {
					title: "Insert Shortcode",
					image: NatkoShortcodes.plugin_url +"/images/shortcodes-icon.png",
					icons: false
				});
				
				// Add a select box
				btn.onRenderMenu.add(function (c, b) {	

					a.addImmediate( b, NatkoShortcodes.two_col, "[two_col]"+NatkoShortcodes.left_column_content+"[/two_col]<br>[two_col_last]"+NatkoShortcodes.right_column_content+"[/two_col_last]<br>" );
					a.addImmediate( b, NatkoShortcodes.three_col, "[three_col]"+NatkoShortcodes.left_column_content+"[/three_col]<br>[three_col]"+NatkoShortcodes.middle_column_content+"[/three_col]<br>[three_col_last]"+NatkoShortcodes.right_column_content+"[/three_col_last]<br>" );
					a.addImmediate( b, NatkoShortcodes.accordion, "<br>[accordion title=\""+NatkoShortcodes.accordion_title_content+"\"]"+NatkoShortcodes.accordion_content+"[/accordion]" );
					a.addImmediate( b, NatkoShortcodes.red_box, "<br>[red_box]"+NatkoShortcodes.box_content+"[/red_box]" );
					a.addImmediate( b, NatkoShortcodes.blue_box, "<br>[blue_box]"+NatkoShortcodes.box_content+"[/blue_box]" );
					a.addImmediate( b, NatkoShortcodes.yellow_box, "<br>[yellow_box]"+NatkoShortcodes.box_content+"[/yellow_box]" );
					a.addImmediate( b, NatkoShortcodes.green_box, "<br>[green_box]"+NatkoShortcodes.box_content+"[/green_box]" );
					a.addImmediate( b, NatkoShortcodes.gray_box, "<br>[gray_box]"+NatkoShortcodes.box_content+"[/gray_box]" );
					a.addImmediate( b, NatkoShortcodes.google_map, "[google_map]"+NatkoShortcodes.google_map_content+"[/google_map]" );
					a.addImmediate( b, NatkoShortcodes.splitter_line, "<br>[splitter-line][/splitter-line]<br>" );
					a.addImmediate( b, NatkoShortcodes.splitter_dashed, "<br>[splitter-dashed][/splitter-dashed]<br>" );

				});

				return btn;
			}
			
			return null;
		},

		//Insert shortcode into content
		addImmediate: function ( ed, title, sc) {
			ed.add({
				title: title,
				onclick: function () {
					tinyMCE.activeEditor.execCommand( "mceInsertContent", false, sc )
				}
			})
		},

		getInfo: function () {
			return {
				longname : "NatkoShortcodes",
				author : 'Natko',
				authorurl : 'http://natko.com/',
				infourl : 'http://natko.com/',
				version : "1.0"
			};
		}
	});
	
	// Add the plugin
	tinymce.PluginManager.add("NatkoShortcodes", tinymce.plugins.NatkoShortcodes);

})();