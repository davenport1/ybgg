<?php 
/*
Plugin Name: Portfolio Plus
Plugin URI: http://natko.com
Description: Portfolio post type plugin.
Author: Natko Hasić
Author URI: http://natko.com
Version: 1.1
*/

class PortfolioPlus{

	/////////////////////////////////////////////////////////////
	// Include all scripts and style
	/////////////////////////////////////////////////////////////

	public function __construct() {

		// Load plugin text domain
		load_plugin_textdomain( 'portfolio-plus', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		// Run when the plugin is activated
		register_activation_hook( __FILE__, array( &$this, 'pp_init_plugin' ) );

		// Add the portfolio post type and taxonomies
		add_action( 'init', array( $this, 'pp_init' ) );

		// Thumbnail support for portfolio posts
		add_theme_support( 'post-thumbnails', array( 'portfolio' ) );

		// Add metaboxes
		add_action( 'add_meta_boxes', array( $this, 'pp_add_metabox' ) );
		add_action( 'save_post', array( $this, 'pp_save_metabox' ) );

		// Add thumbnails to column view
		if (isset($_GET['post_type']) && ($_GET['post_type'] == 'portfolio')){
			add_filter( 'manage_edit-portfolio_columns', array( $this, 'pp_add_thumbnail_column'), 10, 1 );
			add_action( 'manage_posts_custom_column', array( $this, 'pp_display_thumbnail' ), 10, 1 );
		}

		// Allow filtering of posts by taxonomy in the admin view
		add_action( 'restrict_manage_posts', array( $this, 'pp_add_taxonomy_filters' ) );

		// Give the portfolio menu item a unique icon
		add_action( 'admin_head', array( $this, 'pp_menu_icon' ) );

		// Add an input field to "Settings" for the slug
		add_action( 'admin_init', array( $this, 'pp_slug_register_setting' ) );

		// Create the widget
		add_action( 'widgets_init', create_function('', 'register_widget("PortfolioPlus_Widget");'));

	}

	/////////////////////////////////////////////////////////////
	// Flush rewrite rules on init so portfolio posts don't 404
	/////////////////////////////////////////////////////////////

	public function pp_init_plugin() {
		$this->pp_init();
		flush_rewrite_rules();
	}

	/////////////////////////////////////////////////////////////
	// Register post type and taxonomies
	/////////////////////////////////////////////////////////////

	public function pp_init() {
		$this->pp_register_post_type();
		$this->pp_register_taxonomy_tag();
	}

	public function get_taxonomies() {
		return array( 'portfolio_tag' );
	}

	/////////////////////////////////////////////////////////////
	// Register the portfolio post type
	/////////////////////////////////////////////////////////////

	public function pp_register_post_type(){
		$portfolio_slug = '';
		$portfolio_slug = get_option('portfolio_plus_slug');

		if($portfolio_slug == ''){
			$portfolio_slug = __('portfolio', 'portfolio-plus');
		}

		$labels = array(
			'name' => __( 'Portfolio', 'portfolio-plus' ),
			'singular_name' => __( 'Portfolio Item', 'portfolio-plus' ),
			'add_new' => __( 'Add New Item', 'portfolio-plus' ),
			'add_new_item' => __( 'Add New Portfolio Item', 'portfolio-plus' ),
			'edit_item' => __( 'Edit Portfolio Item', 'portfolio-plus' ),
			'new_item' => __( 'Add New Portfolio Item', 'portfolio-plus' ),
			'view_item' => __( 'View Item', 'portfolio-plus' ),
			'search_items' => __( 'Search Portfolio', 'portfolio-plus' ),
			'not_found' => __( 'No portfolio items found', 'portfolio-plus' ),
			'not_found_in_trash' => __( 'No portfolio items found in trash', 'portfolio-plus' ),
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
			'capability_type' => 'post',
			'rewrite' => array( 'slug' => $portfolio_slug ),
			'menu_position' => 20,
		);

		$args = apply_filters( 'portfolioplus_args', $args );
		register_post_type( 'portfolio', $args );
	}

	/////////////////////////////////////////////////////////////
	// Register taxonomy for tags
	/////////////////////////////////////////////////////////////

	public function pp_register_taxonomy_tag() {
		$labels = array(
			'name' => __( 'Portfolio Tags', 'portfolio-plus' ),
			'singular_name' => __( 'Portfolio Tag', 'portfolio-plus' ),
			'menu_name' => __( 'Portfolio Tags', 'portfolio-plus' ),
			'edit_item' => __( 'Edit Portfolio Tag', 'portfolio-plus' ),
			'update_item' => __( 'Update Portfolio Tag', 'portfolio-plus' ),
			'add_new_item' => __( 'Add New Portfolio Tag', 'portfolio-plus' ),
			'new_item_name' => __( 'New Portfolio Tag Name', 'portfolio-plus' ),
			'parent_item' => __( 'Parent Portfolio Tag', 'portfolio-plus' ),
			'parent_item_colon' => __( 'Parent Portfolio Tag:', 'portfolio-plus' ),
			'all_items' => __( 'All Portfolio Tags', 'portfolio-plus' ),
			'search_items' => __( 'Search Portfolio Tags', 'portfolio-plus' ),
			'popular_items' => __( 'Popular Portfolio Tags', 'portfolio-plus' ),
			'separate_items_with_commas' => __( 'Separate portfolio tags with commas', 'portfolio-plus' ),
			'add_or_remove_items' => __( 'Add or remove portfolio tags', 'portfolio-plus' ),
			'choose_from_most_used' => __( 'Choose from the most used portfolio tags', 'portfolio-plus' ),
			'not_found' => __( 'No portfolio tags found.', 'portfolio-plus' ),
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_tagcloud' => true,
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'portfolio_tag' ),
			'show_admin_column' => true,
			'query_var' => true,
		);

		$args = apply_filters( 'portfolio-plus_tag_args', $args );
		register_taxonomy( 'portfolio_tag', array( 'portfolio' ), $args );
	}

	/////////////////////////////////////////////////////////////
	// Add metabox
	/////////////////////////////////////////////////////////////

	public function pp_add_metabox() {
		add_meta_box(
			'pp_portfolio_info',
			__( 'Portfolio Item Info', 'portfolio-plus' ),
			array( $this, 'render_pp_portfolio_info' ),
			'portfolio',
			'side',
			'low'
		);
	}

	public function pp_save_metabox($post_id) {

		if (!isset($_POST['pp_metabox_nonce'])){
			return $post_id;
		}

		$nonce = $_POST['pp_metabox_nonce'];

		if (!wp_verify_nonce($nonce, 'pp_metabox')){
			return $post_id;
		}

		if (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE){
			return $post_id;
		}

		if (!current_user_can( 'edit_post', $post_id )){
			return $post_id;
		}

		// Sanitize the user input.
		$portfolio_info = array();
		$portfolio_info[0] = $_POST['pp_portfolio_info_1'];
		$portfolio_info[1] = $_POST['pp_portfolio_info_2'];
		$portfolio_info[2] = $_POST['pp_portfolio_info_3'];

		// Update the meta field.
		update_post_meta( $post_id, '_pp_portfolio_meta', $portfolio_info );
	}

	public function render_pp_portfolio_info($post){

		$value = array();

		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'pp_metabox', 'pp_metabox_nonce' );

		// Use get_post_meta to retrieve an existing value from the database.
		$value = get_post_meta( $post->ID, '_pp_portfolio_meta', true );

		if(!isset($value[0])){ $value[0] = ''; }
		if(!isset($value[1])){ $value[1] = ''; }
		if(!isset($value[2])){ $value[2] = ''; }

		// Display the form, using the current value.
		echo '<p class="howto">' . __( 'Info about the project like client, year, etc. (example: "CLIENT: John Doe")', 'portfolio-plus' ) . '</p>';

		echo '<label for="pp_portfolio_info_1">' . __( 'First info field', 'portfolio-plus' ) . '</label>';
		echo '<input type="text" id="pp_portfolio_info_1" name="pp_portfolio_info_1" value="' . esc_attr( $value[0] ) . '" size="25" />';

		echo '<label for="pp_portfolio_info_2">' . __( 'Second info field', 'portfolio-plus' ) . '</label>';
		echo '<input type="text" id="pp_portfolio_info_2" name="pp_portfolio_info_2" value="' . esc_attr( $value[1] ) . '" size="25" />';

		echo '<label for="pp_portfolio_info_3">' . __( 'Third info field', 'portfolio-plus' ) . '</label>';
		echo '<input type="text" id="pp_portfolio_info_3" name="pp_portfolio_info_3" value="' . esc_attr( $value[2] ) . '" size="25" />';
	}

	/////////////////////////////////////////////////////////////
	// Add post thumbnail column in the admin section
	/////////////////////////////////////////////////////////////

	public function pp_add_thumbnail_column( $columns ){
		$column_thumbnail = array( 'thumbnail' => __( 'Thumbnail', 'portfolio-plus' ) );
		return array_slice( $columns, 0, 2, true ) + $column_thumbnail + array_slice( $columns, 1, null, true );
	}

	public function pp_display_thumbnail( $column ){
		global $post;
		switch ( $column ) {
		case 'thumbnail':
			echo get_the_post_thumbnail( $post->ID, array(35, 35) );
		break;
		}
	}

	/////////////////////////////////////////////////////////////
	// Add taxonomy filters (http://pippinsplugins.com)
	/////////////////////////////////////////////////////////////

	public function pp_add_taxonomy_filters() {
		global $typenow;

		// An array of all the taxonomies to display
		$taxonomies = $this->get_taxonomies();

		// Display the filter only on the 'portfolio' post type
		if ( 'portfolio' != $typenow ) { return; }

		foreach ( $taxonomies as $tax_slug ) {
			$current_tax_slug = isset( $_GET[$tax_slug] ) ? $_GET[$tax_slug] : false;
			$tax_obj = get_taxonomy( $tax_slug );
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms( $tax_slug );
			if ( 0 == count( $terms ) ) {
				return;
			}
			echo '<select name="' . esc_attr( $tax_slug ) . '" id="' . esc_attr( $tax_slug ) . '" class="postform">';
			echo '<option>' . esc_html( $tax_name ) .'</option>';
			foreach ( $terms as $term ) {
				printf(
					'<option value="%s"%s />%s</option>',
					esc_attr( $term->slug ),
					selected( $current_tax_slug, $term->slug ),
					esc_html( $term->name . '(' . $term->count . ')' )
				);
			}
			echo '</select>';
		}
	}

	/////////////////////////////////////////////////////////////
	// Portfolio Menu Icon
	/////////////////////////////////////////////////////////////

	public function pp_menu_icon(){
		?>
		<style>
			#adminmenu #menu-posts-portfolio.menu-icon-post div.wp-menu-image:before {
				content: "\f322";
			}
		</style>
		<?php
	}

	/////////////////////////////////////////////////////////////
	//	Add Field To "Settings" For Changing The Slug
	/////////////////////////////////////////////////////////////

	public function pp_slug_register_setting(){
		add_settings_section( 'pp_slug_id', '', array( $this, 'pp_slug_description' ), 'general' );
		register_setting( 'general', 'portfolio_plus_slug', 'esc_attr' );
		add_settings_field( 'portfolio_plus_slug', __('Portfolio Slug', 'portfolio-plus'), array( $this, 'pp_slug_settings' ), 'general', 'pp_slug_id' );
		
		$current_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(isset($_GET['settings-updated']) && strpos($current_link,'options-general.php') !== false && $_GET['settings-updated'] == 'true'){
			flush_rewrite_rules();
		}
	}

	public function pp_slug_description(){
		$pp_setting_description = '';
		echo '<p class="description">'.$pp_setting_description.'</p>';
	}

	public function pp_slug_settings($args){
		$data = get_option( 'portfolio_plus_slug', '' );
		if($data == ''){
			$data = __('portfolio', 'portfolio-plus');
		}
		echo '<code>'.home_url().'/</code><input id="pp_slug_id" class="regular-text code" style="width: 9em;" type="text" value="'.$data.'" name="portfolio_plus_slug"/><code>/'.__('sample-post', 'portfolio-plus').'</code>';
	}

}

new PortfolioPlus();


///////////////////////////////////////////////////////////////////////////////////////
// Portfolio Widget
///////////////////////////////////////////////////////////////////////////////////////

class PortfolioPlus_Widget extends WP_Widget {

	function __construct() {
		parent::WP_Widget( 'tp_portfolio_plus_widget', 'Portfolio Plus', array( 'classname' => 'widget-portfolio-plus', 'description' => __('Display your portfolio items in a widget.', 'portfolio-plus') ) );
	}

	function widget( $args, $instance ) {
		extract( $args );

		$title = '';
		$number = 5;

		if(isset($instance['title'])){ $title = $instance['title']; }
		if(isset($instance['number'])){ $number = $instance['number']; }

		// Portfolio loop
		if (post_type_exists('portfolio')){
			$portfolios = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => $number ) );
		}

		echo $before_widget;

		//////////////////////////////////////////////////
		//  Display widget title
		//////////////////////////////////////////////////

		if($title != ''){
			echo $before_title . $instance['title'] . $after_title;
		}

		//////////////////////////////////////////////////
		//  Display portfolio
		//////////////////////////////////////////////////

		if ( $portfolios->have_posts() ):
			echo '<ul class="portfolio-details portfolio-plus-list">';
			while ( $portfolios->have_posts() ) : $portfolios->the_post(); ?>
				<li>
					<?php the_post_thumbnail('large'); ?>
					<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
					<?php if(has_excerpt()){ ?>
						<h2 class="portfolio-excerpt"><?php the_excerpt(''); ?></h2>
					<?php } ?>
				</li>
			<?php endwhile;
			echo '</ul>';
		endif;

		echo $after_widget;

	}
	
	//////////////////////////////////////////////////
	//  U P D A T E   W I D G E T
	//////////////////////////////////////////////////
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$number_of_portfolios = $new_instance['number'];
		$number_of_portfolios = preg_replace('/[^0-9.]+/', '', $number_of_portfolios);
		$instance['number'] = $number_of_portfolios;

		return $instance;

	}

	//////////////////////////////////////////////////
	//   W I D G E T   S E T T I N G S
	//////////////////////////////////////////////////
	
	function form( $instance ) {

		// Set up some default widget settings
		$instance = wp_parse_args( (array) $instance );

		$instance_portfolio_plus_title = '';

		if(isset($instance['title'])){
			$instance_portfolio_plus_title = $instance['title'];
		}

		if(isset($instance['number'])){
			$instance_portfolio_plus_number = $instance['number'];
		} else {
			$instance_portfolio_plus_number = 5;
		}

		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>" class="pt-pretty-tags"><?php echo __('Title:', 'portfolio-plus') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" value="<?php echo $instance_portfolio_plus_title; ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" class="widefat" style="width:100%;">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo __('Number of portfolio items to show:', 'portfolio-plus') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'number' ); ?>" value="<?php echo $instance_portfolio_plus_number; ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" class="widefat" style="width:100%;">
		</p>
<?php

	}
} 

function portfolio_plus_exists_check(){
	return;
}

?>