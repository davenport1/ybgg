<?php 
/*
Plugin Name: Testimonials Plus
Plugin URI: http://natko.com
Description: Testimonials post type plugin.
Author: Natko Hasić
Author URI: http://natko.com
Version: 1.1
*/

class TestimonialsPlus{

	/////////////////////////////////////////////////////////////
	// Plugin constructor
	/////////////////////////////////////////////////////////////

	public function __construct() {

		// Load plugin text domain
		load_plugin_textdomain( 'testimonials-plus', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		// Run when the plugin is activated
		register_activation_hook( __FILE__, array( &$this, 'tp_init_plugin' ) );

		// Add the testimonials post type
		add_action( 'init', array( $this, 'tp_init' ) );

		// Thumbnail support for testimonials posts
		add_theme_support( 'post-thumbnails', array( 'testimonials' ) );

		// Add thumbnails to column view
		if (isset($_GET['post_type']) && ($_GET['post_type'] == 'testimonials')){
			add_filter( 'manage_edit-testimonials_columns', array( $this, 'tp_add_thumbnail_column'), 10, 1 );
			add_action( 'manage_posts_custom_column', array( $this, 'tp_display_thumbnail' ), 10, 1 );
		}

		// Give the testimonials menu item a unique icon
		add_action( 'admin_head', array( $this, 'tp_menu_icon' ) );

		// Add an input field to "Settings" for the slug
		add_action( 'admin_init', array( $this, 'tp_slug_register_setting' ) );

		// Create the widget
		add_action( 'widgets_init', create_function('', 'register_widget("TestimonialsPlus_Widget");'));

	}

	/////////////////////////////////////////////////////////////
	// Flush rewrite rules on init so testimonials posts don't 404
	/////////////////////////////////////////////////////////////

	public function tp_init_plugin(){
		$this->tp_init();
		flush_rewrite_rules();
	}

	/////////////////////////////////////////////////////////////
	// Register post type
	/////////////////////////////////////////////////////////////

	public function tp_init() {
		$this->tp_register_post_type();
	}

	/////////////////////////////////////////////////////////////
	// Register the testimonials post type
	/////////////////////////////////////////////////////////////

	public function tp_register_post_type(){
		$testimonials_slug = '';
		$testimonials_slug = get_option('testimonials_plus_slug');

		$labels = array(
			'name' => __( 'Testimonials', 'testimonials-plus' ),
			'singular_name' => __( 'Testimonial', 'testimonials-plus' ),
			'add_new' => __( 'Add New Testimonial', 'testimonials-plus' ),
			'add_new_item' => __( 'Add New Testimonial', 'testimonials-plus' ),
			'edit_item' => __( 'Edit Testimonial', 'testimonials-plus' ),
			'new_item' => __( 'Add New Testimonial', 'testimonials-plus' ),
			'view_item' => __( 'View Testimonial', 'testimonials-plus' ),
			'search_items' => __( 'Search Testimonials', 'testimonials-plus' ),
			'not_found' => __( 'No testimonials found', 'testimonials-plus' ),
			'not_found_in_trash' => __( 'No testimonials found in trash', 'testimonials-plus' ),
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'supports' => array( 'title', 'editor', 'thumbnail' ),
			'capability_type' => 'post',
			'rewrite' => array( 'slug' => $testimonials_slug ),
			'menu_position' => 30,
		);

		$args = apply_filters( 'testimonialsplus_args', $args );
		register_post_type( 'testimonials', $args );
	}

	/////////////////////////////////////////////////////////////
	// Add post thumbnail column in the admin section
	/////////////////////////////////////////////////////////////

	public function tp_add_thumbnail_column( $columns ){
		$column_thumbnail = array( 'thumbnail' => __( 'Thumbnail', 'testimonials-plus' ) );
		return array_slice( $columns, 0, 2, true ) + $column_thumbnail + array_slice( $columns, 1, null, true );
	}

	public function tp_display_thumbnail( $column ){
		global $post;
		switch ( $column ) {
		case 'thumbnail':
			echo get_the_post_thumbnail( $post->ID, array(35, 35) );
		break;
		}
	}

	/////////////////////////////////////////////////////////////
	// Testimonials Menu Icon
	/////////////////////////////////////////////////////////////

	public function tp_menu_icon(){
		?>
		<style>
			#adminmenu #menu-posts-testimonials.menu-icon-post div.wp-menu-image:before {
				content: "\f307";
			}
		</style>
		<?php
	}

	/////////////////////////////////////////////////////////////
	//	Add Field To "Settings" For Changing The Slug
	/////////////////////////////////////////////////////////////

	public function tp_slug_register_setting(){
		add_settings_section( 'tp_slug_id', '', array( $this, 'tp_slug_description' ), 'general' );
		register_setting( 'general', 'testimonials_plus_slug', 'esc_attr' );
		add_settings_field( 'testimonials_plus_slug', __('Testimonials Slug', 'testimonials-plus'), array( $this, 'tp_slug_settings' ), 'general', 'tp_slug_id' );
		
		$current_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(isset($_GET['settings-updated']) && strpos($current_link,'options-general.php') !== false && $_GET['settings-updated'] == 'true'){
			flush_rewrite_rules();
		}
	}

	public function tp_slug_description(){
		$tp_setting_description = '';
		echo '<p class="description">'.$tp_setting_description.'</p>';
	}

	public function tp_slug_settings($args){
		$data = get_option( 'testimonials_plus_slug', '' );
		if($data == ''){
			$data = __('testimonials', 'testimonials-plus');
		}
		echo '<code>'.home_url().'/</code><input id="tp_slug_id" class="regular-text code" style="width: 9em;" type="text" value="'.$data.'" name="testimonials_plus_slug"/><code>/'.__('sample-testimonial', 'testimonials-plus').'</code>';
	}

}

new TestimonialsPlus();


///////////////////////////////////////////////////////////////////////////////////////
// Testimonials Widget
///////////////////////////////////////////////////////////////////////////////////////

class TestimonialsPlus_Widget extends WP_Widget {

	function __construct() {
		parent::WP_Widget( 'tp_testimonials_plus_widget', 'Testimonials Plus', array( 'classname' => 'widget-testimonials-plus', 'description' => __('Display your client testimonials in a widget.', 'testimonials-plus') ) );
	}

	function widget( $args, $instance ) {
		extract( $args );

		$title = '';
		$number = 5;

		if(isset($instance['title'])){ $title = $instance['title']; }
		if(isset($instance['number'])){ $number = $instance['number']; }

		// Testimonials loop
		if (post_type_exists('testimonials')){
			$testimonials = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => $number ) );
		}

		echo $before_widget;

		//////////////////////////////////////////////////
		//  Display widget title
		//////////////////////////////////////////////////

		if($title != ''){
			echo $before_title . $instance['title'] . $after_title;
		}

		//////////////////////////////////////////////////
		//  Display testimonials
		//////////////////////////////////////////////////

		if ( $testimonials->have_posts() ):
			echo '<ul class="testimonials-plus-list">';
			while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
				<li>
					<?php the_post_thumbnail('thumbnail'); ?>
					<h3><?php the_title(); ?></h3>
					<div class="testimonial-plus-content">
						<?php 
							global $more;
							$temp_more = $more;
							$more = 0;
							the_content('');
							$more = $temp_more;
						?>
					</div>
				</li>
			<?php endwhile;
			echo '</ul>';
		endif;

		echo $after_widget;

	}
	
	//////////////////////////////////////////////////
	//  U P D A T E   W I D G E T
	//////////////////////////////////////////////////
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$number_of_testimonials = $new_instance['number'];
		$number_of_testimonials = preg_replace('/[^0-9.]+/', '', $number_of_testimonials);
		$instance['number'] = $number_of_testimonials;

		return $instance;

	}

	//////////////////////////////////////////////////
	//   W I D G E T   S E T T I N G S
	//////////////////////////////////////////////////
	
	function form( $instance ) {

		// Set up some default widget settings
		$instance = wp_parse_args( (array) $instance );

		$instance_testimonials_plus_title = '';

		if(isset($instance['title'])){
			$instance_testimonials_plus_title = $instance['title'];
		}

		if(isset($instance['number'])){
			$instance_testimonials_plus_number = $instance['number'];
		} else {
			$instance_testimonials_plus_number = 5;
		}

		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>" class="pt-pretty-tags"><?php echo __('Title:', 'testimonials-plus') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" value="<?php echo $instance_testimonials_plus_title; ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" class="widefat" style="width:100%;">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo __('Number of testimonials to show:', 'testimonials-plus') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'number' ); ?>" value="<?php echo $instance_testimonials_plus_number; ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" class="widefat" style="width:100%;">
		</p>
<?php

	}
} 

function testimonials_plus_exists_check(){
	return;
}

?>