(function() {
     /* Register the buttons */
     tinymce.create('tinymce.plugins.popdom_button', {
          init : function(ed, url) {
               /**
               * Inserts shortcode content
               */
               ed.addButton('button_eek', {
                    title : 'Insert Link to Show Popup',
                    image : '../wp-content/plugins/popup-domination/images/favicon.png',
                    onclick : function() {
                        ed.windowManager.open({
                            title : 'Insert Link to Show Popup',
                            file: ajaxurl + '?action=popup_domination_campaigns',
                            width: 990 + ed.getLang('themedelta.delta_width', 0),
                            height: 600 + ed.getLang('themedelta.delta_height', 0),
                            inline: 1
                        });
                    }
               });
          },
          createControl : function(n, cm) {
               return null;
          }
     });
     /* Start the buttons */
     tinymce.PluginManager.add('popdom_button_script', tinymce.plugins.popdom_button);
})();