<?php 
/*
Plugin Name: Love Post
Plugin URI: http://natko.com
Description: Mark your favorite posts, bookmark them for later without logging in.
Author: Natko Hasić
Author URI: http://natko.com
Version: 0.9
*/

class LovePost{

	/////////////////////////////////////////////////////////////
	// Include all scripts and style
	/////////////////////////////////////////////////////////////

	function __construct() {

		// Load plugin text domain
		load_plugin_textdomain( 'love-post', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		add_action( 'wp_enqueue_scripts', array( $this, 'lp_include_scripts' ) );
		add_action( 'widgets_init', create_function('', 'register_widget("LovePostPopular_Widget");'));
		add_action( 'widgets_init', create_function('', 'register_widget("LovePostUser_Widget");'));

		add_action( 'wp_ajax_lp_love_post', array( $this, 'lp_love_post' ) );
		add_action( 'wp_ajax_nopriv_lp_love_post', array( $this, 'lp_love_post' ) );

		add_action( 'wp_login', array( $this, 'lp_user_sync' ) );

		// Add shortcodes
		add_shortcode( 'lovepost', array(&$this, 'lp_user_shortcode'));
		add_shortcode( 'lovepost_popular', array(&$this, 'lp_popular_shortcode'));
		add_shortcode( 'lovepost_link', array(&$this, 'lp_link_shortcode'));

	}

	public function lp_include_scripts(){
		wp_register_script( 'lp-love-post-script' , plugins_url( 'script/love-post.js', __FILE__ ), array('jquery') );
		wp_enqueue_script( 'lp-love-post-script' );

		wp_localize_script( 'lp-love-post-script', 'lp_love_post', array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'nonce' => wp_create_nonce( 'love-post-nonce-check' ),
			)
		);
	}


	///////////////////////////////////////////////////////////////////////////////////////
	//	Love Post
	///////////////////////////////////////////////////////////////////////////////////////

	public function lp_love_post(){

		check_ajax_referer( 'love-post-nonce-check', 'security' );

		$post_id = $_POST['post_id'];
		$cookie_data_array = array();
		$post_love_string = __('You already loved this post.', 'love-post');

		if(isset($_COOKIE['love_post_' . COOKIEHASH])){
			$cookie_data = $_COOKIE['love_post_' . COOKIEHASH];
			$cookie_data_array = explode( ',', $cookie_data );
		}

		$current_loves = get_post_meta( $post_id, '_lp_love_post', true );
		if($current_loves == ''){ $current_loves = 0; }

		if(!in_array($post_id, $cookie_data_array)){
			update_post_meta( $post_id, '_lp_love_post', ++$current_loves );
			array_unshift($cookie_data_array, $post_id);
			$cookie_data_array = array_slice($cookie_data_array, 0, 100);  // max 100 posts can be liked
			$cookie_data = implode(',', $cookie_data_array);
			setcookie('love_post_' . COOKIEHASH, $cookie_data, time()+(3600*24*7), '/');
		}

		$post_url = get_permalink( $post_id );
		$post_title = get_the_title( $post_id );
		$new_link_html = '<li style="display:none;"><a href="'.$post_url.'">' . $post_title . '</a></li>';

		echo json_encode(array( 'success' => true, 'total_loves' => $current_loves++, 'new_title' => $post_love_string, 'new_link' => $new_link_html ));

		die();
	}


	///////////////////////////////////////////////////////////////////////////////////////
	//  UPCOMING:
	//	Runs when the user logs in and syncs the posts he loved while logged out
	//  with the posts that he loved when logged in
	///////////////////////////////////////////////////////////////////////////////////////

	public function lp_user_sync(){
		if(isset($_COOKIE['love_post_' . COOKIEHASH])){
			$cookie_data = $_COOKIE['love_post_' . COOKIEHASH];
			$cookie_data_array = explode( ',', $cookie_data );
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////
	//	Shortcode for displaying the list of users loved posts
	//  returns <ul> of posts or echoes the message if no posts are loved yet
	///////////////////////////////////////////////////////////////////////////////////////

	public function lp_user_shortcode( $atts, $content = "" ) {

		if(isset($_COOKIE['love_post_' . COOKIEHASH])){
			$cookie_data = $_COOKIE['love_post_' . COOKIEHASH];
			$cookie_data_array = explode( ',', $cookie_data );
		}

		if(isset($cookie_data_array)){
			$shortcode_html = '<ul>';

			foreach ($cookie_data_array as $post_ID) {
				$post_url = get_permalink( $post_ID );
				$post_title = get_the_title( $post_ID );
				$shortcode_html .= '<li><a href="'.$post_url.'">' . $post_title . '</a></li>';
			}

			$shortcode_html .= '</ul>';

			return $shortcode_html;
		} else {
			return $content;
		}

		wp_reset_query();

	}

	///////////////////////////////////////////////////////////////////////////////////////
	//	Shortcode for displaying the list of most popular loved posts
	//  returns <ul> of posts or echoes the message if no posts are loved yet
	///////////////////////////////////////////////////////////////////////////////////////

	public function lp_popular_shortcode( $atts, $content = "" ) {

		$popular_query = new WP_Query( array(
			'meta_key' => '_lp_love_post',
			'posts_per_page' => -1,
			'orderby' => 'meta_value_num',
			'ignore_sticky_posts' => 1
		));

		if ($popular_query->have_posts()){
			$shortcode_html = '<ul>';
			while ($popular_query->have_posts()) : $popular_query->the_post();
				$post_url = get_permalink();
				$post_title = get_the_title();
				$val = get_post_meta( get_the_ID(), '_lp_love_post', true );
				$shortcode_html .= '<li><a href="'.$post_url.'">' . $post_title . ' <span>(' . $val . ')</span></a></li>';
			endwhile;
			$shortcode_html .= '</ul>';
			return $shortcode_html;
		} else {
			return $content;
		}

		wp_reset_query();

	}

	///////////////////////////////////////////////////////////////////////////////////////
	//	Shortcode for displaying the love button
	//  returns <a>
	///////////////////////////////////////////////////////////////////////////////////////

	public function lp_link_shortcode( $atts, $content = "" ) {
		$post_id = get_the_ID();
		lp_love_post_link($post_id);
	}

}

new LovePost();

///////////////////////////////////////////////////////////////////////////////////////
//	Display Love Post Link
///////////////////////////////////////////////////////////////////////////////////////

function lp_love_post_link($post_id){

	if(is_page()){ return 0; }

	$cookie_data_array = array();
	$current_loves = get_post_meta( $post_id, '_lp_love_post', true );
	if($current_loves == ''){ $current_loves = 0; }

	if(isset($_COOKIE['love_post_' . COOKIEHASH])){
		$cookie_data = $_COOKIE['love_post_' . COOKIEHASH];
		$cookie_data_array = explode( ',', $cookie_data );
	}

	if(!in_array($post_id, $cookie_data_array)){
		$post_love_string = __('Love this post', 'love-post');
		$post_love_class = 'unloved';
	} else {
		$post_love_string = __('You already loved this post.', 'love-post');
		$post_love_class = 'loved';
	}

	echo '<a href="#" class="heart-love '.$post_love_class.'" title="' .$post_love_string. '">' .$current_loves. '</a>';
}

///////////////////////////////////////////////////////////////////////////////////////
//	Love Post Popular Widget
///////////////////////////////////////////////////////////////////////////////////////

class LovePostPopular_Widget extends WP_Widget {

	function __construct() {
		parent::WP_Widget( 'lp_love_post_popular', 'Love Post Popular', array( 'classname' => 'lp-love-post-popular', 'description' => __('Show most loved posts in your sidebar.', 'love-post') ) );
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		$title = '';
		$number = 5;
		
		if(isset($instance['title'])){ $title = $instance['title']; }
		if(isset($instance['number'])){ $number = $instance['number']; }

		//////////////////////////////////////////////////
		//  Get the posts query
		//////////////////////////////////////////////////

		$popular_query = new WP_Query( array(
			'meta_key' => '_lp_love_post',
			'posts_per_page' => $number,
			'orderby' => 'meta_value_num',
			'ignore_sticky_posts' => 1
		));

		//////////////////////////////////////////////////
		//  Start displaying the widget
		//////////////////////////////////////////////////

		echo $before_widget;

		//////////////////////////////////////////////////
		//  Display widget title
		//////////////////////////////////////////////////

		if($title != ''){
			echo $before_title . $instance['title'] . $after_title;
		}

		//////////////////////////////////////////////////
		//  Display list of posts
		//////////////////////////////////////////////////

		echo '<ul>';

		if ($popular_query->have_posts()) : while ($popular_query->have_posts()) : $popular_query->the_post();
			$post_url = get_permalink();
			$post_title = get_the_title();
			$val = get_post_meta( get_the_ID(), '_lp_love_post', true );
			echo '<li><a href="'.$post_url.'">' . $post_title . '</a> <span>(' . $val . ')</span></li>';
		endwhile; endif;

		echo '</ul>';

		echo $after_widget;

	}
	
	//////////////////////////////////////////////////
	//  U P D A T E   W I D G E T
	//////////////////////////////////////////////////
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$number_of_posts = $new_instance['number'];
		$number_of_posts = preg_replace('/[^0-9.]+/', '', $number_of_posts);
		$instance['number'] = $number_of_posts;

		return $instance;
	}

	//////////////////////////////////////////////////
	//   W I D G E T   S E T T I N G S
	//////////////////////////////////////////////////
	
	function form( $instance ) {

		// Set up some default widget settings
		$instance = wp_parse_args( (array) $instance );

		if(isset($instance['title'])){
			$instance_love_popular_title = $instance['title'];
		} else {
			$instance_love_popular_title = __('Most Loved Articles', 'love-post');
		}

		if(isset($instance['number'])){
			$instance_love_popular_number = $instance['number'];
		} else {
			$instance_love_popular_number = 5;
		}

		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo __('Title:', 'love-post') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" value="<?php echo $instance_love_popular_title; ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" class="widefat" style="width:100%;">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo __('Number of posts to show:', 'love-post') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'number' ); ?>" value="<?php echo $instance_love_popular_number; ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" class="widefat" style="width:100%;">
		</p>
		
<?php

	}
} 


///////////////////////////////////////////////////////////////////////////////////////
//	Love Post User Widget
///////////////////////////////////////////////////////////////////////////////////////

class LovePostUser_Widget extends WP_Widget {

	function __construct() {
		parent::WP_Widget( 'lp_love_post_user', 'Love Post User', array( 'classname' => 'lp-love-post-user', 'description' => __('Show last posts loved by current visitor.', 'love-post') ) );
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		$title = '';
		$description = '';
		$number = 5;
		$show = false;
		$description_empty = '';
		
		if(isset($instance['title'])){ $title = $instance['title']; }
		if(isset($instance['description'])){ $description = $instance['description']; }
		if(isset($instance['number'])){ $number = $instance['number']; }
		if(isset($instance['show'])){ $show = $instance['show']; }
		if(isset($instance['description_empty'])){ $description_empty = $instance['description_empty']; }

		//////////////////////////////////////////////////
		//  Get loved posts from the cookie
		//////////////////////////////////////////////////

		if(isset($_COOKIE['love_post_' . COOKIEHASH])){
			$cookie_data = $_COOKIE['love_post_' . COOKIEHASH];
			$cookie_data_array = explode( ',', $cookie_data );
			$cookie_data_array = array_slice($cookie_data_array, 0, $number);
		} else {
			if($show == false){
				return 0;
			} else {
				echo $before_widget;
				if($title != ''){ echo $before_title . $instance['title'] . $after_title; }
				if($description_empty != ''){ echo '<div class="empty-love-post-description"><p>'. $instance['description_empty'] .'</p></div>'; }
				echo $after_widget;
				return 0;
			}
		}

		//////////////////////////////////////////////////
		//  Start displaying the widget
		//////////////////////////////////////////////////

		echo $before_widget;

		//////////////////////////////////////////////////
		//  Display widget title
		//////////////////////////////////////////////////

		if($title != ''){
			echo $before_title . $title . $after_title;
		}

		//////////////////////////////////////////////////
		//  Display widget description
		//////////////////////////////////////////////////

		if($description != ''){
			echo '<p>'. $description .'</p>';
		}

		echo '<ul>';

		foreach ($cookie_data_array as $post_ID) {
			$post_url = get_permalink( $post_ID );
			$post_title = get_the_title( $post_ID );
			echo '<li><a href="'.$post_url.'">' . $post_title . '</a></li>';
		}

		echo '</ul>';

		echo $after_widget;

	}
	
	//////////////////////////////////////////////////
	//  U P D A T E   W I D G E T
	//////////////////////////////////////////////////
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['description'] = $new_instance['description'];
		$number_of_posts = $new_instance['number'];
		$number_of_posts = preg_replace('/[^0-9.]+/', '', $number_of_posts);
		$instance['number'] = $number_of_posts;
		$instance['show'] = $new_instance['show'];
		$instance['description_empty'] = $new_instance['description_empty'];

		return $instance;
	}

	//////////////////////////////////////////////////
	//   W I D G E T   S E T T I N G S
	//////////////////////////////////////////////////
	
	function form( $instance ) {

		// Set up some default widget settings
		$instance = wp_parse_args( (array) $instance );

		$instance_love_user_title = __('Your Loved Articles', 'love-post');
		$instance_love_user_description = '';
		$instance_love_user_number = 5;
		$instance_love_user_show = false;
		$instance_love_user_description_empty = '';

		if(isset($instance['title'])){ $instance_love_user_title = strip_tags($instance['title']); }
		if(isset($instance['description'])){ $instance_love_user_description = $instance['description']; }
		if(isset($instance['number'])){ $instance_love_user_number = $instance['number']; }
		if(isset($instance['show'])){ $instance_love_user_show = $instance['show']; }
		if(isset($instance['description_empty'])){ $instance_love_user_description_empty = $instance['description_empty']; }

		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo __('Title:', 'love-post') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" value="<?php echo $instance_love_user_title; ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" class="widefat" style="width:100%;">
		</p>

		<p>	
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php echo __('Description:', 'love-post') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'description' ); ?>" value="<?php echo htmlentities($instance_love_user_description); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" class="widefat" style="width:100%;">
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo __('Number of posts to show:', 'love-post') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'number' ); ?>" value="<?php echo $instance_love_user_number; ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" class="widefat" style="width:100%;">
		</p>

		<p>
			<input id="<?php echo $this->get_field_id( 'show' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'show' ); ?>" <?php if ($instance_love_user_show == 'on'){ echo 'checked="checked"';} ?>>
			<label for="<?php echo $this->get_field_id( 'show' ); ?>">Show widget if no posts are loved yet</label>
		</p>

		<p>	
			<label for="<?php echo $this->get_field_id( 'description_empty' ); ?>"><?php echo __('Description (for empty widget):', 'love-post') ?></label>
			<textarea name="<?php echo $this->get_field_name( 'description_empty' ); ?>" id="<?php echo $this->get_field_id( 'description_empty' ); ?>" cols="10" rows="4" class="widefat"><?php echo htmlentities($instance_love_user_description_empty); ?></textarea>
		</p>
<?php

	}
} 

?>