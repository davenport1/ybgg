//////////////////////////////////////////////////////
// Love Post JS
//////////////////////////////////////////////////////

jQuery(document).ready(function($) {

	//////////////////////////////////////////////////////
	// Love Rating
	//////////////////////////////////////////////////////

	$('body').on('click', 'a.heart-love', function(e){
		e.preventDefault();
	});

	$('body').on('click', 'a.heart-love.unloved', function(e){
		e.preventDefault();
		var $this = $(this);
		var postID = $this.parents('article').data('id');

		var postData = {
			'action': 'lp_love_post',
			'security': lp_love_post.nonce,
			'post_id': postID,
		};

		$this.attr('title', '');
		$this.removeClass('unloved');
		$this.addClass('loved fade-out-heart');

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: lp_love_post.ajaxurl,
			data: postData,
			success: function(data){
				if(data.success == true){
					$this.text(data.total_loves).attr('title', data.new_title).addClass('fade-in-heart');
					setTimeout(function(){ $this.removeClass('fade-in-heart fade-out-heart'); }, 500);

					//////////////////////////////////////////////////////
					// Update the user widget
					//////////////////////////////////////////////////////

					if($('.empty-love-post-description').length > 0){
						$('.empty-love-post-description').fadeOut(400, function(){
							$(this).replaceWith('<ul></ul>');
							$('.lp-love-post-user ul').prepend(data.new_link);
							$('.lp-love-post-user ul li:first').fadeIn(400);
						});
					} else {
						$('.lp-love-post-user ul').prepend(data.new_link);
						$('.lp-love-post-user ul li:first').fadeIn(400);						
					}
					
				}
			}
		});
		
	});

});