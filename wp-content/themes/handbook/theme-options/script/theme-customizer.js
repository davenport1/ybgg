( function( $ ) {

	////////////////////////////////////////////////////
	// Color & Style Options
	////////////////////////////////////////////////////

	wp.customize( 'handbook_theme_options[main_color]', function( value ) {
		value.bind( function( newval ) {
			$('.portfolio-keywords li.active a, .portfolio-keywords li.active span, .portfolio-keywords li.active span:after, a.more-link:hover, .admin-portfolio-message a.more-link, .portfolio-list span.view-more-icon, a.share-button:hover').attr('style', 'background-color: '+newval+' !important'); 
			$('.mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current, .mejs-controls .mejs-time-rail .mejs-time-current, .format-link > .link-content p:first-of-type a:hover, ul#pagination li a:hover, ul#pagination li.next a:hover, ul#pagination li.prev a:hover').attr('style', 'background-color: '+newval+' !important'); 
			$('ul.tags a:hover:before, ul.tags a:hover, .widget-pretty-tags a.pretty-tag:hover:before, .widget-pretty-tags a.pretty-tag:hover, .fullwidth-message a, .accordion-wrapper > h4.expanded, .accordion-wrapper > h4:hover, .menu-toggle').attr('style', 'background-color: '+newval+' !important'); 
			$('a:hover, .welcome-message p a:hover, .portfolio-meta li p a:hover, .portfolio-details h1 a:hover, .content p.post-meta a:hover, ul.comments > li.show-all a:hover, #footer a:hover').attr('style', 'color: '+newval+' !important');
		});
	});

	wp.customize( 'handbook_theme_options[header_version]', function( value ) {
		value.bind( function( newval ) {
			if(newval == 'dark'){
				$('body').addClass('dark-header');
			} else {
				$('body').removeClass('dark-header');
			}
		});
	});

	wp.customize( 'handbook_theme_options[use_uppercase]', function( value ) {
		value.bind( function( newval ) {
			if(newval == false){
				$('a.more-link, .page-info h1, .content table th, .articles-list li p, #header .site-title, #sidebar .widget-title, .fullwidth-message a, #footer h5.widget-title, .portfolio-details h2, .portfolio-details h1 a, #slider p.slide-caption, .content .comments-wrapper > h4, .content form input[type="submit"], .comment-respond h3.comment-reply-title, .portfolio-list.grid-layout li h1').css('text-transform', 'none');
				$('#sidebar .widget-title').css('font-size', '13px');
			} else {
				$('a.more-link, .page-info h1, .content table th, .articles-list li p, #header .site-title, #sidebar .widget-title, .fullwidth-message a, #footer h5.widget-title, .portfolio-details h2, .portfolio-details h1 a, #slider p.slide-caption, .content .comments-wrapper > h4, .content form input[type="submit"], .comment-respond h3.comment-reply-title, .portfolio-list.grid-layout li h1').css('text-transform', 'uppercase');
				$('#sidebar .widget-title').css('font-size', '11px');
			}
		});
	});

	wp.customize( 'handbook_theme_options[background_color]', function( value ) {
		value.bind( function( newval ) {
			$('body').css('background-color', newval );
			$('body').attr('data-background', newval).data('background', newval);
		});
	});

	////////////////////////////////////////////////////
	// Site Title & Logo
	////////////////////////////////////////////////////

	wp.customize( 'handbook_theme_options[logo]', function( value ) {
		value.bind( function( newval ) {
			if(newval){
				$('#header .site-title > a').remove();
				$('#header .site-title').prepend('<a href="#" class="logo"><img src="'+newval+'" alt="'+handbookCustomizer.title_text+'"></a>');
			} else {
				$('#header .site-title > a').remove();
			}
		});
	});

	wp.customize( 'handbook_theme_options[title_text]', function( value ) {
		value.bind( function( newval ) {
			$('#header .site-title > div h1').html( newval );
			if(newval == ''){
				$('#header .site-title > div h1').html( handbookCustomizer.title_text );
			}
		});
	});

	wp.customize( 'handbook_theme_options[description_text]', function( value ) {
		value.bind( function( newval ) {
			$('#header .site-title > div > p').html( newval );
			if(newval == ''){
				$('#header .site-title > div > p').html( handbookCustomizer.description_text );
			}
		});
	});

	////////////////////////////////////////////////////
	// Layout Options
	////////////////////////////////////////////////////

	wp.customize( 'handbook_theme_options[show_slider]', function( value ) {
		value.bind( function( newval ) {
			if(newval == false){
				if($('body').hasClass('blog') == false){
					$('#slider').remove();
				}
			} else {
				$('.welcome-message').after(handbookCustomizer.slider);
				$('#slider').flexslider({
					easing:       'easeOutQuad',
					animation:    'slide',
					namespace:    'slider-',
					pauseOnHover: true,
					slideshow: true,
					easing: 'swing',
					controlNav: false,
					animationLoop: true,
					smoothHeight: true,
				});
			}
		});
	});

	wp.customize( 'handbook_theme_options[show_slider_blog]', function( value ) {
		value.bind( function( newval ) {
			if(newval == false){
				$('body.blog #slider').remove();
			} else {
				$('body.blog .primary-content').before(handbookCustomizer.slider);
				$('#slider').flexslider({
					easing:       'easeOutQuad',
					animation:    'slide',
					namespace:    'slider-',
					pauseOnHover: true,
					slideshow: true,
					easing: 'swing',
					controlNav: false,
					animationLoop: true,
					smoothHeight: true,
				});
			}
		});
	});

	wp.customize( 'handbook_theme_options[show_sidebar]', function( value ) {
		value.bind( function( newval ) {
			if(newval == false){
				$('body').removeClass('sidebar-on-left sidebar-on-right').addClass('sidebar-off');
				$('#sidebar').hide();
			} else {
				$('body').removeClass('sidebar-off').addClass('sidebar-on-left');
				if($('#sidebar').length){
					$('#sidebar').show();
				} else {
					$('.primary-content').after(handbookCustomizer.sidebar);
				}
				$('a.widget-order-handle').on('click', function(e){
					e.preventDefault();
				});

				if (typeof sidebars == 'undefined'){
					sidebars = '';
				}

				var postData = sidebars;

				$('a.widget-order-handle').on('click', function(e){
					e.preventDefault();
				});

				$('#sidebar').sortable({
					connectWith: '#sidebar',
					placeholder: 'sortable-widget-placeholder',
					handle: '.widget-order-handle',
					start: function(e,ui){
						ui.placeholder.height(ui.item.height());
					},
					stop: function(){
						var new_order = '';
						var sidebar_id = 'sidebars['+ $('#sidebar').data('id') + ']';

						$('#sidebar aside.widget').each(function(index, value){
							new_order = new_order + 'widget-0_' + $(this).attr('id') +',';
						});
						
						new_order = new_order.slice(0, -1);

						postData.action = 'widgets-order';
						postData.savewidgets = $('#_wpnonce_widgets').val();

						postData[sidebar_id] = new_order;

						$.ajax({
							type: 'POST',
							dataType: 'json',
							url: handbook.ajaxurl,
							data: postData
						});
					}
				});
			}
		});
	});

	wp.customize( 'handbook_theme_options[sidebar_position]', function( value ) {
		value.bind( function( newval ) {
			if(newval == 'right'){
				$('body').removeClass('sidebar-on-left').addClass('sidebar-on-right');
				if($('#sidebar').length == 0){
					$('#sidebar').remove();
				}
			} else {
				$('body').removeClass('sidebar-on-right').addClass('sidebar-on-left');
				if($('#sidebar').length){
					$('#container').after($('#sidebar'));
				} else {
					$('#container').after(handbookCustomizer.sidebar);
				}
			}
		});
	});

	wp.customize( 'handbook_theme_options[boxed_layout]', function( value ) {
		value.bind( function( newval ) {
			if(newval == false){
				$('body').addClass('full-width-layout').css('background-color', '#FFFFFF');
			} else {
				$('body').css('background-color', '').removeClass('full-width-layout').css('background-color', $('body').data('background'));
			}
		});
	});

	////////////////////////////////////////////////////
	// Background Pattern
	////////////////////////////////////////////////////

	wp.customize( 'handbook_theme_options[background_pattern]', function( value ) {
		value.bind( function( newval ) {
			var pattern = handbookCustomizer.themeurl+newval+'.png';
			if(typeof $('body').data('background-image') === 'undefined'){
				var background = handbookCustomizer.background_image;
			} else {
				var background = $('body').data('background-image');
			}
			$('body').css('background-image', 'url(' + pattern + '), url(' + background + ')');
			$('body').css('background-repeat', 'repeat, no-repeat');
			$('body').css('background-size', 'auto auto, cover');
			$('body').data('background-pattern', pattern);
		});
	});

	////////////////////////////////////////////////////
	// Background Image
	////////////////////////////////////////////////////

	wp.customize( 'handbook_theme_options[background_image]', function( value ) {
		value.bind( function( newval ) {
			if(typeof $('body').data('background-pattern') === 'undefined'){
				var pattern = handbookCustomizer.background_pattern;
			} else {
				var pattern = $('body').data('background-pattern');
			}
			$('body').css('background-image', 'url(' + pattern + '), url(' + newval + ')');
			$('body').css('background-repeat', 'repeat, no-repeat');
			$('body').css('background-size', 'auto auto, cover');
			$('body').data('background-image', newval);
		});
	});

	////////////////////////////////////////////////////
	// Portfolio Settings
	////////////////////////////////////////////////////

	wp.customize( 'handbook_theme_options[portfolio_related]', function( value ) {
		value.bind( function( newval ) {
			$('.portfolio-similar-items > h4').text( newval );
		});
	});

	wp.customize( 'handbook_theme_options[portfolio_view_all]', function( value ) {
		value.bind( function( newval ) {
			if(newval != ''){
				$('ul.portfolio-keywords > li:first-child > a').text( newval );
			} else {
				$('ul.portfolio-keywords > li:first-child > a').text( handbookCustomizer.view_all );
			}
		});
	});

	////////////////////////////////////////////////////
	// Frontpage Messages
	////////////////////////////////////////////////////

	wp.customize( 'handbook_theme_options[greeting_word]', function( value ) {
		value.bind( function( newval ) {
			$('.welcome-message > span').html( newval );
		});
	});

	wp.customize( 'handbook_theme_options[welcome_text]', function( value ) {
		value.bind( function( newval ) {
			$('.welcome-message > p').html( newval );
		});
	});

	wp.customize( 'handbook_theme_options[fullwidth_message]', function( value ) {
		value.bind( function( newval ) {
			$('.fullwidth-message > p').html( newval );
			if(newval == ''){
				$('.fullwidth-message').hide();
			} else {
				$('.fullwidth-message').show();
			}
		});
	});

	wp.customize( 'handbook_theme_options[fullwidth_message_button]', function( value ) {
		value.bind( function( newval ) {
			$('.fullwidth-message > a').html( newval );
		});
	});

	wp.customize( 'handbook_theme_options[fullwidth_message_link]', function( value ) {
		value.bind( function( newval ) {
			$('.fullwidth-message > a').attr('href', newval );
		});
	});

	////////////////////////////////////////////////////
	// Footer
	////////////////////////////////////////////////////

	wp.customize( 'handbook_theme_options[footer_text]', function( value ) {
		value.bind( function( newval ) {
			$('#footer .copyright > p').html( newval );
		});
	});

} )( jQuery );