<?php 

///////////////////////////////////////////////////////////////////////////////////////
// Pretty Tags Widget
///////////////////////////////////////////////////////////////////////////////////////

add_action( 'widgets_init', 'pt_pretty_tags' );

function pt_pretty_tags() {
	register_widget( 'pt_pretty_tags' );
}

class pt_pretty_tags extends WP_Widget {
	function pt_pretty_tags() {
		// Widget settings
		$widget_ops = array( 'classname' => 'widget-pretty-tags', 'description' => __('Display tags with a nicer style.', 'handbook') );

		// Widget control settings
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'pt_pretty_tags' );

		// Create the widget
		$this->WP_Widget( 'pt_pretty_tags', 'Pretty Tags', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		$title = '';
		$number = 5;

		if(isset($instance['title'])){ $title = $instance['title']; }
		if(isset($instance['number'])){ $number = $instance['number']; }

		echo $before_widget;

		//////////////////////////////////////////////////
		//  Display widget title
		//////////////////////////////////////////////////

		if($title != ''){
			echo $before_title . $instance['title'] . $after_title;
		}

		//////////////////////////////////////////////////
		//  Display tags
		//////////////////////////////////////////////////

		$tags_output = '';

		$tags = get_tags(array('orderby' => 'count', 'order' => 'DESC', 'number' => $number));
		foreach ($tags as $tag){
			$tag_link = get_tag_link($tag->term_id);
			$tags_output .= "<a href='{$tag_link}' title='{$tag->name}' class='pretty-tag {$tag->slug}'>";
			$tags_output .= "{$tag->name}</a>";
		}

		echo $tags_output ;

		echo '<div class="clear"></div>';

		echo $after_widget;

	}
	
	//////////////////////////////////////////////////
	//  U P D A T E   W I D G E T
	//////////////////////////////////////////////////
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$number_of_tags = $new_instance['number'];
		$number_of_tags = preg_replace('/[^0-9.]+/', '', $number_of_tags);
		$instance['number'] = $number_of_tags;

		return $instance;

	}

	//////////////////////////////////////////////////
	//   W I D G E T   S E T T I N G S
	//////////////////////////////////////////////////
	
	function form( $instance ) {

		// Set up some default widget settings
		$instance = wp_parse_args( (array) $instance );

		$instance_pretty_tags_title = '';

		if(isset($instance['title'])){
			$instance_pretty_tags_title = $instance['title'];
		}

		if(isset($instance['number'])){
			$instance_pretty_tags_number = $instance['number'];
		} else {
			$instance_pretty_tags_number = 5;
		}

		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>" class="pt-pretty-tags"><?php echo __('Title:', 'handbook') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" value="<?php echo $instance_pretty_tags_title; ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" class="widefat" style="width:100%;">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo __('Number of tags to show:', 'handbook') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'number' ); ?>" value="<?php echo $instance_pretty_tags_number; ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" class="widefat" style="width:100%;">
		</p>
<?php

	}
} 


 ?>