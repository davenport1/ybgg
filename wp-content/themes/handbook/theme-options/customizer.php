<?php 

///////////////////////////////////////////////////////////////////////////////////////
// Theme Customizer
///////////////////////////////////////////////////////////////////////////////////////

$handbook_patterns = array(
	'none'   			=> __( 'No Pattern', 'handbook' ),
	'arches'		    => __( 'Arches', 'handbook' ),
	'brick-wall'		=> __( 'Brick Wall', 'handbook' ),
	'circles-light'	    => __( 'Circles Light', 'handbook' ),
	'circles-dark'	    => __( 'Circles Dark', 'handbook' ),
	'damask-01-dark'	=> __( 'Damask Dark 01', 'handbook' ),
	'damask-01-light'	=> __( 'Damask Light 01', 'handbook' ),
	'damask-02-dark'	=> __( 'Damask Dark 02', 'handbook' ),
	'damask-02-light'	=> __( 'Damask Light 02', 'handbook' ),
	'damask-03-dark'	=> __( 'Damask Dark 03', 'handbook' ),
	'damask-03-light'	=> __( 'Damask Light 03', 'handbook' ),
	'damask-04-dark'	=> __( 'Damask Dark 04', 'handbook' ),
	'damask-04-light'	=> __( 'Damask Light 04', 'handbook' ),
	'damask-05-dark'	=> __( 'Damask Dark 05', 'handbook' ),
	'damask-05-light'	=> __( 'Damask Light 05', 'handbook' ),
	'diamonds'			=> __( 'Diamonds', 'handbook' ),
	'dots'				=> __( 'Dots', 'handbook' ),
	'fabric-left'		=> __( 'Fabric Left', 'handbook' ),
	'fabric-right'		=> __( 'Fabric Right', 'handbook' ),
	'horizontal-stripes'=> __( 'Horizontal Stripes', 'handbook' ),
	'noise'				=> __( 'Noise', 'handbook' ),
	'square'			=> __( 'Square', 'handbook' ),
	'squares'			=> __( 'Squares', 'handbook' ),
	'triangles'			=> __( 'Triangles', 'handbook' ),
	'vertical-dots'		=> __( 'Vertical Dots', 'handbook' ),
	'wood'				=> __( 'Wood', 'handbook' ),
);

function handbook_customize_register( $wp_customize ){

	global $handbook_patterns;


	//////////////////////////////////////////////////////////////////////////////////
	// Textarea control
	///////////////////////////////////////////////////////////////////////////////////

	class Customizer_Textarea_Control extends WP_Customize_Control {
		public $type = 'textarea';

		public function render_content() { ?>
		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<textarea rows="7" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
		</label>
		<?php
		}
	}

	///////////////////////////////////////////////////////////////////////////////////
	// Color Options
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->get_section('colors')->title = __( 'Color & Style Options', 'handbook' );
	$wp_customize->get_section('colors')->priority = 1;

	$wp_customize->add_setting( 'handbook_theme_options[main_color]' , array(
		'default'    => '#F77A61',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'handbook_theme_options[main_color]', array(
		'label'      => __( 'Main Color', 'handbook' ),
		'section'    => 'colors',
		'settings'   => 'handbook_theme_options[main_color]',
		'priority'   => 1,
	)));

	$wp_customize->add_setting( 'handbook_theme_options[header_version]' , array(
		'default'    => 'light',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[header_version]', array(
		'label'      => __( 'Header Version', 'handbook' ),
		'section'    => 'colors',
		'settings'   => 'handbook_theme_options[header_version]',
		'type'       => 'radio',
		'priority'   => 2,
		'choices'    => array(
			'light'  => 'Light',
			'dark'   => 'Dark',
		)
	));

	$wp_customize->add_setting( 'handbook_theme_options[use_uppercase]' , array(
		'default'    => true,
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[use_uppercase]', array(
		'label'      => __( 'Use Uppercase Titles', 'handbook' ),
		'settings'   => 'handbook_theme_options[use_uppercase]',
		'section'    => 'colors',
		'type'       => 'checkbox',
		'priority'   => 3,
	));

	$wp_customize->add_setting( 'handbook_theme_options[background_color]' , array(
		'default'    => '#F1F1F1',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'handbook_theme_options[background_color]', array(
		'label'      => __( 'Background Color', 'handbook' ),
		'section'    => 'colors',
		'settings'   => 'handbook_theme_options[background_color]',
		'priority'   => 4,
	)));

	///////////////////////////////////////////////////////////////////////////////////
	// Site Title & Logo
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->get_section('title_tagline')->title = __( 'Site Title & Logo', 'handbook' );
	$wp_customize->get_section('title_tagline')->priority = 2;

	$wp_customize->add_setting( 'handbook_theme_options[logo]' , array(
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'handbook_theme_options[logo]', array(
		'label'      => __( 'Site Logo', 'handbook' ),
		'section'    => 'title_tagline',
		'settings'   => 'handbook_theme_options[logo]',
		'priority'   => 1,
	)));

	$wp_customize->add_setting( 'handbook_theme_options[title_text]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[title_text]', array(
		'label'      => __( 'Title', 'handbook' ),
		'section'    => 'title_tagline',
		'settings'   => 'handbook_theme_options[title_text]',
		'type'       => 'text',
		'priority'   => 2,
	));

	$wp_customize->add_setting( 'handbook_theme_options[description_text]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[description_text]', array(
		'label'      => __( 'Description', 'handbook' ),
		'section'    => 'title_tagline',
		'settings'   => 'handbook_theme_options[description_text]',
		'type'       => 'text',
		'priority'   => 3,
	));

	$wp_customize->add_setting( 'handbook_theme_options[favicon_image_upload]' , array(
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'handbook_theme_options[favicon_image_upload]', array(
		'label'      => __( 'Favicon', 'handbook' ),
		'section'    => 'title_tagline',
		'settings'   => 'handbook_theme_options[favicon_image_upload]',
		'priority'   => 4,
	)));

	///////////////////////////////////////////////////////////////////////////////////
	// Layout
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->add_section( 'handbook_section_layout' , array(
		'title'		 => __('Layout Options','handbook'),
		'priority'   => 3,
	));

	$wp_customize->add_setting( 'handbook_theme_options[show_slider]' , array(
		'default'    => true,
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[show_slider]', array(
		'label'      => __( 'Show Slider on Frontpage', 'handbook' ),
		'settings'   => 'handbook_theme_options[show_slider]',
		'section'    => 'handbook_section_layout',
		'type'       => 'checkbox',
		'priority'   => 1,
	));

	$wp_customize->add_setting( 'handbook_theme_options[show_slider_blog]' , array(
		'default'    => false,
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[show_slider_blog]', array(
		'label'      => __( 'Show Slider on Blog', 'handbook' ),
		'settings'   => 'handbook_theme_options[show_slider_blog]',
		'section'    => 'handbook_section_layout',
		'type'       => 'checkbox',
		'priority'   => 2,
	));

	$wp_customize->add_setting( 'handbook_theme_options[show_sidebar]' , array(
		'default'    => true,
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[show_sidebar]', array(
		'label'      => __( 'Show Sidebar', 'handbook' ),
		'settings'   => 'handbook_theme_options[show_sidebar]',
		'section'    => 'handbook_section_layout',
		'type'       => 'checkbox',
		'priority'   => 3,
	));

	$wp_customize->add_setting( 'handbook_theme_options[sidebar_position]' , array(
		'default'    => 'left',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[sidebar_position]', array(
		'label'      => __( 'Sidebar Position', 'handbook' ),
		'section'    => 'handbook_section_layout',
		'settings'   => 'handbook_theme_options[sidebar_position]',
		'type'       => 'radio',
		'priority'   => 4,
		'choices'    => array(
			'left'   => __( 'Left Sidebar', 'handbook' ),
			'right'  => __( 'Right Sidebar', 'handbook' ),
		)
	));

	$wp_customize->add_setting( 'handbook_theme_options[boxed_layout]' , array(
		'default'    => true,
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[boxed_layout]', array(
		'label'      => __( 'Use boxed layout', 'handbook' ),
		'settings'   => 'handbook_theme_options[boxed_layout]',
		'section'    => 'handbook_section_layout',
		'type'       => 'checkbox',
		'priority'   => 5,
	));

	///////////////////////////////////////////////////////////////////////////////////
	// Background Pattern
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->add_section( 'handbook_section_pattern' , array(
		'title'		 => __( 'Background Pattern', 'handbook' ),
		'priority'   => 4,
	));

	$wp_customize->add_setting( 'handbook_theme_options[background_pattern]' , array(
		'default'    => 'dotted-overlay',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[background_pattern]', array(
		'label'      => __( 'Background Pattern', 'handbook' ),
		'section'    => 'handbook_section_pattern',
		'settings'   => 'handbook_theme_options[background_pattern]',
		'type'       => 'radio',
		'priority'   => 1,
		'choices'    => $handbook_patterns,
	));

	///////////////////////////////////////////////////////////////////////////////////
	// Background Image
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->remove_section( 'background_image' );

	$wp_customize->add_section( 'handbook_section_background_image' , array(
		'title'		 => __( 'Background Image', 'handbook' ),
		'priority'   => 5,
	));

	$wp_customize->add_setting( 'handbook_theme_options[background_image]' , array(
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'handbook_theme_options[background_image]', array(
		'label'      => __( 'Background Image', 'handbook' ),
		'section'    => 'handbook_section_background_image',
		'settings'   => 'handbook_theme_options[background_image]',
		'priority'   => 1,
	)));

	///////////////////////////////////////////////////////////////////////////////////
	// Portfolio Settings
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->add_section( 'handbook_section_portfolio_settings' , array(
		'title'		 => __('Portfolio Settings', 'handbook'),
		'priority'   => 6,
	));

	$wp_customize->add_setting( 'handbook_theme_options[portfolio_view_all]', array(
		'default'    => __( 'View All', 'handbook' ),
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[portfolio_view_all]', array(
		'label'      => __( 'Label for displaying all items', 'handbook' ),
		'section'    => 'handbook_section_portfolio_settings',
		'settings'   => 'handbook_theme_options[portfolio_view_all]',
		'type'       => 'text',
		'priority'   => 1,
	));

	$wp_customize->add_setting( 'handbook_theme_options[portfolio_number]', array(
		'default'    => '40',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[portfolio_number]', array(
		'label'      => __( 'Number of items', 'handbook' ),
		'section'    => 'handbook_section_portfolio_settings',
		'settings'   => 'handbook_theme_options[portfolio_number]',
		'type'       => 'text',
		'priority'   => 2,
	));

	$wp_customize->add_setting( 'handbook_theme_options[portfolio_related]', array(
		'default'    => __( 'RELATED WORK', 'handbook' ),
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[portfolio_related]', array(
		'label'      => __( 'Related work title', 'handbook' ),
		'section'    => 'handbook_section_portfolio_settings',
		'settings'   => 'handbook_theme_options[portfolio_related]',
		'type'       => 'text',
		'priority'   => 3,
	));

	$wp_customize->add_setting( 'handbook_theme_options[portfolio_autoplay]' , array(
		'default'    => true,
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[portfolio_autoplay]', array(
		'label'      => __( 'Enable portfolio gallery autoplay (disable if using videos)', 'handbook' ),
		'settings'   => 'handbook_theme_options[portfolio_autoplay]',
		'section'    => 'handbook_section_portfolio_settings',
		'type'       => 'checkbox',
		'priority'   => 4,
	));

	///////////////////////////////////////////////////////////////////////////////////
	// Frontpage messages
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->add_section( 'handbook_section_frontpage_message' , array(
		'title'		 => __('Frontpage Messages', 'handbook'),
		'priority'   => 7,
	));

	$wp_customize->add_setting( 'handbook_theme_options[greeting_word]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[greeting_word]', array(
		'label'      => __( 'Greeting Word', 'handbook' ),
		'section'    => 'handbook_section_frontpage_message',
		'settings'   => 'handbook_theme_options[greeting_word]',
		'type'       => 'text',
		'priority'   => 1,
	));

	$wp_customize->add_setting( 'handbook_theme_options[welcome_text]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new Customizer_Textarea_Control( $wp_customize, 'handbook_theme_options[welcome_text]', array(
		'label'      => __( 'Welcome Text', 'handbook' ),
		'section'    => 'handbook_section_frontpage_message',
		'settings'   => 'handbook_theme_options[welcome_text]',
		'priority'   => 2,
	)));

	$wp_customize->add_setting( 'handbook_theme_options[fullwidth_message]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new Customizer_Textarea_Control( $wp_customize, 'handbook_theme_options[fullwidth_message]', array(
		'label'      => __( 'Fullwidth Message', 'handbook' ),
		'section'    => 'handbook_section_frontpage_message',
		'settings'   => 'handbook_theme_options[fullwidth_message]',
		'priority'   => 3,
	)));

	$wp_customize->add_setting( 'handbook_theme_options[fullwidth_message_button]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[fullwidth_message_button]', array(
		'label'      => __( 'Fullwidth Message Button Label', 'handbook' ),
		'section'    => 'handbook_section_frontpage_message',
		'settings'   => 'handbook_theme_options[fullwidth_message_button]',
		'type'       => 'text',
		'priority'   => 4,
	));

	$wp_customize->add_setting( 'handbook_theme_options[fullwidth_message_link]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[fullwidth_message_link]', array(
		'label'      => __( 'Fullwidth Message Button Link', 'handbook' ),
		'section'    => 'handbook_section_frontpage_message',
		'settings'   => 'handbook_theme_options[fullwidth_message_link]',
		'type'       => 'text',
		'priority'   => 5,
	));

	///////////////////////////////////////////////////////////////////////////////////
	// Footer
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->add_section( 'handbook_section_footer' , array(
		'title'		 => __('Footer', 'handbook'),
		'priority'   => 8,
	));

	$wp_customize->add_setting( 'handbook_theme_options[footer_text]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( 'handbook_theme_options[footer_text]', array(
		'label'      => __( 'Footer Text', 'handbook' ),
		'section'    => 'handbook_section_footer',
		'settings'   => 'handbook_theme_options[footer_text]',
		'type'       => 'text',
		'priority'   => 1,
	));

	///////////////////////////////////////////////////////////////////////////////////
	// Login Image
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->add_section( 'handbook_login_image' , array(
		'title'		 => __('Login Image', 'handbook'),
		'description'=> __('Image you want to display on the login page, maximum 80x80 pixels.', 'handbook'),
		'priority'   => 20,
	));

	$wp_customize->add_setting( 'handbook_theme_options[login_image]' , array(
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'handbook_theme_options[login_image]', array(
		'label'      => __( 'Login Image', 'handbook' ),
		'section'    => 'handbook_login_image',
		'settings'   => 'handbook_theme_options[login_image]',
		'priority'   => 20,
	)));

	///////////////////////////////////////////////////////////////////////////////////
	// Additional CSS
	///////////////////////////////////////////////////////////////////////////////////

	$wp_customize->add_section( 'handbook_additional_css' , array(
		'title'		 => __('Additional CSS','handbook'),
		'priority'   => 999,
	));

	$wp_customize->add_setting( 'handbook_theme_options[additional_css]', array(
		'default'    => '',
		'type'       => 'option',
		'transport'  => 'postMessage',
	));

	$wp_customize->add_control( new Customizer_Textarea_Control( $wp_customize, 'handbook_theme_options[additional_css]', array(
		'label'      => __( 'Additional CSS', 'handbook' ),
		'section'    => 'handbook_additional_css',
		'settings'   => 'handbook_theme_options[additional_css]',
		'priority'   => 1,
	)));


	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

	$wp_customize->remove_control('header_textcolor');
	$wp_customize->remove_control('background_color');
	$wp_customize->remove_control('blogname');
	$wp_customize->remove_control('blogdescription');
	$wp_customize->remove_control('display_header_text');
	

}

add_action( 'customize_register', 'handbook_customize_register' );


function handbook_css_custom() {
	global $post;
	$options = get_option('handbook_theme_options'); 
	$additional_css = ''; 
	$custom_background = '';
	$additional_css = $options['additional_css'];
	$favicon = '';
	if(is_object($post)){
		$custom_background = get_post_meta( get_the_ID(), '_custom-background', true );
	}
	if(isset($options['favicon_image_upload'])){
		$favicon = $options['favicon_image_upload'];
	} ?>

	<!--Customizer CSS--> 

	<?php if ($favicon != ''){ ?>
		<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
	<?php } ?>

	<style type="text/css">

		<?php if ($options['background_pattern'] != 'none' && $options['background_image'] != ''){ ?>
			body{
				background-color: <?php echo $options['background_color']; ?>;
				background-image: url("<?php echo get_template_directory_uri(); ?>/images/patterns/<?php echo $options['background_pattern'] ?>.png"), url("<?php echo $options['background_image']; ?>");
				background-size: auto auto, cover;
				background-repeat: repeat, no-repeat;
			}
		<?php } else if ($options['background_pattern'] == 'none' && $options['background_image'] != '') { ?>
			body{
				background-color: <?php echo $options['background_color']; ?>;
				background-image: url("<?php echo $options['background_image']; ?>");
				background-size: cover;
				background-repeat: no-repeat;
			}
		<?php } else if ($options['background_pattern'] != 'none' && $options['background_image'] == '') { ?>
			body{
				background-color: <?php echo $options['background_color']; ?>;
				background-image: url("<?php echo get_template_directory_uri(); ?>/images/patterns/<?php echo $options['background_pattern'] ?>.png");
				background-size: auto auto;
				background-repeat: repeat;
			}
		<?php } ?>

		a:hover, .welcome-message p a:hover, .portfolio-meta li p a:hover, .portfolio-details h1 a:hover, .content p.post-meta a:hover, ul.comments > li.show-all a:hover, #footer a:hover{
			color: <?php echo $options['main_color']; ?>;
		}

		<?php if( $options['use_uppercase'] == false){ ?>
			a.more-link, .page-info h1, #nav-menu.show, .content table th, .articles-list li p, .fullwidth-message > a, #header .site-title, #sidebar .widget-title, #footer h5.widget-title, .portfolio-details h2, .portfolio-details h1 a, #slider p.slide-caption, .content .comments-wrapper > h4, .content form input[type="submit"], .comment-respond h3.comment-reply-title, .portfolio-list.grid-layout li h1{
				text-transform: none;
			}
			#sidebar .widget-title{
				font-size: 13px;
			}
		<?php } ?>

		.portfolio-keywords li.active a, .portfolio-keywords li.active span, .portfolio-keywords li.active span:after, a.more-link:hover, 
		.admin-portfolio-message a.more-link, .portfolio-list span.view-more-icon, .portfolio-similar-items span.view-more-icon, a.share-button:hover, 
		.mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current, .mejs-controls .mejs-time-rail .mejs-time-current,
		ul#pagination li a:hover, ul#pagination li.next a:hover, ul#pagination li.prev a:hover, ul#pagination li a.selected,
		ul.tags a:hover:before, ul.tags a:hover, .widget-pretty-tags a.pretty-tag:hover:before, .widget-pretty-tags a.pretty-tag:hover,
		.fullwidth-message > a, .accordion-wrapper > h4.expanded, .accordion-wrapper > h4:hover, .menu-toggle, .slider-direction-nav li a:hover{
			background-color: <?php echo $options['main_color']; ?> !important;
		}

		.portfolio-nav a:hover,
		#swipebox-action #swipebox-prev:hover, #swipebox-action #swipebox-next:hover, #swipebox-action #swipebox-close:hover,
		.format-link > .link-content p:first-of-type a:hover{
			background-color: <?php echo $options['main_color']; ?>;
		}

		body.page .content .excerpt p,
		body.single .content .excerpt p,
		.content blockquote{
			border-left: 4px solid <?php echo $options['main_color']; ?>;
		}

		a.gallery-thumbnail.active:before{
			box-shadow: 0 0 0 3px <?php echo $options['main_color']; ?>;
		}

		<?php if($custom_background != ''){ ?>
			body, body.custom-background{
				background-image: url('<?php echo $custom_background; ?>');
				background-size: cover;
			}
		<?php } ?>

		<?php if($options['boxed_layout'] == false){ ?>
			body.full-width-layout{
				background-color: #FFF;
			}
		<?php } ?>
			
	</style>
	<!--/Customizer CSS-->

	<?php if($additional_css != ''){ ?>
		<!--Additional CSS--> 
		<style type="text/css">
			<?php echo $additional_css; ?>
		</style>
		<!--/Additional CSS-->
	<?php } ?>

<?php }

add_action( 'wp_head', 'handbook_css_custom' );

///////////////////////////////////////////////////////////////////////////////////
// Login Image
///////////////////////////////////////////////////////////////////////////////////

function handbook_login_image() {

	$options = get_option('handbook_theme_options'); 

	if($options['login_image'] != ''){
	?>

	<style type="text/css">
		h1 a{ background-image: url("<?php echo $options['login_image']; ?>") !important; background-size: auto !important; margin-bottom: 10px !important; }
	</style>

<?php }
}

add_action('login_head', 'handbook_login_image', 999);

function handbook_login_image_url() {
    return home_url('/');
}
add_filter( 'login_headerurl', 'handbook_login_image_url' );

function handbook_login_image_title() {
    return get_option('blogname');
}
add_filter( 'login_headertitle', 'handbook_login_image_title' );


///////////////////////////////////////////////////////////////////////////////////
// Body Classes
///////////////////////////////////////////////////////////////////////////////////

function handbook_body_class($classes) {

	$options = get_option('handbook_theme_options');
	$ajaxed_comments = get_option('ajaxed_comments');

	if($options['header_version'] == 'dark'){
		$classes[] = 'dark-header';
	}

	if($options['show_sidebar'] == true && $options['sidebar_position'] == 'left'){
		$classes[] = 'sidebar-on-left';
	} else if($options['show_sidebar'] == true && $options['sidebar_position'] == 'right'){
		$classes[] = 'sidebar-on-right';
	}

	if($options['show_sidebar'] == false || is_404()){
		$classes[] = 'sidebar-off';
	}

	if($options['boxed_layout'] == false){
		$classes[] = 'full-width-layout';
	}

	if($ajaxed_comments != '1'){
		$classes[] = 'disable-ajaxed-comments';
	}

	return $classes;
}

add_filter('body_class','handbook_body_class');



function handbook_customize_preview() {
	wp_enqueue_script( 'handbook-live', get_template_directory_uri().'/theme-options/script/theme-customizer.js', array( 'jquery','customize-preview' ));

	$options = get_option('handbook_theme_options');
	$logo = '';
	$logo = $options['logo'];

	$title_text = get_bloginfo('name');
	$description_text = get_bloginfo('description');

	ob_start();
		get_template_part('slider', 'template');
		$slider_html = ob_get_contents();
	ob_end_clean();

	ob_start(); ?>
		<div id="sidebar" data-id="sidebar-main" class="widget-area">
			<?php dynamic_sidebar('sidebar-main'); ?>
		</div> <?php
		$sidebar_html = ob_get_contents();
	ob_end_clean();

	wp_localize_script( 'handbook-live', 'handbookCustomizer', array( 
		'ajaxurl'    => admin_url( 'admin-ajax.php' ),
		'slider'     => $slider_html,
		'sidebar'    => $sidebar_html,
		'title_text' => $title_text,
		'description_text' => $description_text,
		'view_all' => __('View All', 'handbook'),
		'themeurl'   => get_template_directory_uri() .'/images/patterns/',
		'background_image'   => $options['background_image'],
		'background_pattern'   => get_template_directory_uri() .'/images/patterns/' . $options['background_pattern'] . '.png',
		)
	);
}

add_action( 'customize_preview_init', 'handbook_customize_preview' );



function handbook_customize_toggler(){ 
	global $handbook_patterns; 
	$pattern_url = get_template_directory_uri() .'/images/patterns/';
	$options = get_option('handbook_theme_options'); 
	$background_pattern_url = $pattern_url . $options['background_pattern'] . '.png';
	$background_image_url = $options['background_image'];

	?>

	<script type="text/javascript">
		jQuery(document).ready(function($) {

			function trimspace(str) {
				return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			}

			$('#customize-theme-controls input:radio').each(function() {
				var $this = $(this);

				var labelclass = $this.val().replace(/\s+/g, '-').toLowerCase();
				$this.parent('label').addClass(labelclass);

				var title = trimspace($this.parent().text());
				$this.parent().attr('title', title);
			});

			setTimeout(function(){ $('#customize-theme-controls input[type=radio]:checked').parent('label').addClass('selected'); }, 10);

			$('#customize-control-handbook_theme_options-sidebar_position input, #customize-control-handbook_theme_options-boxed_layout input, #customize-control-handbook_theme_options-background_pattern input').on('click', function(){
				$(this).parent('label').addClass('selected').siblings().removeClass('selected');
			});

			$('#customize-control-handbook_theme_options-show_sidebar input').on('click', function(){
				$(this).closest('li').next().stop().slideToggle();
			});

			$('#customize-control-handbook_theme_options-show_sidebar input').on('click', function(){
				if( $('li#customize-control-handbook_theme_options-show_sidebar input').is(':checked') ){
					$('#customize-control-handbook_theme_options-sidebar_position label.left input').trigger('click');
				}
			});

			if( $('li#customize-control-handbook_theme_options-show_sidebar input').is(':checked') ){
				$('li#customize-control-handbook_theme_options-sidebar_position').show();
			}

			$('#customize-control-handbook_theme_options-background_pattern').data('background-pattern', '<?php echo $background_pattern_url; ?>');
			$('#customize-control-handbook_theme_options-background_image').data('background-image', '<?php echo $background_image_url; ?>');

		});
	</script>

	<style type="text/css">

		.wp-full-overlay-sidebar {
			box-shadow: 0 0 2px rgba(0, 0, 0, 0.3);
		}

		#customize-control-handbook_theme_options-slider_size,
		#customize-control-handbook_theme_options-sidebar_position,
		#customize-control-handbook_theme_options-show_header_always{
			display: none;
		}

		#customize-control-handbook_theme_options-background_pattern input,
		#customize-control-handbook_theme_options-content_width input,
		#customize-control-handbook_theme_options-slider_size input,
		#customize-control-handbook_theme_options-sidebar_position input{
			position: absolute;
			left: -9999px;
		}

		#customize-control-handbook_theme_options-background_pattern label,
		#customize-control-handbook_theme_options-content_width label,
		#customize-control-handbook_theme_options-slider_size label,
		#customize-control-handbook_theme_options-sidebar_position label{
			float: left;
			display: block;
			width: 81px; 
			margin-top: 10px;
			text-indent: -9999px;
			white-space: nowrap;
			height: 66px;
			padding: 6px;
			background-color: transparent;
			border-radius: 4px;
			border: 3px solid #d7d7d7;
			background-image: url("<?php echo get_template_directory_uri() ?>/images/admin/content-width.png");
			background-position: 0 0;
		}

		#customize-control-handbook_theme_options-sidebar_position label{
			background-image: url("<?php echo get_template_directory_uri() ?>/theme-options/images/sidebar-position.png");
		}

		#customize-control-handbook_theme_options-content_width label.compact,
		#customize-control-handbook_theme_options-slider_size label.compact,
		#customize-control-handbook_theme_options-sidebar_position label.right{
			background-position: 0 -122px;
			margin-left: 30px;
		}

		#customize-control-handbook_theme_options-background_pattern label.selected,
		#customize-control-handbook_theme_options-content_width label.selected,
		#customize-control-handbook_theme_options-slider_size label.selected,
		#customize-control-handbook_theme_options-sidebar_position label.selected{
			border: 3px solid #5b9af9;
			-webkit-transition: all 0.2s ease-in-out;
			-moz-transition: all 0.2s ease-in-out;
			-o-transition: all 0.2s ease-in-out;
			transition: all 0.2s ease-in-out;
		}

		#customize-control-handbook_theme_options-background_pattern label{
			width: 50px;
			height: 50px;
			margin: 10px 10px 0 0;
			padding: 0;
		}

		<?php foreach ($handbook_patterns as $pattern => $value) { ?>
			#customize-control-handbook_theme_options-background_pattern label.<?php echo $pattern; ?>{
				<?php if($pattern == 'brick-light' || $pattern == 'sand'){ ?>
					background-image: url("<?php echo get_template_directory_uri().'/images/patterns/'; echo $pattern; ?>.jpg");
					background-size: 100px 100px;
				<?php } else { ?>
					background-image: url("<?php echo get_template_directory_uri().'/images/patterns/'; echo $pattern; ?>.png");
				<?php } ?>
			}
		<?php } ?>
		
	</style>
<?php }

add_action( 'customize_controls_print_scripts',  'handbook_customize_toggler', 999);

///////////////////////////////////////////////////////////////////////////////////
// Setup Defaults
///////////////////////////////////////////////////////////////////////////////////

function initial_handbook_setup(){
	$options = get_option('handbook_theme_options');
	$ajaxed_comments = esc_attr( get_option( 'ajaxed_comments', '' ) );

	if($options == ''){
		$defaults = array(

			// Color & Style Options
			'main_color' => '#F77A61',
			'header_version' => 'light',
			'use_uppercase' => true,
			'background_color' => '#F1F1F1',

			// Site Title & Logo
			'logo' => '',
			'title_text' => '',
			'description_text' => '',
			'favicon_image_upload' => '',

			// Layout Options
			'show_slider' => true,
			'show_slider_blog' => false,
			'show_sidebar' => true,
			'sidebar_position' => 'right',
			'boxed_layout' => true,

			// Background Pattern
			'background_pattern' => 'dots',

			// Background Image
			'background_image' => '',

			// Portfolio Settings
			'portfolio_view_all' => __( 'View All', 'handbook' ),
			'portfolio_number' => '40',
			'portfolio_related' => __( 'RELATED WORK', 'handbook'),
			'portfolio_autoplay' => true,

			// Frontpage Options
			'greeting_word' => __( 'HEY!', 'handbook' ),
			'welcome_text' => __( 'Welcome to Handbook Theme, perfect for showcasing your <a href="#">portfolio</a> and writing an interesting blog about someting interesting and captivating that will people read.', 'handbook' ),
			'fullwidth_message' => __( 'This is an example full width text description, you can remove it if you want but it looks awesome!', 'handbook' ),
			'fullwidth_message_button' => __( 'Contact me &rarr;', 'handbook' ),
			'fullwidth_message_link' => get_home_url(),

			// Footer
			'footer_text' => __( 'Handbook Theme designed & developed by Natko.', 'handbook' ),

			// Login Image
			'login_image' => '',

			// Additional CSS
			'additional_css' => '',

		);
		add_option('handbook_theme_options', $defaults);
	}

	if(!isset($ajaxed_comments)){
		update_option( 'ajaxed_comments', '1' );
	}
}

add_action( 'after_setup_theme', 'initial_handbook_setup' );

?>