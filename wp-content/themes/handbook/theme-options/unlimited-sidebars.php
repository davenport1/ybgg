<?php 

///////////////////////////////////////////////////////////////////////////////////////
// Page For Creating New Sidebars
///////////////////////////////////////////////////////////////////////////////////////

function add_unlimited_sidebars() {
	$options = get_option('handbook_theme_options'); 
	if($options['show_sidebar'] == true){
		add_theme_page('Sidebars', 'Sidebars', 'edit_theme_options', 'handbook-sidebars', 'unlimited_sidebars');
	}
}

add_action('admin_menu', 'add_unlimited_sidebars');


function init_unlimited_sidebars(){
	register_setting( 'unlimited_sidebars_settings', 'unlimited_sidebars_settings');
	add_settings_section('unlimited_sidebars_section', '', '', 'handbook-sidebars');
	add_settings_field('settings_unlimited_sidebars', '', 'settings_unlimited_sidebars', 'handbook-sidebars', 'unlimited_sidebars_section');
}

add_action('admin_init', 'init_unlimited_sidebars');


function settings_unlimited_sidebars() {
	$options = get_option('unlimited_sidebars_settings');

	if (isset($_GET['settings-updated'])) {
		if ($_GET['settings-updated'] == true){ ?>
			<div id="message" class="updated below-h2">
				<p><?php echo __('Sidebar settings updated.', 'handbook'); ?></p>
			</div>
		<?php }
	} ?>

	<div id="add-new-item">
		<input class="new-item-name" autocomplete="off" type="text" placeholder="<?php echo __('Sidebar name...', 'handbook'); ?>" />
		<input class="add-item button button-primary button-large" type="submit" value="<?php echo __('Add Sidebar', 'handbook'); ?>" />
	</div>

	<div class="repeatables-wrap sortable-wrap">

		<input name="unlimited_sidebars_settings" type="hidden" class="item-name-input" value="" />

		<div id="dummy-item-placeholder" class="item-holder hidden">
			<div class="repeatable-name">
				<div class="item-move"></div>
				<h4></h4>
			</div>
			<div class="item-info">
				<input autocomplete="off" placeholder="<?php echo __('Sidebar name...', 'handbook'); ?>" name="[name]" type="text" class="item-name-input" value="" />
				<a class="delete-single-repeat" href=""><?php echo __('Delete Sidebar', 'handbook'); ?></a>
			</div>
		</div>

		<?php if(!$options){ ?>

			<h4 class="no-items"><?php echo __('There are no extra sidebars currently, add one!', 'handbook'); ?></h4>

		<?php } else { ?>

			<?php foreach ($options as $sidebar_id => $sidebar_name) { ?>

				<div id="<?php echo $sidebar_id ?>" class="item-holder">
					<div class="repeatable-name">
						<div class="item-move"></div>
						<h4><?php echo $sidebar_name['name']; ?></h4>
					</div>
					<div class="item-info">
						<input autocomplete="off" placeholder="<?php echo __('Sidebar name...', 'handbook'); ?>" name="unlimited_sidebars_settings[<?php echo $sidebar_id; ?>][name]" type="text" class="item-name-input" value="<?php echo $sidebar_name['name']; ?>" />
						<a class="delete-single-repeat" href=""><?php echo __('Delete Sidebar', 'handbook'); ?></a>
					</div>
				</div>
			<?php } ?>

		<?php } ?>

	</div>
	<?php
}

function unlimited_sidebars(){

	global $post;

	if (!current_user_can('manage_options')) {  
		wp_die( __('You do not have sufficient permissions to access this page.', 'handbook') );  
	}

	wp_register_script( 'script-backend', get_template_directory_uri() . '/theme-options/script/script-repeatable.js' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'jquery-ui-sortable' ); 
	wp_enqueue_script( 'script-backend' );

	wp_register_style( 'admin-backend-style', get_template_directory_uri() . '/theme-options/style/style-repeatable.css' );
	wp_enqueue_style( 'admin-backend-style' );

	if (is_rtl()){
		wp_register_style( 'admin-backend-style-rtl', get_template_directory_uri() . '/theme-options/style/style-repeatable-rtl.css' );
		wp_enqueue_style( 'admin-backend-style-rtl' );
	}

	wp_localize_script( 'script-backend', 'handbookRepeatables', array(
		'alert' => __('Are you sure you want to delete this sidebar?', 'handbook'),
		)
	);

	?>

	<div class="wrap">
		<div id="icon-themes" class="icon32">
			<br>
		</div>
		<h2><?php echo __('Sidebars', 'handbook'); ?></h2>
		<p><?php echo __('Add a new sidebar which you can then use on any post or page by selecting it from the "Sidebars" metabox. Number of sidebars is unlimited.', 'handbook'); ?></p>

		<form action="options.php" method="post" class="repeatable-form unlimited-sidebars-form">
			<?php settings_fields('unlimited_sidebars_settings'); ?>
			<?php do_settings_sections('handbook-sidebars'); ?>
			<input class="button button-primary button-large" type="submit" value="<?php echo __('Save Sidebars', 'handbook'); ?>" name="save">
		</form>
	</div>
<?php }

///////////////////////////////////////////////////////////////////////////////////////
// Add Sidebars Metabox
///////////////////////////////////////////////////////////////////////////////////////

$handbook_options = get_option('handbook_theme_options');

if($handbook_options['show_sidebar'] == true){

	add_action( 'add_meta_boxes', 'choose_sidebar_metabox_add' );

	function choose_sidebar_metabox_add(){
		add_meta_box( '_choose-sidebar', 'Sidebar', 'choose_sidebar_metabox', 'post', 'side' );
		add_meta_box( '_choose-sidebar', 'Sidebar', 'choose_sidebar_metabox', 'page', 'side' );
	}

	function choose_sidebar_metabox( $post ){

		$selected_value = get_post_meta( $post->ID, '_choose_sidebar', true );

		if(isset($selected_value) == false){
			$selected_value = 'sidebar-main';
		}

		$sidebars = $GLOBALS['wp_registered_sidebars'];
		unset($sidebars['footer-1'], $sidebars['footer-2'], $sidebars['footer-3']);

		wp_nonce_field( 'choose-sidebar-nonce', 'choose-sidebar-check' ); ?>

		<select name="choose_sidebar" id="choose_sidebar" style="width:100%;">

			<?php foreach ( $sidebars as $sidebar ) { ?>
				<option value="<?php echo $sidebar['id']; ?>" <?php selected( $selected_value, $sidebar['id'] ); ?> >
					<?php echo $sidebar['name']; ?>
				</option>
			<?php } ?>

			<option value="no-sidebar" <?php selected( $selected_value, 'no-sidebar' ); ?> >
				<?php echo __('&lt;no sidebar&gt;', 'handbook'); ?>
			</option>
			
		</select>
	<?php }

	add_action( 'save_post', 'choose_sidebar_metabox_save' );

	function choose_sidebar_metabox_save($post_id){

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return; 
		if( !isset( $_POST['choose-sidebar-check'] ) || !wp_verify_nonce( $_POST['choose-sidebar-check'], 'choose-sidebar-nonce' ) ) return; 
		if( !current_user_can( 'edit_post', $post_id ) ) return;  

		if( isset( $_POST['choose_sidebar'] )){
			update_post_meta( $post_id, '_choose_sidebar', esc_attr( $_POST['choose_sidebar'] ) );  
		}

	}

}


///////////////////////////////////////////////////////////////////////////////////////
//	If the current post/page doesn't have a sidebar
///////////////////////////////////////////////////////////////////////////////////////

function handbook_sidebar_body_class($classes) {

	$sidebar_id = '';

	if(is_singular()){
		global $post;
		$sidebar_id = get_post_meta( $post->ID, '_choose_sidebar', true );
	}

	if($sidebar_id == 'no-sidebar'){
		$remove_classes = array('sidebar-on-right', 'sidebar-on-left');
		$classes = array_diff($classes, $remove_classes);
		$classes[] = 'sidebar-off';
	}

	return $classes;

}

add_filter('body_class','handbook_sidebar_body_class');

?>