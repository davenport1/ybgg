<?php
/*
Template Name: Front Page Grid
*/

	get_header();

	$options = get_option('handbook_theme_options');
	$has_thumbnail = false;
	$show_slider = false;

	if(is_front_page() && !is_paged() && $options['show_slider'] == true){
		$show_slider = true;
	}

	// Get Portfolio Tags/Categories
	$portfolio = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => -1 ) ); $i = 0; $final = array(); 
	while ( $portfolio->have_posts() ) : $portfolio->the_post();
		if(has_post_thumbnail()){
			$has_thumbnail = true;
		}
	endwhile;

	$tags_unique = array_count_values($final);
	wp_reset_query();
?>

	<div class="welcome-message" style="background-color:#fcf478; border:10px solid #f9ed39; padding:15px">
    <span>WELCOME</span>
		<p>We'd love to help you get a great deal on tickets or workshops for your group.<br>View a list of shows <a href="http://offbroadwaybrainstormers.com/groups/shows/">here</a> or call us at 877-943-BWAY (2929)! For additional information call: 877-943-BWAY (2929)</p>
		
	</div>

	<?php if($show_slider == true){
		get_template_part( 'slider', 'template' );
	} ?>

<div class="portfolio-details hidden">
		<div class="portfolio-nav portfolio-archive">
			<a href="" class="close-portfolio">X</a>
			<a href="" class="prev-portfolio ajax-enabled" data-id="">&lt;</a>
			<a href="" class="next-portfolio ajax-enabled" data-id="">&gt;</a>
		</div>
		<span class="dash"></span>
		<div class="portfolio-details-content fadein"></div>
	</div>

	<div class="clear"></div>

	<!-- portfolio-filter-->
	<div class="portfolio-filter">
		
		<?php if($has_thumbnail == true){ ?>
			<div class="clear"></div>
			<div class="splitter-vertical"><span></span></div>
			<ul id="items" class="portfolio-list grid-layout">
				<?php while ( $portfolio->have_posts() ) : $portfolio->the_post();
					if(has_post_thumbnail()){ ?>
						<li>
							<a href="<?php the_permalink(); ?>" data-id="<?php the_ID(); ?>">
								<span class="view-more-icon"></span>
								<?php the_post_thumbnail('portfolio-thumbnail-big'); ?>
							</a>
							<h1><a href="<?php the_permalink(); ?>" data-id="<?php the_ID(); ?>"><?php the_title(); ?></a></h1>
							<?php the_excerpt(); ?>
							<div class="splitter"></div>
						</li>

					<?php }
				endwhile; ?>
			</ul>
		<?php } else if($has_thumbnail == false && current_user_can('edit_pages')){
			if(function_exists('portfolio_plus_exists_check')){ ?>
				<div class="admin-portfolio-message">
					<p><?php echo __("Currently there are no portfolio items with a thumbnail, why don't you go ahead and add one?", 'handbook'); ?></p>
					<a href="<?php echo admin_url('post-new.php?post_type=portfolio'); ?>" class="more-link"><?php echo __('Add portfolio item', 'handbook'); ?></a>
				</div>
			<?php } else { ?>
				<div class="admin-portfolio-message">
					<p><?php echo __('To use the portfolio feature you need to install Portfolio Plus plugin that comes with the theme in the "plugins" folder.', 'handbook'); ?></p>
				</div>
			<?php } ?>
		<?php } ?>

		<div class="clear"></div>

	</div> <!-- end portfolio-filter -->

	<?php if(function_exists('testimonials_plus_exists_check')){ 
		get_template_part( 'testimonials', 'template' );
	} ?>

	<?php if($options['fullwidth_message'] != ''){ ?>
		<!-- fullwidth-message -->
		<div class="fullwidth-message">
			<p><?php echo $options['fullwidth_message']; ?></p>
			<?php if($options['fullwidth_message_link'] != ''){
				echo '<a href="'. $options['fullwidth_message_link'] .'">' . $options['fullwidth_message_button'] . '</a>';
			} ?>
		</div> <!-- end fullwidth-message -->
	<?php } ?>

<?php get_footer(); ?>