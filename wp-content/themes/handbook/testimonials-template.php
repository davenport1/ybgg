<?php
/**
 * The template for displaying the testimonials
 *
 */

// Testimonials loop
if (post_type_exists('testimonials')){
	$testimonials = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => 9 ) );
} 
?>

<!-- testimonials -->
<div class="flexslider testimonials">
	<span class="dash"></span>
	<ul class="slides">
		<?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
			<li>
				<?php the_post_thumbnail('thumbnail'); ?>
				<h3><?php the_title(); ?></h3>
				<div class="testimonial">
					<?php 
						global $more;
						$more = 0;
						the_content('');
					?>
				</div>
			</li>
		<?php endwhile; ?>
	</ul>
</div> <!-- end testimonials -->