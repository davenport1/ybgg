<?php 
	get_header(); 

	$options = get_option('handbook_theme_options'); 

	global $withcomments; 
	$withcomments = true;

	$show_slider = false;

	if(!is_paged() && $options['show_slider_blog'] == true){
		$show_slider = true;
	}
?>

	<?php if($show_slider == true){ ?>

		<?php get_template_part( 'slider', 'template' ); ?>

	<?php } ?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php get_template_part( 'content', get_post_format() ); ?>

		<?php endwhile; ?>

		<?php else : ?>

			<div class="page-info">
				<h1><?php echo __('No items found', 'handbook') ?></h1>
			</div>
			
		<?php endif; ?>

		<?php handbook_pagination('', 3); ?>

		</div> <!-- end content -->
	</div> <!-- end primary-content -->

	<?php if($options['show_sidebar'] == true){
		get_sidebar();
	} ?>

<?php get_footer(); ?>