<?php

////////////////////////////////////////////////////////////////////
//	Template for displaying Image post format
////////////////////////////////////////////////////////////////////

$post_id = get_the_ID();
?>

	<article id="post-<?php echo $post_id; ?>" data-id="<?php echo $post_id; ?>" <?php post_class(); ?>>

		<h1 class="post-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</h1>

		<p class="post-meta">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_time(get_option('date_format')); ?></a> 
			<span>/</span>
			<span class="category-list">
				<?php the_category( ',' ); ?>
			</span>
			<span>/</span>
			<a href="<?php comments_link(); ?>"><?php comments_number(); ?></a>
			<?php if (function_exists('lp_love_post_link')){ lp_love_post_link($post_id); } ?>
		</p>

		<?php the_tags('<ul class="tags"><li>','</li><li>','</li></ul>'); ?>

		<div class="image-content">
			<?php if ( has_post_thumbnail() ) {
				the_post_thumbnail();
			} else {
				$continue_text = __('Continue reading...', 'handbook'); 
				the_content('<span class="more-text">'.$continue_text.'</span>');
			}
			?>
		</div>

		<?php if(has_excerpt()){ ?>
			<div class="excerpt">
				<?php the_excerpt(); ?>
			</div>
		<?php } ?>

		<div class="clear"></div>

		<?php if (is_single()){ get_template_part( 'author', 'infobox' ); } ?>

		<?php if(is_single()){
			comments_template( '', true ); 
		} ?>

		<span class="dash"></span>
		
	</article>