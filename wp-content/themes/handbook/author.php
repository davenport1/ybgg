<?php

////////////////////////////////////////////////////////////////////
//	Template for displaying the author page
////////////////////////////////////////////////////////////////////

get_header();

$options = get_option('handbook_theme_options');

// Get author info
$authordata = get_userdata(get_query_var('author'));

?>

	<?php get_template_part( 'author', 'infobox' ); ?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

			<?php endwhile; ?>

			<?php else : ?>

				<p><?php echo __('No results found.', 'persona') ?></p>
				
			<?php endif; ?>

			<?php if (function_exists('handbook_pagination')) {
				handbook_pagination('', 3);
			} else {
				//posts_nav_link();
			} ?>

		</div> <!-- end content -->
	</div> <!-- end container -->

	<?php if($options['show_sidebar'] == true){
		get_sidebar();
	} ?>

<?php get_footer(); ?>