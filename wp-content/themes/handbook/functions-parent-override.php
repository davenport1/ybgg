<?php
remove_action( 'pre_gbs_head', 'gb_redirect_from_home' );
add_action( 'pre_gbs_head', 'gb_redirect_away_from_home' );
function gb_redirect_away_from_home() {
    if ( is_home()) {
        wp_redirect( gb_get_deals_link() );
        exit();
    }
} 

add_action( 'init', 'remove_actions' );
function remove_actions() {
    remove_action( 'gb_account_register_form_controls', array( 'Group_Buying_Facebook_Connect', 'show_registration_option' ) );
}
add_action( 'gb_account_register_form_controls', 'new_reg_option' );
function new_reg_option() {
    echo '<fb:login-button id="facebook_registration_button" class="facebook_button clearfix">';
    echo Group_Buying_Facebook_Connect::button( 'Login with Facebook' );
    echo '</fb:login-button><!-- #facebook_registration_button.facebook_button-->';
}  

remove_action( 'pre_gbs_head', 'gb_redirect_from_home' );
add_action( 'pre_gbs_head', 'gb_redirect_away_from_home' );

//Gets post cat slug and looks for single-[cat slug].php and applies it
add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(STYLESHEETPATH . "/single-{$cat->slug}.php") )
		return STYLESHEETPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);

function new_excerpt_more( $more ) {
	return '... <h4><a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', '_s' ) . '</a></h4>';
}
add_filter('excerpt_more', 'new_excerpt_more');