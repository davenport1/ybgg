//////////////////////////////////////////////////////
// Handbook JS Admin
//////////////////////////////////////////////////////

jQuery(document).ready(function($) {

	function trimspace(str) {
		return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}

	//////////////////////////////////////////////////////
	// Widgets Order
	//////////////////////////////////////////////////////

	if (typeof sidebars == 'undefined'){
		sidebars = '';
	}

	var postData = sidebars;

	$('a.widget-order-handle').on('click', function(e){
		e.preventDefault();
	});

	$('#sidebar').sortable({
		connectWith: '#sidebar',
		placeholder: 'sortable-widget-placeholder',
		handle: '.widget-order-handle',
		start: function(e,ui){
			ui.placeholder.height(ui.item.height());
		},
		stop: function(){
			var new_order = '';
			var sidebar_id = 'sidebars['+ $('#sidebar').data('id') + ']';

			$('#sidebar aside.widget').each(function(index, value){
				new_order = new_order + 'widget-0_' + $(this).attr('id') +',';
			});
			
			new_order = new_order.slice(0, -1);

			postData.action = 'widgets-order';
			postData.savewidgets = $('#_wpnonce_widgets').val();

			postData[sidebar_id] = new_order;

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: handbook.ajaxurl,
				data: postData
			});
		}
	});

});