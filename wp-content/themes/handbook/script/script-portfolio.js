//////////////////////////////////////////////////////
// Handbook Portfolio JS
//////////////////////////////////////////////////////

jQuery(document).ready(function($) {

	var $container = $('.portfolio-list');
	var $splitter  = $('.splitter-vertical');

	if($('html').attr('dir') == 'rtl'){
		$.Isotope.prototype._positionAbs = function( x, y ) {
			return { right: x, top: y };
		};
		function isotopeInit() {
			$container.isotope({
				transformsEnabled: false
			});
		}
	} else {
		function isotopeInit() {
			$container.isotope({
				animationEngine : 'css',
			});
		}
	}

	$container.imagesLoaded( function(){
		isotopeInit();
	});

	$(window).resize( function() { 
		isotopeInit();
		$splitter.height($('ul.portfolio-list').height()-60);
	});

	setTimeout(function(){
		$splitter.height($('ul.portfolio-list').height()-60);
	}, 1000);


	//////////////////////////////////////////////////////
	// Isotope filter active/inactive
	//////////////////////////////////////////////////////

	$('.portfolio-keywords a').on('click', function(e){
		e.preventDefault();
		$this = $(this);

		$('.portfolio-keywords li.active').removeClass('active');
		$this.parent().addClass('active');

		var selector = $this.attr('data-filter');
		$container.isotope({ filter: selector });
	});

	//////////////////////////////////////////////////////
	// AJAX portfolio load
	//////////////////////////////////////////////////////

	function handbook_get_portfolio_item(postID, postURL){
		var $body             = $('body');
		var $nextItem         = $('.portfolio-nav a.next-portfolio');
		var $prevItem         = $('.portfolio-nav a.prev-portfolio');
		var $portfolioWrap    = $('.portfolio-details');
		var $portfolioContent = $('.portfolio-details-content');
		var $portfolioRelated = $('.portfolio-similar-items');

		if($body.hasClass('single-portfolio')){
			var postShort = false;
		} else {
			var postShort = true;
		}

		$portfolioWrap.removeClass('hidden').addClass('loading show');
		$portfolioContent.addClass('loading');
		$portfolioRelated.addClass('loading');
		if($portfolioContent.height() != 0){
			$portfolioContent.css('height', $portfolioContent.height());
		}

		if (typeof handbookPortfolio.autoplay !== 'undefined'){
			if(handbookPortfolio.autoplay == 'true'){
				var autoplay = true;
			} else {
				var autoplay = false;
			}
		} else {
			var autoplay = true;
		}

		var postData = {
			'security': handbook.nonce,
			'action': 'handbook_get_portfolio_item',
			'post_ID': postID,
			'post_URL': postURL,
			'post_short': postShort,
		};

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: handbook.ajaxurl,
			data: postData,
			success: function(data){
				$portfolioContent.empty().append(data.portfolio_html);

				$('.fullscreen-gallery').swipebox({ 
					hideBarsDelay : 0,
					supportSVG : false,
				});

				$portfolioContent.css('height', 'auto');
				$('.portfolio-details-content .flexslider').css('opacity', '0');

				if($body.hasClass('single-portfolio')){
					var portfolioRelatedItems = $('.portfolio-similar-items > ul > li a');
					for(var i = 0; i < 4; i++){
						$(portfolioRelatedItems[i]).attr('data-id', data.related_id[i]).data('id', data.related_id[i]);
						$(portfolioRelatedItems[i]).attr('href', data.related_url[i]);
						$(portfolioRelatedItems[i]).children('img').attr('src', data.related_thumbnail[i]);
					}
				}

				$('.portfolio-details-content .gallery ul.slides li:first-child').imagesLoaded( function() {
					$('.portfolio-details-content .flexslider').flexslider({
						easing:       'easeOutQuad',
						animation:    'slide',
						namespace:    'slider-',
						pauseOnHover: true,
						smoothHeight: true,
						slideshow: autoplay,
						video: true,
						useCSS: autoplay,
					});
					setTimeout(function(){
						$portfolioWrap.removeClass('loading');
						$portfolioContent.removeClass('loading'); 
						$portfolioRelated.removeClass('loading'); 
						$('.portfolio-details-content .flexslider').animate({ opacity: 1 }, 200 );
					}, 50);
				});


				if($body.hasClass('single-portfolio') && typeof postURL === 'undefined'){
					history.pushState('', data.post_title, data.post_link);
					document.title = data.document_title;
				} else if($body.hasClass('single-portfolio')) {
					history.replaceState('', data.post_title, data.post_link);
					document.title = data.document_title;
				}
				if(data.next_id == ''){
					$nextItem.addClass('disable-nav');
				} else {
					$nextItem.removeClass('disable-nav');
				}
				if(data.prev_id == ''){
					$prevItem.addClass('disable-nav');
				} else {
					$prevItem.removeClass('disable-nav');
				}
				$nextItem.attr('data-id', data.next_id).data('id', data.next_id);
				$prevItem.attr('data-id', data.prev_id).data('id', data.prev_id);
				$('.portfolio-nav a.prev-portfolio, .portfolio-nav a.next-portfolio, .portfolio-similar-items a').addClass('ajax-enabled');
			}
		});
	}

	$('body').on('click', '.portfolio-nav a', function(e){
		if($(this).hasClass('portfolio-back') == false){
			e.preventDefault();
		}
	});

	$('body').on('click', '.portfolio-list li a, .portfolio-nav a.ajax-enabled, .portfolio-similar-items a.ajax-enabled', function(e){
		e.preventDefault();
		var $this = $(this);
		var postID = $this.data('id');

		if(postID == ''){ return; }

		$('.portfolio-nav a, .portfolio-similar-items a').removeClass('ajax-enabled');
		handbook_get_portfolio_item(postID);
	});

	//////////////////////////////////////////////////////
	// Scroll to the item details
	//////////////////////////////////////////////////////

	$('body').on('click', '.portfolio-list li a, .portfolio-similar-items a', function(e){
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('.portfolio-details').offset().top-100
		}, 250, 'easeOutQuad');
	});

	//////////////////////////////////////////////////////
	// Close the box
	//////////////////////////////////////////////////////

	$('body').on('click', '.portfolio-nav a.close-portfolio', function(e){
		e.preventDefault();
		var $this = $(this);

		$('.portfolio-details.show').slideUp( 300 , function() {
			$(this).removeClass('show loading').addClass('hidden');
			$(this).css('display', '');
		});
	});

	//////////////////////////////////////////////////////
	// Next/Previous browser navigation
	//////////////////////////////////////////////////////

	window.onpopstate = function(event) {
		if($('body').hasClass('single-portfolio')){
			handbook_get_portfolio_item('', location.pathname);
		}
	};
	
});