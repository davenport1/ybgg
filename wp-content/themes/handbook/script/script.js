//////////////////////////////////////////////////////
// Handbook JS
//////////////////////////////////////////////////////

jQuery(document).ready(function($) {

	$('.fullscreen-gallery').swipebox({ 
		hideBarsDelay : 0,
		supportSVG : false,
	});

	//////////////////////////////////////////////////////
	// Mobile Menu Toggle
	//////////////////////////////////////////////////////

	$('body').on('click', '.menu-toggle.show', function(e){
		e.preventDefault();
		$(this).removeClass('show').addClass('close');

		var $menu = $('#header #nav-menu').clone(true);
		$('body').prepend($menu);

		$menu.children('ul.sub-menu').prepend('<a href="">back</a>');
		$('.container').addClass('push-back');
		
		$('body > .wrapper').prepend('<div class="gray-overlay"></div>');

		setTimeout(function(){ 
			$('body').addClass('disable-scroll');
			$('.gray-overlay').addClass('show');
		}, 20);
		
		setTimeout(function(){ $menu.addClass('show');  }, 100);

		var $menu_parents = $('body > #nav-menu .sub-menu').siblings('a');

		//////////////////////////////////////////////////////
		// Duplicate the parent if href is not empty or #
		//////////////////////////////////////////////////////

		for(var i = 0, l = $menu_parents.length; i < l; i++) {
			var $current_parent = $($menu_parents[i]);
			if ($current_parent.attr('href') != '' && $current_parent.attr('href') != '#'){
				menu_class = $current_parent.parent('li').attr('class');
				var $cloned_parent = $current_parent.clone(true);
				var $wraped_parent = $cloned_parent.wrap('<li class="'+menu_class+'"></li>').parent().removeClass('submenu-parent');
				$current_parent.siblings('.sub-menu').prepend($wraped_parent);
			}
		}
		
	});


	$('body').on('click', '#nav-menu.show li.submenu-parent > a', function(e){
		e.preventDefault();
		var $this = $(this);

		var $submenu = $this.siblings('ul');
		$submenu.addClass('show');
		setTimeout(function(){ $submenu.addClass('fadein'); }, 20);

		$this.addClass('hide');

		var $parents = $this.parent().siblings().addBack();

		for(var i = 0; i < $parents.length; i++){
			$($parents[i]).addClass('hide');
		}

	});

	$('body').on('click', '.menu-toggle.close', function(e){
		e.preventDefault();
		$(this).removeClass('close').addClass('show');

		$('body > #nav-menu').remove();
		$('body').removeClass('disable-scroll');

		setTimeout(function(){ 
			$('.container').removeClass('push-back push-front');
		}, 10);

		$('.gray-overlay').remove();

	});

	//////////////////////////////////////////////////////
	// Slider
	//////////////////////////////////////////////////////

	if (typeof handbookPortfolio.autoplay !== 'undefined'){
		if(handbookPortfolio.autoplay == 'true'){
			var autoplay = true;
		} else {
			var autoplay = false;
		}
	} else {
		var autoplay = true;
	}

	$('.portfolio-details-content .gallery ul.slides li:first-child').imagesLoaded( function() {
		$('.portfolio-details-content .flexslider').flexslider({
			easing:       'easeOutQuad',
			animation:    'slide',
			namespace:    'slider-',
			pauseOnHover: true,
			smoothHeight: true,
			slideshow: autoplay,
			video: true,
			useCSS: autoplay,
		});
	});
	
	$('.gallery-embed .flexslider').flexslider({
		easing:       'easeOutQuad',
		animation:    'slide',
		namespace:    'slider-',
		pauseOnHover: true,
		slideshow: false,
		easing: 'swing',
		controlNav: false,
		animationLoop: false,
		itemWidth: 100,
		itemMargin: 15,
		minItems: 2,
		maxItems: 6,
	});

	$('.flexslider').imagesLoaded( function() {
		$('.flexslider').flexslider({
			easing:       'easeOutQuad',
			animation:    'slide',
			namespace:    'slider-',
			pauseOnHover: true,
			slideshow: false,
			easing: 'swing',
			controlNav: false,
			animationLoop: true,
			smoothHeight: true,
			video: true,
			useCSS: false,
		});
	});

	$('.widget-testimonials-plus').flexslider({
		easing:       'easeOutQuad',
		animation:    'slide',
		namespace:    'slider-',
		selector:     '.testimonials-plus-list > li',
		pauseOnHover: true,
		slideshow: true,
		controlNav: true,
		directionNav: false,
		smoothHeight: true,
	});

	$('.widget-portfolio-plus').imagesLoaded( function() {
		$('.widget-portfolio-plus').flexslider({
			easing:       'easeOutQuad',
			animation:    'slide',
			namespace:    'slider-',
			selector:     '.portfolio-plus-list > li',
			pauseOnHover: true,
			slideshow: false,
			controlNav: false,
			directionNav: true,
			smoothHeight: true,
		});
	});
	

	$('a.gallery-thumbnail').on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		if($this.hasClass('active')){
			return false;
		}

		$this.parent().siblings().children('a.gallery-thumbnail.active').removeClass('active');
		$this.addClass('active');
		var url = $this.attr('href');
		var $big = $this.parents('.carousel').prev('.current-image');
		$big.removeClass('gallery-fade-in').addClass('gallery-fade-out');
		$big.children('img.loaded-image').attr('src', url);

		$('.current-image img.loaded-image').imagesLoaded( function() {
			setTimeout(function(){ 
				$big.children('img').attr('src', url);
				if($this.data('caption')){
					$big.children('div.caption').children('p').html($this.data('caption'));
					$big.children('div.caption').removeClass('hidden').show();
				} else {
					$big.children('div.caption').hide();
				}
				$big.removeClass('gallery-fade-out').addClass('gallery-fade-in'); 
			}, 150);
		});
		
	});


	//////////////////////////////////////////////////////
	// Accordion
	//////////////////////////////////////////////////////

	$('.accordion-wrapper h4').on('click', function(){
		var $this = $(this);
		$this.siblings('.accordion-content').stop().slideToggle(300);
		$this.toggleClass('expanded');
	});


	//////////////////////////////////////////////////////
	// Show Pingbacks
	//////////////////////////////////////////////////////

	$('a.show-pingbacks').on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('pingbacks-visible').siblings('.pingbacks-trackbacks').slideToggle();
		if($this.hasClass('pingbacks-visible')){
			$this.text('('+$this.data('text-hide')+')');
		} else {
			$this.text('('+$this.data('text-show')+')');
		}
	});

	//////////////////////////////////////////////////////
	// Comment Form
	//////////////////////////////////////////////////////

	function valid(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	}

	function trimspace(str) {
		if(str !== undefined){
			return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		}
	}

	$('.comment-box').on('focusin', function(){
		var $this = $(this);
		$this.parent().addClass('show-box');
		$this.siblings('span').addClass('in-focus');
	});

	$('.comment-box').on('focusout', function(){
		var $this = $(this);
		if($this.val() == ''){
			$this.parent().removeClass('show-box');
		}
		$this.siblings('span').removeClass('in-focus');
	});

	$('.comment-reply-link').on('click', function(){
		$(this).nextAll('.comment-box').focus();
	});

	if($('body').hasClass('disable-ajaxed-comments') == false){

		//////////////////////////////////////////////////////
		// Comment Pagination
		//////////////////////////////////////////////////////

		$('body').on('click', '.comment-nav-below .nav-next a, .comment-nav-below .nav-previous a', function(e){
			e.preventDefault();
			var $this = $(this);
			var getPage;

			var $currentPage = $this.parents('.comment-nav-below').find('.current-comment-page');
			var $commentList = $this.parents('.comment-nav-below').siblings('.comment-list');
			var currentPage  = parseInt($currentPage.data('current-page'), 10);
			var topPage      = $this.parents('.comment-nav-below').find('.top-comment-page').data('top-page');

			$('ul.comment-list .comment-respond').insertAfter($('.comment-list'));

			$commentList.append('<li class="loading-overlay"></li>');
			var $overlay = $commentList.children('li.loading-overlay');

			if($this.parent().hasClass('nav-previous')){
				$this.parent().siblings('.nav-next').children('a').removeClass('hide');
				if(currentPage == 2){
					$this.addClass('hide');
				} else {
					$this.removeClass('hide');
				}
				$currentPage.html(currentPage-1).attr('data-current-page', currentPage-1).data('current-page', currentPage-1);
				getPage = currentPage-1;
			} else {
				$this.parent().siblings('.nav-previous').children('a').removeClass('hide');
				if(currentPage == topPage-1){
					$this.addClass('hide');
				} else {
					$this.removeClass('hide');
				}
				$currentPage.html(currentPage+1).attr('data-current-page', currentPage+1).data('current-page', currentPage+1);
				getPage = currentPage+1;
			}

			var postData = {
				'security': handbook.nonce,
				'action': 'handbook_get_comment_page',
				'get_page': getPage,
				'post_ID': $this.parents('article').data('id'),
			};

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: handbook.ajaxurl,
				data: postData,
				success: function(data){
					$('.comment-list li:not(:last)').remove();
					$('.comment-list').html(data.comments);
					$overlay.addClass('fade-out');
					setTimeout(function(){ $overlay.remove(); }, 300);
				}
			});
		});

		//////////////////////////////////////////////////////
		// Add Comment
		//////////////////////////////////////////////////////

		$('body').on('submit', '.comment-form', function(e){
			e.preventDefault();
			var $this = $(this);

			var $commentBox  = $this.find('.comment-box');
			var $authorBox   = $this.children('.author');
			var $emailBox    = $this.children('.email');
			var $urlBox      = $this.children('.url');
			var $respond     = $this.parents('#respond');
			var $info        = $this.children('em.info');
			var $parent      = $this.find('#comment_parent');

			var comment      = trimspace($commentBox.val());
			var author       = trimspace($authorBox.val());
			var email        = trimspace($emailBox.val());
			var url          = trimspace($urlBox.val());
			var parent       = $parent.val();
			var postID       = $this.parents('article').data('id');
			var message      = $info.html();

			if( $('body').hasClass('logged-in') == true && comment || comment && author && email){

				if( $('body').hasClass('logged-in') == true || valid(email) ){

					$respond.prepend('<li class="loading-overlay"></li>');
					var $overlay = $respond.children('li.loading-overlay');
					
					var postData = {
						'security': handbook.nonce,
						'action': 'handbook_add_comment',
						'comment_post_ID': postID,
						'comment_author': author,
						'comment_author_email': email,
						'comment_author_url': url,
						'comment_parent': parent,
						'comment_content': comment,
					};

					$respond.prepend('<li class="loading-overlay"></li>');
					var $overlay = $respond.children('li.loading-overlay');

					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: handbook.ajaxurl,
						data: postData,
						success: function(data){
							if(data.inserted == true){
								if($respond.parent('li').length > 0){
									if($respond.siblings('.children').length == 0){
										$respond.after('<ul class="children"></ul>');
									}
									$respond.siblings('.children').append(data.comment);
									setTimeout(function(){ 
										$respond.siblings('.children').children('li.comment.hidden').addClass('fade-in');
										$respond.find('a#cancel-comment-reply-link').hide();
										$('ul.comment-list').after($respond);
									}, 1);
									$parent.val('0');
								} else {
									$respond.siblings('ul.comment-list').append(data.comment);
									setTimeout(function(){ $respond.siblings('ul.comment-list').children('li.comment.hidden').addClass('fade-in'); }, 1);
								}
								$commentBox.val('');
								$overlay.remove();
								$info.removeClass('error');
							}
						},
						error: function(data){
							$info.addClass('error').html(data.responseText);
							setTimeout(function(){ $info.addClass('fade-out'); }, 6000);
							setTimeout(function(){ $info.html(message).removeClass('error fade-out'); }, 7000);
							$overlay.remove();
						}
					});


				} else {
					$.each([$commentBox, $authorBox], function() { $(this).removeClass('error') });
					$emailBox.addClass('error');
				}

			} else {
				
				$.each([$commentBox, $authorBox, $emailBox], function() {
					$(this).removeClass('error');
					if($(this).val().length == 0){
						$(this).addClass('error');
					}
				});

				if( !valid(email) ){
					$emailBox.addClass('error');
				}

			}

		});
	}

});