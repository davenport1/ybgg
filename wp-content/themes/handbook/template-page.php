<?php
/*
Template Name: Page
*/
?>

<?php get_header();

$options = get_option('handbook_theme_options');

$post_id = get_the_ID();
?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

			<article id="page-<?php the_ID(); ?>" data-id="<?php the_ID(); ?>" <?php post_class(); ?>>

				<h1 class="post-title">
					<?php the_title(); ?>
				</h1>

				<?php if (has_excerpt()){ ?>
			<div class="excerpt">
				<?php the_excerpt(); ?>
			</div>
		<?php } else if (!is_singular()){
			$continue_text = __('Continue reading...', 'handbook'); 
			the_content('<span class="read-more">'.$continue_text.'</span>');
		} ?>

		<?php if(is_singular()){ the_content(); } ?>
        
        <?php if(have_posts()) : while(have_posts()) : the_post();

					global $post;

					$content = get_the_content();

					preg_match_all('/'.$pattern.'/s', $post->post_content, $matches);

					 ?>

					<div>
						<?php 
							$content = apply_filters ('the_content', $content); 
							echo $content; 
						?>
					</div>

				<?php endwhile; endif; ?>

				

			</article>

		</div> <!-- end content -->
	</div> <!-- end container -->

	<?php if($options['show_sidebar'] == true){ ?>

		<?php get_sidebar(); ?>

	<?php } ?>

<?php get_footer(); ?>



