<?php
/**
 * The Template for displaying the search page.
 *
 */

get_header();

$options = get_option('handbook_theme_options'); ?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

		<div class="page-info">
			<h1><?php echo __('Results found: ', 'handbook'); global $wp_query; echo $wp_query->found_posts; ?></h1>
			<?php get_search_form(); ?>
		</div>

		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php if( !is_single() ){ get_template_part( 'content', get_post_format() ); } else { get_template_part( 'content'); }?>

		<?php endwhile; ?>

		<?php else : ?>

			<div class="page-info">
				<h1><?php echo __('No items found', 'handbook') ?></h1>
			</div>
			
		<?php endif; ?>

		<?php handbook_pagination('', 3); ?>

		</div> <!-- end content -->
	</div> <!-- end primary-content -->

	<?php if($options['show_sidebar'] == true){
		get_sidebar();
	} ?>

<?php get_footer(); ?>


