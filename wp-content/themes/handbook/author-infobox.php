<?php 

////////////////////////////////////////////////////////////////////
//	Template for displaying the author info box
////////////////////////////////////////////////////////////////////

$social_urls = array();
$social_urls['Twitter']   = get_the_author_meta('twitter');
$social_urls['Facebook']  = get_the_author_meta('facebook');
$social_urls['Google+']   = get_the_author_meta('gplus');
$social_urls['LinkedIn']  = get_the_author_meta('linkedin');
$social_urls['Dribbble']  = get_the_author_meta('dribbble');
$social_urls['Pinterest'] = get_the_author_meta('pinterest');
$social_urls['Instagram'] = get_the_author_meta('instagram');

foreach ($social_urls as $social_name => $social_url) {
	if($social_url == ''){
		unset ($social_urls[$social_name]);
	} else {
		$social_url_prefix = substr( $social_url, 0, 4 );
		if($social_url_prefix != 'http'){
			$new_url = 'http://' . $social_url;
			$social_urls[$social_name] = $new_url;
		}
	}
} 

?>

<div class="author-infobox">
	<?php echo get_avatar(get_the_author_meta( 'ID' ), 100); ?>
	<div class="author-description">
		<h3><?php echo __('About', 'handbook');?> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a></h3>
		<p><?php the_author_meta('user_description'); ?></p>
		<?php if (!empty($social_urls)){ ?>
			<p>
				<small>
					<?php echo __('Find me here: ', 'handbook'); ?>
					<?php 
						foreach ($social_urls as $social_name => $social_url) {
							echo '<a href="'.$social_url.'">'.$social_name.'</a> /';
						}

					 ?>
				</small>
			</p>
		<?php } ?>
	</div>
	<span class="dash"></span>
	<div class="clear"></div>
</div>
