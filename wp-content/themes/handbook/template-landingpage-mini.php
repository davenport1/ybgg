<?php
/*
Template Name: Landing Page Minimalistic
*/

	get_header();

	$options = get_option('handbook_theme_options');
	$has_thumbnail = false;
	$show_slider = false;
	$total_posts = 0;
	$posts_per_page = '-1';
	$posts_per_page = intval($options['portfolio_number']);

	if(is_front_page() && !is_paged() && $options['show_slider'] == true){
		$show_slider = true;
	}

	// Get Portfolio Tags/Categories
	$portfolio = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => $posts_per_page ) ); $i = 0; $final = array(); 
	while ( $portfolio->have_posts() ) : $portfolio->the_post();
		if(has_post_thumbnail()){
			$total_posts++;
			$has_thumbnail = true;
			//$all_tags = get_the_terms( get_the_ID(), 'portfolio_tag');
			$all_tags = array( 'tag_slug__in' => array( 'Broadway') ) ;
			if($all_tags){
				foreach($all_tags as $tag){
					if ($tag->name == 'Broadway') { 
						$tag_single = $tag->name;
						$final[$i] = $tag_single;
						$i++;
					}
				}
			}
		}
	endwhile;

	$tags_unique = array_count_values($final);
	wp_reset_query();
?>

	<div class="welcome-message" style="background-color:#fcf478; border:10px solid #f9ed39; padding:10px; margin-bottom:50px">
    
		<span>Call us at 855-733-BWAY (2929)!</span><p>We'd love to help you get a great deal on tickets or workshops for your group.<br>
Call us today or submit an inquiry below!</p>
		
	</div>

	<div class="welcome-message" style="background-color:#346cd7; border:10px solid #123b7e; color: #fcf75c; padding:10px; margin-bottom:50px">
    
		<h1 style="color: #fcf75c; text-align: center; font-size: 50px;">$100 OFF YOUR FIRST ORDER!*</h1>

	</div>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

			<article id="page-<?php the_ID(); ?>" data-id="<?php the_ID(); ?>" <?php post_class(); ?>>


        
        <?php if(have_posts()) : while(have_posts()) : the_post();

					global $post;

					$content = get_the_content();

					preg_match_all('/'.$pattern.'/s', $post->post_content, $matches);

					 ?>

					<div>
						<?php 
							$content = apply_filters ('the_content', $content); 
							echo $content; 
						?>
					</div>

				<?php endwhile; endif; ?>

				

			</article>

		</div> <!-- end content -->
	</div> <!-- end container -->
	<?php if($options['show_sidebar'] == true){
		get_sidebar();
	} ?>

	<?php if($show_slider == true){
		get_template_part( 'slider', 'template' );
	} ?>

	<div class="portfolio-details hidden">
		<div class="portfolio-nav portfolio-archive">
			<a href="" class="close-portfolio">X</a>
			<a href="" class="prev-portfolio ajax-enabled" data-id="">&lt;</a>
			<a href="" class="next-portfolio ajax-enabled" data-id="">&gt;</a>
		</div>
		<span class="dash"></span>
		<div class="portfolio-details-content fadein"></div>
	</div>

	<div class="clear"></div>

	<div class="portfolio-filter">
		
		<?php if($has_thumbnail == true){ ?>

			<div class="clear"></div>
			<ul id="items" class="portfolio-list">
				<?php while ( $portfolio->have_posts() ) : $portfolio->the_post();
					if(has_post_thumbnail()){
						$portfolio_tags = get_the_terms( get_the_ID(), 'portfolio_tag'); ?>
						<li class="<?php if(is_array($portfolio_tags)){ foreach($portfolio_tags as $tag){ $tag_single = str_replace(' ', '_', strtolower($tag->name)); echo $tag_single .' '; } } ?>">
							<a href="<?php the_permalink(); ?>" data-id="<?php the_ID(); ?>">
								<span class="view-more-icon"></span>
								<?php the_post_thumbnail('portfolio-thumbnail'); ?>
							</a>
						</li>

					<?php }
				endwhile; ?>
			</ul>
		<?php } else if($has_thumbnail == false && current_user_can('edit_pages')){
			if(function_exists('portfolio_plus_exists_check')){ ?>
				<div class="admin-portfolio-message">
					<p><?php echo __("Currently there are no portfolio items with a thumbnail, why don't you go ahead and add one?", 'handbook'); ?></p>
					<a href="<?php echo admin_url('post-new.php?post_type=portfolio'); ?>" class="more-link"><?php echo __('Add portfolio item', 'handbook'); ?></a>
				</div>
			<?php } else { ?>
				<div class="admin-portfolio-message">
					<p><?php echo __('To use the portfolio feature you need to install Portfolio Plus plugin that comes with the theme in the "plugins" folder.', 'handbook'); ?></p>
				</div>
			<?php } ?>
		<?php } ?>

	</div> <!-- end portfolio-filter -->

	
	<div class="fullwidth-message">
			<p>We love to talk about Broadway! We can help you with tickets, restaurant recommendations, workshops, and more. </p>
			<a href="tel:855-733-2929">855-733-BWAY (2929)!</a>		</div>

<?php get_footer(); ?>