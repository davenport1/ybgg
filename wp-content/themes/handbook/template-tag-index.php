<?php
/*
Template Name: Tag Index
*/
?>

<?php get_header(); ?>

	<script type="text/javascript">
		jQuery(document).ready(function($) { 
			$('.tag-index-page div').isotope();
			$(window).resize( function() { 
				$('.tag-index-page div').isotope();
			});
		});
	</script>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

			<article class="tag-index-page">
				<div>
					<?php 

					// Make an array from 1-9
					$numbers = range(1, 9);

					// Make an array from A-Z
					$my_tags = get_tags( array('order' => 'ASC') );
					$tag_array = array();
					foreach($my_tags as $tag){
						$tag_array[] = $tag->name;
					}

					function first_characters($array){
						$result = array();
						foreach($array as $item){
							$result[] = strtoupper(mb_substr($item, 0, 1, 'utf-8'));
						}
						return array_unique($result);
					}

					// First characters
					$letters = first_characters($tag_array);
					$letters = array_diff($letters, $numbers);

					sort($letters, SORT_LOCALE_STRING);

					// List all tags that start with a number in one <ul>
					if( $numbers && is_array( $numbers ) ) {
						$html = '<ul>';
						$html .= '<li class="tag-title">#<span class="dash"></span></li>';
						foreach( $numbers as $index => $number ){
							$tags = get_tags( array('name__like' => $number, 'order' => 'ASC') );

							foreach ( (array) $tags as $tag ) {
								$tag_link = get_tag_link($tag->term_id);
								$html .= '<li><a href="'.$tag_link.'">'.$tag->name.'<span>#'.$tag->count.'</span></a></li>';
							}
							$index++;
							unset($tags);
						}
						$html .= '</ul>';
						echo $html;
					}

					// List all tags that start with a letter in separate <ul>
					if( $letters && is_array( $letters ) ) {
						foreach( $letters as $index => $letter ){
							$tags = get_tags( array('name__like' => $letter, 'order' => 'ASC') );
							
								if ($tags) {
									$html = '<ul>';
									$html .= '<li class="tag-title">'.$letter.'<span class="dash"></span></li>';
									foreach ( (array) $tags as $tag ) {
										if (strtoupper(mb_substr($tag->name, 0, 1, 'UTF-8')) == $letter) {
											$tag_link = get_tag_link($tag->term_id);
											$html .= '<li><a href="'.$tag_link.'">'.$tag->name.'<span>#'.$tag->count.'</span></a></li>';
										}
									}
									$html .= '</ul>';
									echo $html;
								}
							
							$index++;
							unset($tags, $html);
						}
					}

					?>

				</div>
			</article>

		</div> <!-- end content -->
	</div> <!-- end primary-content -->

	<?php
	$options = get_option('handbook_theme_options');
	if($options['show_sidebar'] == true){
		get_sidebar();
	} ?>

<?php get_footer(); ?>