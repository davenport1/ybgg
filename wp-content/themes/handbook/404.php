<?php
/**
 * The Template for displaying 404 page.
 *
 */

get_header();

?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

			<article class="page-404">
				<h1 class="post-title"><?php echo __('Sorry, Page Not Found', 'handbook'); ?></h1>
				<p><?php echo __('The page you requested does not exist, try searching similar ones below...', 'handbook'); ?></p>

				<?php get_search_form(); ?>

				<div class="clear"></div>
			</article>
					
		</div> <!-- end content -->
	</div> <!-- end container -->

<?php get_footer(); ?>