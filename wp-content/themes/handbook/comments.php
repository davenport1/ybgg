<?php

////////////////////////////////////////////////////////////////////
//	Template for displaying Comments
////////////////////////////////////////////////////////////////////

if ( post_password_required() )
	return; 

	global $post;
	$total_comments = get_comments_number( $post->ID );
	$total_pingbacks = count($wp_query->comments_by_type['pingback']) + count($wp_query->comments_by_type['trackback']);
	$only_comments = get_comments(array('type' => 'comment', 'post_id' => $post->ID, 'status' => 'approved'));
	$number_of_pages = get_comment_pages_count($only_comments);

	$show_avatars = get_option('show_avatars');
	if(!$show_avatars){
		$avatar_off_class = ' avatars-off';
	} else {
		$avatar_off_class = '';
	}

?>

<div class="comments-wrapper comments-area<?php echo $avatar_off_class ?>" id ="comments">

	<?php if (is_singular()){ ?>
		<h4><?php comments_number( 'No comments', 'One comment', '% comments' ); ?></h4>
		<?php if ($total_pingbacks > 0 ){ ?>
			<a href="#" class="show-pingbacks" data-text-show="<?php echo __('show pingbacks', 'handbook'); ?>" data-text-hide="<?php echo __('hide pingbacks', 'handbook'); ?>">(<?php echo __('show pingbacks', 'handbook'); ?>)</a>
		<?php } ?>
	<?php } ?>

	<?php if ( $number_of_pages > 1 && get_option( 'page_comments' ) && is_singular() ) { 
		global $cpage;
		
		$hide_nav_previous = '';
		$hide_nav_next = '';

		$default_comments_page = get_option('default_comments_page');

		if (isset($cpage)){
			$current_comment_page = $cpage;
		} else if (!isset($cpage) && $default_comments_page == 'newest') {
			$current_comment_page = $number_of_pages;
		} else if (!isset($cpage) && $default_comments_page == 'oldest') {
			$current_comment_page = 1;
		}

		if($current_comment_page == 1){
			$hide_nav_previous = 'hide';
		} else if ($current_comment_page == $number_of_pages){
			$hide_nav_next = 'hide';
		}

		?>
		<nav class="comment-nav-below" role="navigation">
			<?php if(function_exists('handbook_get_comment_page')){ ?>
				<div class="nav-previous">
					<a href="<?php echo get_comments_pagenum_link($current_comment_page-1); ?>" class="<?php echo $hide_nav_previous; ?>" title="<?php echo __( 'Older Comments', 'handbook' ); ?>"></a>
				</div>
				<h4><span class="current-comment-page" data-current-page="<?php echo $current_comment_page; ?>"><?php echo $current_comment_page; ?></span> / <span class="top-comment-page" data-top-page="<?php echo $number_of_pages; ?>"><?php echo $number_of_pages; ?></span></h4>
				<div class="nav-next">
					<a href="<?php echo get_comments_pagenum_link($current_comment_page+1); ?>" class="<?php echo $hide_nav_next; ?>" title="<?php echo __( 'Newer Comments', 'handbook' ); ?>"></a>
				</div>
			<?php } else {
				paginate_comments_links();
			} ?>
		</nav>
	<?php } ?>

	<div class="clear"></div>

	<?php if ($total_pingbacks > 0 ){ ?>
		<ul class="pingbacks-trackbacks">
			<?php foreach ($comments as $comment){
				$comment_type = get_comment_type();
				if($comment_type != 'comment') {
					?><li><?php comment_author_link() ?></li><?php
				}
			} ?>
		</ul>
	<?php } ?>

	<ul class="comments comment-list">
		<?php wp_list_comments( array( 'callback' => 'handbook_comment') ); ?>
	</ul> <!-- end comments -->
	
	<?php

	////////////////////////////////////////////////////////////////
	//  Display the reply box
	////////////////////////////////////////////////////////////////

	if('open' == $post->comment_status && is_singular()) {
		comment_form();
	}

	if (is_single()){ ?>
		<span class="dash"></span>
	<?php } ?>

</div>