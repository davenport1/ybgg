<?php

////////////////////////////////////////////////////////////////////
//	Template for displaying Standard post format
////////////////////////////////////////////////////////////////////

$post_id = get_the_ID();
?>

	<article id="post-<?php echo $post_id; ?>" data-id="<?php echo $post_id; ?>" <?php post_class(); ?>>

		<h1 class="post-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</h1>


		<?php the_tags('<ul class="tags"><li>','</li><li>','</li></ul>'); ?>

		<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>

		<?php if (has_excerpt()){ ?>
			<div class="excerpt">
				<?php the_excerpt(); ?>
			</div>
		<?php } else if (!is_singular()){
			$continue_text = __('Continue reading...', 'handbook'); 
			the_content('<span class="read-more">'.$continue_text.'</span>');
		} ?>

		<?php if(is_singular()){ the_content(); } ?>

		<div class="clear"></div>

		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'handbook' ), 'after' => '</div>' ) ); ?>

		<?php if (is_single()){ get_template_part( 'author', 'infobox' ); } ?>

		<?php if(is_singular()){
			comments_template( '', true ); 
		} ?>

		<span class="dash"></span>

	</article>
