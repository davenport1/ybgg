<?php

////////////////////////////////////////////////////////////////////
//	Template for displaying Link post format
////////////////////////////////////////////////////////////////////

$post_id = get_the_ID();
?>

	<article id="post-<?php echo $post_id; ?>" data-id="<?php echo $post_id; ?>" <?php post_class(); ?>>

		<div class="link-content">
			<?php the_content(''); ?>
		</div>

		<?php if (has_excerpt()){ ?>
			<div class="excerpt">
				<?php the_excerpt(); ?>
			</div>
		<?php } ?>

		<div class="clear"></div>

		<p class="post-meta">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_time(get_option('date_format')); ?></a> 
			<span>/</span>
			<span class="category-list">
				<?php the_category( ',' ); ?>
			</span>
			<span>/</span>
			<a href="<?php comments_link(); ?>"><?php comments_number(); ?></a>
			<?php if (function_exists('lp_love_post_link')){ lp_love_post_link($post_id); } ?>
		</p>

		<?php if(is_single()){
			comments_template( '', true ); 
		} ?>

		<span class="dash"></span>
		
	</article>
