<?php

////////////////////////////////////////////////////////////////////
//	Template for displaying single portfolio
////////////////////////////////////////////////////////////////////

function handbook_portfolio_no_sidebar($classes) {
    $classes[] = 'sidebar-off';
    return $classes;
}
add_filter('body_class', 'handbook_portfolio_no_sidebar');

get_header();

$options = get_option('handbook_theme_options');
$image_list = '';
$disabled_class_prev = '';
$disabled_class_next = '';
$post_info_html = '';
$template_archive_url = '#';

$pages = get_pages(array(
	'meta_key' => '_wp_page_template',
	'meta_value' => 'template-portfolio-mini.php'
));

if(!isset($pages[0])){
	$pages = get_pages(array(
		'meta_key' => '_wp_page_template',
		'meta_value' => 'template-portfolio-grid.php'
	));

	if(!isset($pages[0])){
			$pages = get_pages(array(
			'meta_key' => '_wp_page_template',
			'meta_value' => 'template-portfolio-grid-filter.php'
		));
	}
}

if(isset($pages[0])){
	foreach($pages as $page){
		$template_archive_url = get_page_link($page->post_id);
	}
}

?>

	<?php if(have_posts()) : while(have_posts()) : the_post();

		$post_ID = get_the_ID();

		// Get next and previous posts
		$next_post = get_adjacent_post(false, '', false);
		$prev_post = get_adjacent_post(false, '', true);

		if($prev_post == ''){
			$disabled_class_prev = 'disable-nav';
		}

		if($next_post == ''){
			$disabled_class_next = 'disable-nav';
		}

		// Get portfolio info metabox
		$post_info = get_post_meta($post_ID, '_pp_portfolio_meta', true);

		if($post_info != ''){
			foreach($post_info as $info => $info_value){
				$info_value_split = explode(':', $info_value, 2);
				if(!isset($info_value_split[1])){
					$info_value_split = explode(' ', $info_value, 2);
				}
				if(isset($info_value_split[1])){
					$post_info_html .= '<li><span>'.$info_value_split[0].':</span><p>'.$info_value_split[1].'</p></li>';
				}
			} 
		}

		// Get related portfolio items
		$portfolio_related_id = handbook_get_portfolio_related($post_ID);

		foreach($portfolio_related_id as $portfolio_index => $portfolio_id){
			$portfolio_related_url[$portfolio_index] = get_permalink($portfolio_id);
			$portfolio_related_thumbnail[$portfolio_index] = wp_get_attachment_image_src( get_post_thumbnail_id($portfolio_id), 'portfolio-thumbnail' );
			$portfolio_related_thumbnail[$portfolio_index] = $portfolio_related_thumbnail[$portfolio_index]['0'];
		} ?>

		<?php $gallery = get_post_gallery($post_ID, false); $gallery = explode(',', $gallery['ids']); wp_reset_query(); ?>

		<div class="portfolio-nav">
			<a class="portfolio-back" href="<?php echo $template_archive_url; ?>"><?php echo __('&larr; view all items', 'handbook'); ?></a>
			<a href="<?php if($prev_post != '') { echo get_permalink($prev_post->ID); } ?>" class="prev-portfolio ajax-enabled <?php echo $disabled_class_prev ?>" data-id="<?php if($prev_post != '') { echo $prev_post->ID; } ?>">prev</a>
			<a href="<?php if($next_post != '') { echo get_permalink($next_post->ID); } ?>" class="next-portfolio ajax-enabled <?php echo $disabled_class_next ?>" data-id="<?php if($next_post != '') { echo $next_post->ID; } ?>">next</a>
		</div>

		<div class="portfolio-details show">
			<div class="portfolio-details-content fadein">
				<div class="gallery flexslider">
					<ul class="slides">
						<?php foreach($gallery as $attachment_id){
							$get_url = parse_url($attachment_id);
							if(isset($get_url['host'])){
								$get_url = $get_url['host'];
								if($get_url == 'www.youtube.com' || $get_url == 'youtube.com' || $get_url == 'youtu.be' || $get_url == 'www.vimeo.com' || $get_url == 'vimeo.com' ){
									$embed_code = wp_oembed_get($attachment_id, array('width' => 700));
									$image_list .= '<li><div class="video-content">'.$embed_code.'</div></li>';
								}
							}
					 		else {
								$attachment_meta = get_post($attachment_id);
								$image_src = wp_get_attachment_image_src($attachment_id, $size='full');
								$image_list .= '<li><a class="fullscreen-gallery" href="'.$image_src[0].'" rel="gallery-'.$post_ID.'" title="'.$attachment_meta->post_excerpt.'">+</a><img src="' . $image_src[0] . '"/></li>';
							}
						} 
						echo $image_list;
						?>
					</ul>
				</div>
				<div class="portfolio-meta col_9">
					<h1><?php the_title(); ?></h1>
					<?php if(has_excerpt()){ ?>
						<h2 class="portfolio-excerpt"><?php echo get_the_excerpt(); ?></h2>
					<?php } ?>
					<ul>
						<?php echo $post_info_html; ?>
					</ul>
					<div class="portfolio-content">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	
	<?php endwhile; ?>

	<?php else : ?>

		<div class="page-info">
			<h1><?php echo __('No items found', 'handbook') ?></h1>
		</div>
		
	<?php endif; ?>

	<div class="portfolio-similar-items fadein">
		<span class="dash"></span>
		<?php if(isset($options['portfolio_related']) && $options['portfolio_related'] != ''){ ?>
			<h4><?php echo $options['portfolio_related']; ?></h4>
		<?php } ?>
		<ul>
			<?php foreach($portfolio_related_id as $portfolio_index => $portfolio_id){ ?>
			<li>
				<a data-id="<?php echo $portfolio_id; ?>" class="ajax-enabled" href="<?php echo $portfolio_related_url[$portfolio_index]; ?>">
					<span class="view-more-icon"></span>
					<img class="attachment-portfolio-thumbnail wp-post-image" width="260" height="200" alt="thumbnail" src="<?php echo $portfolio_related_thumbnail[$portfolio_index]; ?>">
				</a>
			</li>
			<?php } ?>

		</ul>
		<div class="clear"></div>
	</div>


<?php get_footer(); ?>