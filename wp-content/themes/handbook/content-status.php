<?php

////////////////////////////////////////////////////////////////////
//	Template for displaying Status post format
////////////////////////////////////////////////////////////////////

$post_id = get_the_ID();
?>

	<article id="post-<?php echo $post_id; ?>" data-id="<?php echo $post_id; ?>" <?php post_class(); ?>>

		<span class="dash"></span>

		<?php echo get_avatar(get_the_author_meta('ID'), 130); ?>

		<div class="author">
			<span><?php the_author(); ?></span>  &bull;  
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</div>

		<div class="status-content">
			<?php $continue_text = __('Continue reading...', 'handbook'); 
			the_content('<span class="more-text">'.$continue_text.'</span>'); ?>
		</div>

		<div class="clear"></div>

		<p class="post-meta">
			<a href="<?php the_permalink(); ?>"><?php the_time(get_option('date_format')); ?></a> 
			<span>/</span> 
			<span class="category-list">
				<?php the_category( ',' ); ?>
			</span>
			<span>/</span> 
			<a href="<?php comments_link(); ?>"><?php comments_number(); ?></a>
			<?php if (function_exists('lp_love_post_link')){ lp_love_post_link($post_id); } ?>
		</p>

		<?php if(is_single()){
			comments_template( '', true ); 
		} ?>
		
	</article>
