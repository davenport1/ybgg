<?php
/*
Template Name: Archive Page
*/
?>

<?php get_header();

$options = get_option('handbook_theme_options');

$post_id = get_the_ID();
?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

			<article class="page">

				<h1 class="post-title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</h1>

				<?php if(has_excerpt()){ ?>
					<p class="excerpt"><?php the_excerpt(); ?></p>
				<?php } ?>

				<h4><?php echo __('Archives by Year:', 'handbook') ?></h4>
				<ul>
					<?php wp_get_archives('type=yearly'); ?>
				</ul>
				<h4><?php echo __('Archives by Month:', 'handbook') ?></h4>
				<ul>
					<?php wp_get_archives('type=monthly'); ?>
				</ul>
				<h4><?php echo __('Latest posts:', 'handbook') ?></h4>
				<ul>
					<?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 20) ); ?>
				</ul>

			</article>

		</div> <!-- end content -->
	</div> <!-- end container -->

	<?php if($options['show_sidebar'] == true){ ?>

		<?php get_sidebar(); ?>

	<?php } ?>

<?php get_footer(); ?>



