<?php

////////////////////////////////////////////////////////////////////
//	Template for displaying pages
////////////////////////////////////////////////////////////////////

get_header();

$options = get_option('handbook_theme_options');

?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php get_template_part( 'content'); ?>

		<?php endwhile; ?>

		<?php else : ?>

			<div class="page-info">
				<h1><?php echo __('No items found', 'handbook') ?></h1>
			</div>
			
		<?php endif; ?>

		</div> <!-- end content -->
	</div> <!-- end primary-content -->

	<?php if($options['show_sidebar'] == true){
		get_sidebar();
	} ?>

<?php get_footer(); ?>