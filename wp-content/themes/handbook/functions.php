<?php 

////////////////////////////////////////////////////////////////
//	Theme Setup Functions
////////////////////////////////////////////////////////////////

// This theme supports a variety of post formats.
add_theme_support( 'post-formats', array( 'status', 'image', 'gallery', 'video', 'audio', 'quote', 'link' ) );

// This theme uses wp_nav_menu() in two locations.
register_nav_menu( 'header', __( 'Header Menu', 'handbook' ) );
register_nav_menu( 'footer', __( 'Footer Menu', 'handbook' ) );

// This theme supports featured image.
add_theme_support( 'post-thumbnails' );

set_post_thumbnail_size(750);
add_image_size( 'gallery-thumbnail', 135, 100, true );

// Add Portoflio thumbnails
add_image_size( 'portfolio-thumbnail', 260, 200, true );
add_image_size( 'portfolio-thumbnail-big', 300, 9999, false );


// Set content width and automatic feed links
if (!isset( $content_width )) $content_width = 750;
add_theme_support( 'automatic-feed-links' );

// Load textdomain
add_action('after_setup_theme', 'handbook_setup');
function handbook_setup(){
	load_theme_textdomain('handbook', get_template_directory() . '/languages');
}


///////////////////////////////////////////////////////////////////////////////////////
//	Theme JS includes
///////////////////////////////////////////////////////////////////////////////////////

function handbook_scripts(){

	$options = get_option('handbook_theme_options');

	///////////////////////////////////////////////////////////////////////////////////////
	// Frontend Scripts
	///////////////////////////////////////////////////////////////////////////////////////

	if(!is_admin()){
		wp_enqueue_script( 'jquery' );

		if (current_user_can('edit_theme_options')){
			wp_enqueue_script( 'jquery-ui-core' );
			wp_enqueue_script( 'jquery-ui-sortable' );
		}

		wp_register_script( 'handbook-imagesloaded', get_template_directory_uri() . '/script/jquery.imagesloaded.min.js' );
		wp_register_script( 'handbook-flexslider', get_template_directory_uri() . '/script/jquery.flexslider-min.js' );
		wp_register_script( 'handbook-swipebox', get_template_directory_uri() . '/script/jquery.swipebox.min.js' );
		wp_register_script( 'handbook-easing', get_template_directory_uri() . '/script/jquery.easing.1.3.js' );
		wp_register_script( 'handbook-isotope', get_template_directory_uri() . '/script/jquery.isotope.min.js' );
		wp_register_script( 'handbook-script-portfolio', get_template_directory_uri() . '/script/script-portfolio.js' );
		wp_register_script( 'handbook-script', get_template_directory_uri() . '/script/script.js' );

		if(is_page_template('template-tag-index.php')){
			wp_enqueue_script( 'handbook-isotope' );
		}

		///////////////////////////////////////////////////////////////////////////////////////
		// Portfolio Scripts
		///////////////////////////////////////////////////////////////////////////////////////

		if(is_page_template('template-frontpage-mini.php') || is_page_template('template-frontpage-grid.php') || is_page_template('template-frontpage-grid-filter.php') || is_page_template('template-portfolio-grid-filter.php') || is_page_template('template-portfolio-grid.php') || is_page_template('template-portfolio-mini.php') || is_singular('portfolio')){
			wp_enqueue_script( 'handbook-easing' );
			wp_enqueue_script( 'handbook-isotope' );
			wp_enqueue_script( 'handbook-script-portfolio' );

			if( isset($options['portfolio_autoplay']) && $options['portfolio_autoplay'] == false ){
				wp_localize_script( 'handbook-script-portfolio', 'handbookPortfolio', array( 'autoplay' => 'false', ) );
			} else {
				wp_localize_script( 'handbook-script-portfolio', 'handbookPortfolio', array( 'autoplay' => 'true', ) );
			}
		}

		wp_enqueue_script( 'comment-reply' );
		wp_enqueue_script( 'handbook-imagesloaded' );
		wp_enqueue_script( 'handbook-flexslider' );
		wp_enqueue_script( 'handbook-swipebox' );
		wp_enqueue_script( 'handbook-script' );
	}

	///////////////////////////////////////////////////////////////////////////////////////
	// Admin Scripts
	///////////////////////////////////////////////////////////////////////////////////////

	if (!is_admin() && current_user_can('edit_posts')){
		wp_register_script( 'handbook-script-admin', get_template_directory_uri() . '/script/script-admin.js' );
		wp_enqueue_script( 'handbook-script-admin' );
	}

	///////////////////////////////////////////////////////////////////////////////////////
	// Localize Script
	///////////////////////////////////////////////////////////////////////////////////////

	wp_localize_script( 'handbook-script', 'handbook', array( 
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'nonce' => wp_create_nonce( 'handbook-nonce-check' ),
		)
	);

}

add_action( 'wp_enqueue_scripts', 'handbook_scripts' );
add_action( 'admin_enqueue_scripts', 'handbook_scripts' );

///////////////////////////////////////////////////////////////////////////////////////
//	Theme AJAX actions
///////////////////////////////////////////////////////////////////////////////////////

function handbook_actions(){

	// Adding comments
	add_action( 'wp_ajax_handbook_add_comment', 'handbook_add_comment' );
	add_action( 'wp_ajax_nopriv_handbook_add_comment', 'handbook_add_comment' );

	// Comment pagination
	add_action( 'wp_ajax_handbook_get_comment_page', 'handbook_get_comment_page' );
	add_action( 'wp_ajax_nopriv_handbook_get_comment_page', 'handbook_get_comment_page' );

	// Portfolio item loading
	add_action( 'wp_ajax_handbook_get_portfolio_item', 'handbook_get_portfolio_item' );
	add_action( 'wp_ajax_nopriv_handbook_get_portfolio_item', 'handbook_get_portfolio_item' );

}

add_action( 'init', 'handbook_actions' );


///////////////////////////////////////////////////////////////////////////////////////
//	Theme CSS includes
///////////////////////////////////////////////////////////////////////////////////////

function handbook_styles(){

	wp_register_style( 'handbook-font', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,800' );
	wp_register_style( 'handbook-style', get_stylesheet_uri() );

	wp_enqueue_style( 'handbook-font' );
	wp_enqueue_style( 'handbook-style' );

	if (is_rtl()){
		wp_register_style( 'handbook-style-rtl', get_template_directory_uri() . '/style/style-rtl.css' );
		wp_enqueue_style( 'handbook-style-rtl' );
	}

}

add_action( 'wp_enqueue_scripts', 'handbook_styles' );


///////////////////////////////////////////////////////////////////////////////////////
// Theme Customizer
///////////////////////////////////////////////////////////////////////////////////////

require_once('theme-options/customizer.php');


///////////////////////////////////////////////////////////////////////////////////////
// Theme Widgets
///////////////////////////////////////////////////////////////////////////////////////

require_once('theme-options/widget-pretty-tags.php');


///////////////////////////////////////////////////////////////////////////////////////
// Slider Manager
///////////////////////////////////////////////////////////////////////////////////////

require_once('theme-options/slider-manager.php');


///////////////////////////////////////////////////////////////////////////////////////
// Unlimited Sidebars
///////////////////////////////////////////////////////////////////////////////////////

require_once('theme-options/unlimited-sidebars.php');


///////////////////////////////////////////////////////////////////////////////////////
// Register Widgetized Areas
///////////////////////////////////////////////////////////////////////////////////////

if(current_user_can ('edit_theme_options') ){
	$widget_order_handler = '<a href="" class="widget-order-handle"></a>';
} else {
	$widget_order_handler = '';
}

register_sidebar(array(
	'name' => __( 'Main Sidebar', 'handbook' ),
	'id' => 'sidebar-main',
	'description' => __( 'Widgets in this area will be shown in the main sidebar.', 'handbook' ),
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">'.$widget_order_handler,
	'after_widget'  => '<span class="dash"></span></aside>',
));

$handbook_options = get_option('handbook_theme_options');
$unlimited_sidebars_options = get_option('unlimited_sidebars_settings'); 

if(isset($handbook_options['show_sidebar'])){
	if($unlimited_sidebars_options && $handbook_options['show_sidebar'] == true){
		foreach ($unlimited_sidebars_options as $sidebar_id => $sidebar_name) {
			register_sidebar(array(
				'name' => $sidebar_name['name'],
				'id' => $sidebar_id,
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>',
				'before_widget' => '<aside id="%1$s" class="widget %2$s">'.$widget_order_handler,
				'after_widget'  => '</aside>',
			));
		}
	}
}

register_sidebar(array(
	'name' => __( 'Footer Left', 'handbook' ),
	'id' => 'footer-1',
	'description' => __( 'Widgets in this area will be shown on the left side of the footer.', 'handbook' ),
	'before_title' => '<h5 class="widget-title">',
	'after_title' => '</h5>',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
));

register_sidebar(array(
	'name' => __( 'Footer Middle', 'handbook' ),
	'id' => 'footer-2',
	'description' => __( 'Widgets in this area will be shown in the middle of the footer.', 'handbook' ),
	'before_title' => '<h5 class="widget-title">',
	'after_title' => '</h5>',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
));

register_sidebar(array(
	'name' => __( 'Footer Right', 'handbook' ),
	'id' => 'footer-3',
	'description' => __( 'Widgets in this area will be shown on the right side of the footer.', 'handbook' ),
	'before_title' => '<h5 class="widget-title">',
	'after_title' => '</h5>',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
));


///////////////////////////////////////////////////////////////////////////////////////
// List Comments
///////////////////////////////////////////////////////////////////////////////////////

function handbook_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?> data-id="comment-<?php comment_ID(); ?>">

		<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
			<?php echo get_avatar( $comment, 60 ); ?>

			<p class="author">
				<span><?php comment_author_link(); ?></span> &bull; 
				<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>" class="post-date" title="<?php echo __('comment permalink', 'handbook'); ?>"><?php echo get_comment_date(); ?></a>
				
				<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'add_below' => 'div-comment', 'before' => ' &bull; '))); ?>
				<?php if ( '0' == $comment->comment_approved ){?>
					&bull; <span class="comment-awaiting-moderation"><?php echo __( 'Your comment is awaiting moderation.', 'handbook' ); ?></span>
				<?php } ?>
			</p>

			<div class="comment-text">
				<?php comment_text(); ?>
			</div>
		</article>
		
	<?php
		break;
	endswitch; // end comment_type check
}

///////////////////////////////////////////////////////////////////////////////////////
//  Comment Form
///////////////////////////////////////////////////////////////////////////////////////

function handbook_comment_form(){
	global $post;
	global $current_user; get_currentuserinfo();
	
	if (is_user_logged_in()){
		$textarea = get_avatar(get_the_author_meta( 'ID' ), 60);
		$textarea .= '<div class="comment-box-wrap"><div><span></span><textarea placeholder="Leave a comment ..." name="comment" aria-required="true" class="comment-box"></textarea></div></div>';
	} else {
		$textarea = '<textarea placeholder="Leave a comment ..." name="comment" aria-required="true" class="comment-box"></textarea>';
	}

	$allowed_tags = allowed_tags();

	$fields =  array(
		'author' => '<input type="text" value="" name="author" class="author" placeholder="'. __('Your Name...', 'handbook').'">',
		'email'  => '<input type="text" value="" name="email" class="email" placeholder="'. __('Your Email...', 'handbook').'">',
		'url'    => '<input type="text" value="" name="url" class="url" placeholder="'. __('Webpage...', 'handbook').'">',
	);

	$form_options = array(
		'fields' => apply_filters( 'comment_form_default_fields', $fields ),
		'comment_field' => $textarea,
		'comment_notes_before' => '<p>'. __('Comment moderation is enabled, no need to resubmit any comments posted.', 'handbook') .'</p>',
		'comment_notes_before' => '',
		'must_log_in' => '<p class="must-be-logged-in">' . sprintf( __( 'You must be <a href="%1$s" title="Login">logged in</a> to leave a comment.', 'handbook' ), wp_login_url( get_permalink() ) ) . '</p>',
		'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as %1$s. <a href="%2$s" title="Log out of this account">Log out?</a>' ), $current_user->display_name, wp_logout_url( get_permalink() ), 'handbook') . '</p>',
		'comment_notes_after' => '<em class="info">You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: <code> '.$allowed_tags.' </code></em>',
		'id_form' => 'comment-form-' .$post->ID,
		'id_submit' => 'submit-' .$post->ID,
		'label_submit' => __( 'Reply' , 'handbook'),
		'title_reply' => __( 'Leave a comment' , 'handbook'),
		'title_reply_to' => __( 'Leave a Reply to %s' , 'handbook'),
		'cancel_reply_link' => __( '(Cancel reply)' , 'handbook'),
	);

	return $form_options;
}

add_filter('comment_form_defaults', 'handbook_comment_form');

function handbook_move_textarea( $input = array () ){
	static $textarea = '';

	if ( 'comment_form_defaults' === current_filter() ) {
		$textarea = $input['comment_field'];
		$input['comment_field'] = '';
		return $input;
	}

	print apply_filters( 'comment_form_field_comment', $textarea );
}

if(is_user_logged_in() == false){
	add_filter( 'comment_form_defaults', 'handbook_move_textarea' );
	add_action( 'comment_form_top', 'handbook_move_textarea' );
}

///////////////////////////////////////////////////////////////////////////////////////
// AJAX Comments
///////////////////////////////////////////////////////////////////////////////////////

function handbook_add_comment(){

	check_ajax_referer( 'handbook-nonce-check', 'security' );

	$comment_parent = $_POST['comment_parent'];
	$time = current_time('mysql');

	// If the user is logged in approve the comment and get the author ID
	$current_user = wp_get_current_user();

	if ( is_user_logged_in() ){
		$comment_approved = 1;
		$comment_author_id = $current_user->ID;
		$comment_author_name = $current_user->display_name;
		$comment_author_email = $current_user->user_email;
		$comment_author_url = $current_user->user_url;
	} else {
		$comment_approved = 1;
		$comment_author_id = 0;
		$comment_author_name = $_POST['comment_author'];
		$comment_author_email = $_POST['comment_author_email'];
		$comment_author_url = $_POST['comment_author_url'];
	}


	// Add the comment
	$data = array(
		'comment_post_ID' => $_POST['comment_post_ID'],
		'comment_author' => $comment_author_name,
		'comment_author_email' => $comment_author_email,
		'comment_author_url' => $comment_author_url,
		'comment_content' => $_POST['comment_content'],
		'comment_type' => '',
		'comment_parent' => $comment_parent,
		'comment_date' => $time,
		'comment_approved' => $comment_approved,
		'user_id' => $comment_author_id,
	);

	$insert_comment = wp_new_comment($data);

	if ( is_wp_error($insert_comment) ){
		
		$error = $insert_comment->get_error_message();
		echo json_encode(array( 'inserted' => false, 'errorInfo' => $error ));

	} else {

		// Set a comment cookie
		$comment = get_comment($insert_comment);
		$user = wp_get_current_user();
		wp_set_comment_cookies($comment, $user);

		if($comment_author_url == ''){
			$comment_author_html = '<span>'.$comment_author_name.'</span> &bull; ';
		} else {
			$comment_author_html = '<span><a class="url" href="'.$comment_author_url.'">'.$comment_author_name.'</a></span> &bull; ';
		}

		if(get_option('thread_comments') == ''){
			$max_depth = 1;
			$comment_depth = 0;
		} else {
			$max_depth = get_option('thread_comments_depth');
			$comment_depth = get_comment_depth($insert_comment);
		}

		$comment_awaiting_moderation = '';
		if ($comment->comment_approved == '0'){
			$comment_awaiting_moderation .= ' &bull; <span class="comment-awaiting-moderation">';
			$comment_awaiting_moderation .= __( 'Your comment is awaiting moderation.', 'handbook' );
			$comment_awaiting_moderation .= '</span>';
		}

		$comment_reply_link = '';
		$comment_reply_link_fixed = '';
		$comment_reply_link = get_comment_reply_link( array('depth' => $comment_depth, 'max_depth' => $max_depth, 'add_below' => 'div-comment', 'before' => ' &bull; ' ), $insert_comment, 142);
		$comment_reply_link_fixed = str_replace($_SERVER['REQUEST_URI'], '', $comment_reply_link);

		// Return the comment HTML
		$comment_html = '<li id="comment-'.$insert_comment.'" class="comment hidden" data-id="comment-'.$insert_comment.'">';
		$comment_html .= '<article id="div-comment-'.$insert_comment.'" class="comment-body">';
		$comment_html .= get_avatar( $comment_author_email, 60 );
		$comment_html .= '<p class="author">';
		$comment_html .=     $comment_author_html;
		$comment_html .=     '<a href="'.esc_url( get_comment_link( $comment->comment_ID ) ).'" class="post-date">'.get_comment_date( get_option('date_format'), $insert_comment ).'</a>';
		$comment_html .=     $comment_reply_link_fixed;
		$comment_html .=     $comment_awaiting_moderation;
		$comment_html .= '</p>';
		$comment_html .= '<div class="comment-text"><p>';
		$comment_html .=     apply_filters('get_comment_text', $comment->comment_content);
		$comment_html .= '</div>';
		$comment_html .= '</article>';
		$comment_html .= '</li>';

		echo json_encode(array( 'inserted' => true, 'comment' => $comment_html ));
	}

	die();
}

function get_comment_depth($comment_ID){
	static $current_depth = 0;
	$current_depth++;

	$current_comment_info = get_comment($comment_ID);
	$current_comment_parent = $current_comment_info->comment_parent;

	if($current_comment_parent == 0){
		return $current_depth;
	} else {
		get_comment_depth($current_comment_parent);
		return $current_depth;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AJAX Comments Pagination
///////////////////////////////////////////////////////////////////////////////////////

function handbook_get_comment_page(){

	check_ajax_referer( 'handbook-nonce-check', 'security' );

	$page = $_POST['get_page'];
	$post_ID = $_POST['post_ID'];
	$per_page = get_option('comments_per_page');

	$comments = get_comments( array('post_id' => $post_ID, 'status' => 'approve', 'order' => 'ASC' ));

	// Get the comments in $comment_html variable
	ob_start();
		wp_list_comments( array( 'callback' => 'handbook_comment', 'page' => $page, 'per_page' => $per_page), $comments );
		$comment_html = ob_get_contents();
	ob_end_clean();

	// Fix the empty page in comment link
	$comment_html = str_replace( 'comment-page-', 'comment-page-' . $page, $comment_html);

	echo json_encode(array( 'inserted' => true, 'comments' => $comment_html ));

	die();
}

///////////////////////////////////////////////////////////////////////////////////////
//	Disable AJAX option
///////////////////////////////////////////////////////////////////////////////////////

add_action( 'admin_init', 'ajaxed_comments_register_setting' );

function ajaxed_comments_register_setting(){
	add_settings_section( 'ajaxed_comments_id', __('AJAX Comments', 'handbook'), 'ajaxed_comments_description', 'discussion' );
	register_setting( 'discussion', 'ajaxed_comments', 'esc_attr' );
	add_settings_field( 'ajaxed_comments', __('AJAX Comments', 'handbook'), 'ajaxed_comments_settings', 'discussion', 'ajaxed_comments_id', array ( 'label_for' => 'ajaxed_comments_id' ) );
}

function ajaxed_comments_description(){
	$ajax_setting_description = __('You can disable AJAX comments if you are experiencing any issues when using third-party plugins that extend the default comments form functionality.', 'handbook');
    echo '<p class="description">'.$ajax_setting_description.'</p>';
}

function ajaxed_comments_settings($args){
	$data = esc_attr( get_option( 'ajaxed_comments', '' ) );

	$ajax_setting_checkbox_label = __('Enable AJAX comments and pagination.', 'handbook');

	printf(
		'<label for="%1$s"><input type="checkbox" name="ajaxed_comments" value="1" id="%1$s" '.checked( 1, $data, false ).'  />%2$s</label>',
		$args['label_for'],
		$ajax_setting_checkbox_label
	);
}

///////////////////////////////////////////////////////////////////////////////////////
//	Add 'submenu-parent' class to menu items
///////////////////////////////////////////////////////////////////////////////////////

function add_submenu_parent_class( $items ) {
	
	$parents = array();
	foreach ( $items as $item ) {
		if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
			$parents[] = $item->menu_item_parent;
		}
	}
	
	foreach ( $items as $item ) {
		if ( in_array( $item->ID, $parents ) ) {
			$item->classes[] = 'submenu-parent';
		}
	}
	
	return $items;
}

add_filter( 'wp_nav_menu_objects', 'add_submenu_parent_class' );


///////////////////////////////////////////////////////////////////////////////////////
// Custom Gallery Markup
///////////////////////////////////////////////////////////////////////////////////////

function handbook_custom_gallery($attr) {
  
	global $post;

	if(basename($_SERVER['PHP_SELF']) == 'admin-ajax.php'){
		return;
	}

	if ( ! empty( $attr['ids'] ) ) {
		if ( empty( $attr['orderby'] ) )
			$attr['orderby'] = 'post__in';
		$attr['include'] = $attr['ids'];
	}

	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'itemtag'    => 'dl',
		'icontag'    => 'dt',
		'captiontag' => 'dd',
		'columns'    => 3,
		'size'       => 'full',
		'include'    => '',
		'exclude'    => ''
    ), $attr));

	$args = array(
		'post_type' => 'attachment',
		'post_parent' => $id,
		'numberposts' => -1,
		'orderby' => 'menu_order'
	);
   
	if(!empty($include)){
		$images = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$images = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	$html = '';
	if($images){
		$first_image = wp_get_attachment_image_src($images[0]->ID, $size='full');
		$first_caption = $images[0]->post_excerpt;
		if($first_caption == ''){
			$html .=  '<div class="gallery-embed"><div class="current-image"><div class="caption hidden"><p></p></div><img src="'. $first_image[0] .'" alt="" /><img class="loaded-image" src="#" alt="#" /></div>';
		} else {
			$html .=  '<div class="gallery-embed"><div class="current-image"><div class="caption"><p>'.$first_caption.'</p></div><img src="'. $first_image[0] .'" alt="" /><img class="loaded-image" src="#" alt="#" /></div>';
		}
		$html .=  '<div class="flexslider carousel"><ul class="slides">';

		$i = 0;
		foreach ( $images as $image ) {
			$title = '';
			$caption = $image->post_excerpt;
			if($i == 0){
				$class_active = ' active';
			} else {
				$class_active = '';
			}

			$description = $image->post_content;
			if($description == '') $description = $title;

			$image_alt = get_post_meta($image->ID,'_wp_attachment_image_alt', true);

			$image_data = wp_get_attachment_image_src($image->ID, $size='full');

			$html .= '<li><a href="'. $image_data[0] .'" data-caption="'.$caption.'" data-id="'.$image->ID.'" class="gallery-thumbnail'.$class_active.'" title="'.$image->post_title.'">';
			$html .=  wp_get_attachment_image( $image->ID, $size ='gallery-thumbnail');
			$html .= '</a></li>';
			$i++;
		            
		}
		$html .= '</ul></div></div>';
		return $html;
     }

}

remove_shortcode('gallery');
add_shortcode('gallery', 'handbook_custom_gallery');


///////////////////////////////////////////////////////////////////////////////////////
// AJAX Portfolio - get the portfolio item info
///////////////////////////////////////////////////////////////////////////////////////

function handbook_get_portfolio_item(){
	check_ajax_referer( 'handbook-nonce-check', 'security' );

	$image_list     = '';
	$post_info_html = '';
	$post_URL       = '';
	$next_post_id   = '';
	$prev_post_id   = '';

	if(isset($_POST['post_ID'])){
		$post_ID = $_POST['post_ID'];
	}
	
	if(isset($_POST['post_URL'])){
		$post_URL = $_POST['post_URL'];
	}

	if($post_ID == ''){
		$post_ID = url_to_postid($post_URL);
	}

	$post_data = get_post($post_ID);
	$post_short = $_POST['post_short'];

	// Get the post data based on post ID
	$post_type = get_post_type( $post_ID );
	$item = array(
		'p' => $post_ID,
		'post_type' => $post_type,
	);

	$item_query = new WP_Query($item);
	$item_query->the_post();

	$post_title    = get_the_title();
	$post_excerpt  = get_the_excerpt();
	$post_link     = get_permalink($post_ID);
	$post_gallery  = get_post_gallery($post_ID, false); $post_gallery = explode(',', $post_gallery['ids']);
	$post_info     = get_post_meta($post_ID, '_pp_portfolio_meta', true);
	$post_content  = apply_filters('the_content', $post_data->post_content );
	if($post_short == "true"){
		$post_content  = explode('<!--more-->', $post_content);
		$post_content  = $post_content[0];
	}

	// Get ID's for the next and previous post
	$next_post = get_adjacent_post(false, '', false);
	$prev_post = get_adjacent_post(false, '', true);

	if($next_post != '') {
		$next_post_id = $next_post->ID;
	}

	if($prev_post != '') {
		$prev_post_id = $prev_post->ID;
	}

	// Get the gallery from the post
	foreach($post_gallery as $attachment_id){
		$get_url = parse_url($attachment_id);
		if(isset($get_url['host'])){
			$get_url = $get_url['host'];
			if($get_url == 'www.youtube.com' || $get_url == 'youtube.com' || $get_url == 'youtu.be' || $get_url == 'www.vimeo.com' || $get_url == 'vimeo.com' ){
				$embed_code = wp_oembed_get($attachment_id, array('width' => 700));
				$image_list .= '<li><div class="video-content">'.$embed_code.'</div></li>';
			}
		}
 		else {
			$attachment_meta = get_post($attachment_id);
			$image_src = wp_get_attachment_image_src($attachment_id, $size='full');
			$image_list .= '<li><a class="fullscreen-gallery" href="'.$image_src[0].'" rel="gallery-'.$post_ID.'" title="'.$attachment_meta->post_excerpt.'">+</a><img src="' . $image_src[0] . '"/></li>';
		}
	}

	// Get portfolio info metabox
	if($post_info != ''){
		foreach($post_info as $info => $info_value){
			$info_value_split = explode(':', $info_value, 2);
			if(!isset($info_value_split[1])){
				$info_value_split = explode(' ', $info_value, 2);
			}
			if(isset($info_value_split[1])){
				$post_info_html .= '<li><span>'.$info_value_split[0].':</span><p>'.$info_value_split[1].'</p></li>';
			}
		}
	}

	// Generate the portfolio item HTML
	$portfolio_html = '<div class="gallery flexslider"><ul class="slides">'.$image_list.'</ul></div>';
	$portfolio_html .= '<div class="portfolio-meta col_9">';
	$portfolio_html .= '<h1><a href="'.$post_link.'" class="portfolio-title">'.$post_title.'</a></h1>';
	if(has_excerpt($post_ID)){
		$portfolio_html .= '<h2 class="portfolio-excerpt">'.$post_excerpt.'</h2>';
	}
	$portfolio_html .= '<ul class="portfolio-info">'.$post_info_html.'</ul>';
	$portfolio_html .= '<div class="portfolio-content">'.$post_content.'</div>';
	$portfolio_html .= '<a href="'.$post_link.'" class="more-link">';
	$portfolio_html .= __('Read more', 'handbook');
	$portfolio_html .= ' <span>&rsaquo;</span></a>';
	$portfolio_html .= '</div><div class="clear"></div>';

	// Create a new document title
	$document_title = get_post_meta(get_the_ID(), '_yoast_wpseo_title', true);
	if($document_title == ''){
		$document_title = get_the_title() . ' | ' . get_bloginfo('name');
	}

	// Get related portfolio items
	$portfolio_related_id = array();
	$portfolio_related_url = array();
	$portfolio_related_thumbnail = array();
	
	if($post_short == "false"){

		$portfolio_related_id = handbook_get_portfolio_related($post_ID);

		foreach($portfolio_related_id as $portfolio_index => $portfolio_id){
			$portfolio_related_url[$portfolio_index] = get_permalink($portfolio_id);
			$portfolio_related_thumbnail[$portfolio_index] = wp_get_attachment_image_src( get_post_thumbnail_id($portfolio_id), 'portfolio-thumbnail' );
			$portfolio_related_thumbnail[$portfolio_index] = $portfolio_related_thumbnail[$portfolio_index]['0'];
		}
	}

	// Echo the data in JSON
	echo json_encode(array(
		'prev_id'           => $prev_post_id,
		'next_id'           => $next_post_id,
		'portfolio_html'    => $portfolio_html,
		'post_link'         => $post_link,
		'related_id'        => $portfolio_related_id,
		'related_url'       => $portfolio_related_url,
		'related_thumbnail' => $portfolio_related_thumbnail,
		'document_title'    => $document_title,
	));

	die();
}

function add_video_wmode_transparent( $html, $url, $attr ){
	if ( strpos ( $html, 'feature=oembed' ) !== false ) {
		return str_replace( 'feature=oembed', 'feature=oembed&wmode=opaque', $html );
	} elseif ( strpos( $html, "<embed" ) !== false ){
		return str_replace( '<embed', '<embed wmode="transparent" ', $html );
	} else {
		return $html;
	}
}

add_filter( 'oembed_result', 'add_video_wmode_transparent', 10, 3);

///////////////////////////////////////////////////////////////////////////////////////
// AJAX Portfolio - get the related items
///////////////////////////////////////////////////////////////////////////////////////

function handbook_get_portfolio_related($post_id){

	$i = 0;
	$related_items = array();
	$exclude_items = array();
	$tags = array();

	$terms = get_the_terms( $post_id, 'portfolio_tag');

	if(empty($terms) == false){

		foreach($terms as $tag){
			$tags[] = $tag->slug;
		}

		$related = new WP_Query(
			array( 	'post_type' 		=> 'portfolio', 
					'post__not_in'		=> array($post_id),
					'posts_per_page' 	=> 4,
					'tax_query' => array(
						array(
							'taxonomy' => 'portfolio_tag',
							'field' => 'slug',
							'terms' => $tags
						)
					)
			)
		);

		while ( $related->have_posts() ) : $related->the_post(); 
			$related_items[] = get_the_ID();
			$i++;
		endwhile;

		wp_reset_query(); 
		
	}

	if($i < 4){
		$exclude_items = $related_items;
		$exclude_items[] = $post_id;
		$related = new WP_Query( 
			array( 	'post_type' 		=> 'portfolio',
					'post__not_in'		=> $exclude_items,
					'posts_per_page' 	=> 4 - $i,
			)
		);

		while ( $related->have_posts() ) : $related->the_post();
			$related_items[] = get_the_ID();
		endwhile;
	}

	return array_slice($related_items, 0, 4);
}

///////////////////////////////////////////////////////////////////////////////////////
//	Markup for password protected posts
///////////////////////////////////////////////////////////////////////////////////////

function handbook_password_posts(){
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );

	$form_html  = '<div class="password-post-wrapper">';
	$form_html .= '<p>' . __( 'To view this protected post, enter the password below:', 'handbook' ) . '</p>';
	$form_html .= '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">';
	$form_html .=     '<input name="post_password" class="password-field" placeholder="'. __( 'Password...', 'handbook'  ) .'" id="' . $label . '" type="password" size="20" maxlength="20" />';
	$form_html .=     '<input type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" />';
	$form_html .= '</form>';
	$form_html .= '</div>';

	return $form_html;
}

add_filter( 'the_password_form', 'handbook_password_posts' );


///////////////////////////////////////////////////////////////////////////////////////
// Add Custom Background Metabox
///////////////////////////////////////////////////////////////////////////////////////

add_action( 'add_meta_boxes', 'custom_background_image_add' );

function custom_background_image_add(){
	add_meta_box( '_custom-background', __('Background Image', 'handbook'), 'custom_background_image', 'post', 'side' );
	add_meta_box( '_custom-background', __('Background Image', 'handbook'), 'custom_background_image', 'page', 'side' );
	if (post_type_exists('portfolio')){
		add_meta_box( '_custom-background', __('Background Image', 'handbook'), 'custom_background_image', 'portfolio', 'side' );
	}

	wp_register_script( 'script-repeatable', get_template_directory_uri() . '/theme-options/script/script-repeatable.js' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'script-repeatable' );
}

function custom_background_image( $post ){

	if( !is_object($post) ) return;

	$selected_image = get_post_meta( $post->ID, '_custom-background', true );

	if(isset($selected_image) == false){
		$selected_image = '';
	} ?>

	<img src="<?php echo $selected_image; ?>" alt="" style="width: 100%" class="preview-background">
	<input type="hidden" name="custom-background-url" id="custom-background-url" value="<?php echo esc_attr($selected_image); ?>">
	<p class="hide-if-no-js">
		<?php if($selected_image == ''){ ?>
			<a href="" data-uploader_title="<?php echo __('Choose Background', 'handbook'); ?>" data-uploader_button_text="<?php echo __('Set As Background', 'handbook'); ?>" class="upload-image"><?php echo __('Select Background Image', 'handbook'); ?></a>
			<a href="" class="remove-background-image" style="display:none;"><?php echo __('Remove Background Image', 'handbook'); ?></a>
		<?php } else { ?>
			<a href="" style="display:none;" data-uploader_title="<?php echo __('Choose Background', 'handbook'); ?>" data-uploader_button_text="<?php echo __('Set As Background', 'handbook'); ?>" class="upload-image"><?php echo __('Select Background Image', 'handbook'); ?></a>
			<a href="" class="remove-background-image"><?php echo __('Remove Background Image', 'handbook'); ?></a>
		<?php } ?>
	</p>

<?php }



function custom_background_image_save($post_id){

	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if( !current_user_can( 'edit_post', $post_id ) ) return;

	if( isset( $_POST['custom-background-url'] )){
		update_post_meta( $post_id, '_custom-background', esc_attr( $_POST['custom-background-url'] ) );  
	}

}

add_action( 'save_post', 'custom_background_image_save' );


///////////////////////////////////////////////////////////////////////////////////////
// Pagination
///////////////////////////////////////////////////////////////////////////////////////

function handbook_pagination($pages = '', $range = 2)
{
	$showitems = ($range * 2)+1;

	global $paged;
	if(empty($paged)) $paged = 1;

	if($pages == ''){
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages){
			$pages = 1;
		}
	}

	if(1 != $pages){
		echo "<ul id='pagination'>";
		if($paged > 1) echo "<li class='prev'><a href='".get_pagenum_link($paged - 1)."'></a></li>";

		for ($i=1; $i <= $pages; $i++){
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
				echo ($paged == $i)? "<li><a class='selected'>".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a></li>";
			}
		}

		if ($paged < $pages) echo "<li class='next'><a href='".get_pagenum_link($paged + 1)."'></a></li>";
		echo '<li class="total">'.$paged.'/'.$pages.'</li></ul>';
	}
}


///////////////////////////////////////////////////////////////////////////////////////
//	Add social fields for the user profile
///////////////////////////////////////////////////////////////////////////////////////

function handbook_profile_fields($profile_fields){
	$profile_fields['twitter']      = __('Twitter URL', 'handbook');
	$profile_fields['facebook']     = __('Facebook URL', 'handbook');
	$profile_fields['gplus']        = __('Google+ URL', 'handbook');
	$profile_fields['linkedin']     = __('LinkedIn URL', 'handbook');
	$profile_fields['dribbble']     = __('Dribbble URL', 'handbook');
	$profile_fields['pinterest']    = __('Pinterest URL', 'handbook');
	$profile_fields['instagram']    = __('Instagram URL', 'handbook');

	return $profile_fields;
}

add_filter('user_contactmethods', 'handbook_profile_fields');

///////////////////////////////////////////////////////////////////////////////////////
//	Wrap oEmbed videos to enable better mobile experience
///////////////////////////////////////////////////////////////////////////////////////

function handbook_oembed_wrap($html, $url, $attr, $post_id){
	$video_url = parse_url($url);
	$host = strtolower($video_url['host']);
	$video_hosts = array('youtube.com', 'www.youtube.com', 'vimeo.com', 'www.vimeo.com', 'youtu.be', 'dailymotion.com', 'www.dailymotion.com', 'blip.tv', 'www.blip.tv');
	if (in_array($host, $video_hosts)){
		return '<div class="oembed-wrap">' . $html . '</div>';
	} else {
		return $html;
	}
}

add_filter('embed_oembed_html', 'handbook_oembed_wrap', 99, 4);


///////////////////////////////////////////////////////////////////////////////////////
//	Fix empty title on home page
///////////////////////////////////////////////////////////////////////////////////////

function natko_title_home($title, $sep){
	$site_description = '';
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description != '' && (is_home() || is_front_page())){
		$title = get_bloginfo( 'name', 'display' );
		$title = "$title $sep $site_description";
	} else {
		$blogname = get_bloginfo( 'name', 'display' );
		$title = "$title $blogname";
	}

	return $title;
}

add_filter('wp_title', 'natko_title_home', 10, 2);


///////////////////////////////////////////////////////////////////////////////////////
//	Prevent the plugin update/overwrite from wordpress.org
///////////////////////////////////////////////////////////////////////////////////////

function handbook_disable_plugin_updates( $value ) {

	$plugins = array(
		// Disable Approve Quickly updates
		'approve-quickly/approve-quickly.php',
		// Disable Love Post updates
		'love-post/love-post.php',
		// Disable Natko Shortcodes updates
		'natko-shortcodes/natko-shortcodes.php',
		// Disable Portfolio Plus updates
		'portfolio-plus/portfolio-plus.php',
		// Disable Social Profiles Widget Plus updates
		'social-profile-widget-plus/social-profile-widget-plus.php',
		// Disable Testimonials Plus updates
		'testimonials-plus/testimonials-plus.php',
		// Disable Quick Publish Plus updates
		'quick-publish-plus/quick-publish-plus.php',
	);

	foreach($plugins as $plugin){
		if(isset($value->response[$plugin])){
			unset( $value->response[$plugin] );
		}
	}

	return $value;
}

add_filter( 'site_transient_update_plugins', 'handbook_disable_plugin_updates' );


///////////////////////////////////////////////////////////////////////////////////////
//	Plugin Activation
///////////////////////////////////////////////////////////////////////////////////////

if(is_admin()){

	// Include the TGM_Plugin_Activation class.
	require_once('theme-options/tgm/class-tgm-plugin-activation.php');

	//Require and recommend plugins
	add_action( 'tgmpa_register', 'handbook_register_required_plugins' ); 

	function handbook_register_required_plugins() {

		$plugins = array(

			array(
				'name'     => 'Approve Quickly',
				'slug'     => 'approve-quickly',
				'source'   => get_stylesheet_directory() . '/theme-options/tgm/plugins/approve-quickly.zip',
				'required' => false,
			),

			array(
				'name'     => 'Love Post',
				'slug'     => 'love-post',
				'source'   => get_stylesheet_directory() . '/theme-options/tgm/plugins/love-post.zip',
				'required' => false,
			),

			array(
				'name'     => 'Natko Shortcodes',
				'slug'     => 'natko-shortcodes',
				'source'   => get_stylesheet_directory() . '/theme-options/tgm/plugins/natko-shortcodes.zip',
				'required' => false,
			),

			array(
				'name'     => 'Portfolio Plus',
				'slug'     => 'portfolio-plus',
				'source'   => get_stylesheet_directory() . '/theme-options/tgm/plugins/portfolio-plus.zip',
				'required' => false,
			),

			array(
				'name'     => 'Quick Publish Plus',
				'slug'     => 'quick-publish-plus',
				'source'   => get_stylesheet_directory() . '/theme-options/tgm/plugins/quick-publish-plus.zip',
				'required' => false,
			),

			array(
				'name'     => 'Social Profiles Widget Plus',
				'slug'     => 'social-profiles-widget-plus',
				'source'   => get_stylesheet_directory() . '/theme-options/tgm/plugins/social-profiles-widget-plus.zip',
				'required' => false,
			),
			

			array(
				'name'     => 'Testimonials Plus',
				'slug'     => 'testimonials-plus',
				'source'   => get_stylesheet_directory() . '/theme-options/tgm/plugins/testimonials-plus.zip',
				'required' => false,
			),


			array(
				'name' 		=> 'Contact Form 7',
				'slug' 		=> 'contact-form-7',
				'required' 	=> false,
			),

		);

		$config = array(
			'domain'       		=> 'handbook',
			'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
			'parent_menu_slug' 	=> 'themes.php', 				// Default parent menu slug
			'parent_url_slug' 	=> 'themes.php', 				// Default parent URL slug
			'menu'         		=> 'install-required-plugins', 	// Menu slug
			'has_notices'      	=> true,                       	// Show admin notices or not
			'is_automatic'    	=> false,					   	// Automatically activate plugins after installation or not
			'message' 			=> '',							// Message to output right before the plugins table
			'strings'      		=> array(
				'page_title'                       			=> __( 'Install Required Plugins', 'handbook' ),
				'menu_title'                       			=> __( 'Install Plugins', 'handbook' ),
				'installing'                       			=> __( 'Installing Plugin: %s', 'handbook' ), // %1$s = plugin name
				'oops'                             			=> __( 'Something went wrong with the plugin API.', 'handbook' ),
				'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
				'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
				'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
				'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
				'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
				'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
				'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
				'return'                           			=> __( 'Return to Required Plugins Installer', 'handbook' ),
				'plugin_activated'                 			=> __( 'Plugin activated successfully.', 'handbook' ),
				'complete' 									=> __( 'All plugins installed and activated successfully. %s', 'handbook' ), // %1$s = dashboard link
				'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
			)
		);

		tgmpa( $plugins, $config );

	}

}

?>