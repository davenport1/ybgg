<?php
/*
Template Name: Contact Page
*/
?>

<?php get_header(); ?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

			<article id="page-<?php the_ID(); ?>" data-id="<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if(have_posts()) : while(have_posts()) : the_post();

					global $post;

					$content = get_the_content();
					$pattern = get_shortcode_regex();

					preg_match_all('/'.$pattern.'/s', $post->post_content, $matches);

					if( $matches ){
						$contact_shortcode_index = '';
						$map_shortcode_index = '';
						$shortcodes = $matches[2];

						foreach($shortcodes as $index=>$shortcode){
							if($shortcode == 'contact-form-7' && $contact_shortcode_index == ''){
								$contact_shortcode_index = $index;
							}
							if($shortcode == 'google_map' && $map_shortcode_index == ''){
								$map_shortcode_index = $index;
							}
						}

						if (is_array($matches) && $contact_shortcode_index != '') {

							$contact_shortcode = $matches[0][$contact_shortcode_index];
							$content = str_replace($contact_shortcode, '', $content);

							$map_shortcode = $matches[0][$map_shortcode_index];
							$content = str_replace($map_shortcode, '', $content);

							echo do_shortcode($map_shortcode);
							echo do_shortcode($contact_shortcode);
						}

					}

					 ?>

					<div class="contact-page-description">
						<?php 
							$content = apply_filters ('the_content', $content); 
							echo $content; 
						?>
					</div>

				<?php endwhile; endif; ?>

			</article>

		</div> <!-- end content -->
	</div> <!-- end container -->

<?php get_footer(); ?>