<?php
/**
 * The Template for displaying archives.
 *
 */

get_header();

$options = get_option('handbook_theme_options');

global $withcomments;
$withcomments = true;

?>

	<!-- primary -->
	<div class="primary-content">

		<!-- content -->
		<div class="content">

			<div class="page-info">
				<?php if (is_category()){ ?>
					<h1><?php single_cat_title(); ?></h1>
				<?php } elseif (is_tag()){ ?>
					<h1><?php single_tag_title(__('Viewing all items for tag ', 'persona')); ?></h1>
				<?php } elseif (is_day()){ ?>
					<h1><?php echo __('All items for ', 'persona'); the_time(get_option('date_format')); ?></h1>
				<?php } elseif (is_month()){ ?>
					<h1><?php echo __('All items for ', 'persona'); single_month_title(' '); ?></h1>
				<?php } elseif (is_year()){ ?>
					<h1><?php echo __('All items for ', 'persona'); the_time('Y'); ?></h1>
				<?php } elseif (is_date()){ ?>
					<h1><?php the_time(get_option('date_format')); ?></h1>
				<?php } ?>
			</div>

			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

			<?php endwhile; ?>

			<?php else : ?>

				<p><?php echo __('No results found.', 'handbook') ?></p>
				
			<?php endif; ?>

			<?php if (function_exists('persona_pagination')) {
				persona_pagination('', 3);
			} else {
				posts_nav_link();
			} ?>

		</div> <!-- end content -->
	</div> <!-- end container -->

	<?php //if($options['show_sidebar'] == true){ ?>

		<?php get_sidebar(); ?>

	<?php //} ?>

<?php get_footer(); ?>



