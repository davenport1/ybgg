<?php
/*
Template Name: Front Page Grid + Filter
*/

	get_header();

	$options = get_option('handbook_theme_options');
	$has_thumbnail = false;
	$show_slider = false;
	$total_posts = 0;
	$posts_per_page = '-1';
	$posts_per_page = intval($options['portfolio_number']);

	if(is_front_page() && !is_paged() && $options['show_slider'] == true){
		$show_slider = true;
	}

	// Get Portfolio Tags/Categories
	if (post_type_exists('portfolio')){
		$portfolio = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => $posts_per_page ) ); $i = 0; $final = array(); 
		while ( $portfolio->have_posts() ) : $portfolio->the_post();
			if(has_post_thumbnail()){
				$has_thumbnail = true;
				$total_posts++;
				$all_tags = get_the_terms( get_the_ID(), 'portfolio_tag');

				if($all_tags){
					foreach($all_tags as $tag){
						$tag_single = $tag->name;
						$final[$i] = $tag_single;
						$i++;
					}
				}
			}
		endwhile;

		$tags_unique = array_count_values($final);
		wp_reset_query();
	}

?>

	

	<?php if($show_slider == true){
		get_template_part( 'slider', 'template' );
	} ?>

	<div class="portfolio-details hidden">
		<div class="portfolio-nav portfolio-archive">
			<a href="" class="close-portfolio">X</a>
			<a href="" class="prev-portfolio ajax-enabled" data-id="">&lt;</a>
			<a href="" class="next-portfolio ajax-enabled" data-id="">&gt;</a>
		</div>
		<span class="dash"></span>
		<div class="portfolio-details-content fadein"></div>
	</div>

	<div class="clear"></div>

	<!-- portfolio-filter-->
	<div class="portfolio-filter">
		
		<?php if($has_thumbnail == true){ ?>
			<ul class="portfolio-keywords">
				<li class="active"><a href="" class="active" data-filter="*"><?php echo $options['portfolio_view_all']; ?></a><span><?php echo $total_posts; ?></span></li>
				<?php if($tags_unique){
					foreach($tags_unique as $tag_name => $count){
						$for_class = str_replace(' ', '_', strtolower($tag_name));
						echo '<li><a href="#" data-filter=".'.$for_class.'">'.$tag_name.'</a><span>'.$count.'</span></li>';
					}
				} ?>
			</ul>
			<div class="clear"></div>
			<ul id="items" class="portfolio-list grid-layout filter-on">
				<?php while ( $portfolio->have_posts() ) : $portfolio->the_post();
					if(has_post_thumbnail()){ 
						$portfolio_tags = get_the_terms( get_the_ID(), 'portfolio_tag'); ?>
						<li class="<?php if(is_array($portfolio_tags)){ foreach($portfolio_tags as $tag){ $tag_single = str_replace(' ', '_', strtolower($tag->name)); echo $tag_single .' '; } } ?>">

							<a href="<?php the_permalink(); ?>" data-id="<?php the_ID(); ?>">
								<span class="view-more-icon"></span>
								<?php the_post_thumbnail('portfolio-thumbnail-big'); ?>
							</a>
							<h1><a href="<?php the_permalink(); ?>" data-id="<?php the_ID(); ?>"><?php the_title(); ?></a></h1>
							<?php the_excerpt(); ?>
							<div class="splitter"></div>
						</li>

					<?php }
				endwhile; ?>
			</ul>
		<?php } else if($has_thumbnail == false && current_user_can('edit_pages')){
			if(function_exists('portfolio_plus_exists_check')){ ?>
				<div class="admin-portfolio-message">
					<p><?php echo __("Currently there are no portfolio items with a thumbnail, why don't you go ahead and add one?", 'handbook'); ?></p>
					<a href="<?php echo admin_url('post-new.php?post_type=portfolio'); ?>" class="more-link"><?php echo __('Add portfolio item', 'handbook'); ?></a>
				</div>
			<?php } else { ?>
				<div class="admin-portfolio-message">
					<p><?php echo __('To use the portfolio feature you need to install Portfolio Plus plugin that comes with the theme in the "plugins" folder.', 'handbook'); ?></p>
				</div>
			<?php } ?>
		<?php } ?>

		<div class="clear"></div>

	</div> <!-- end portfolio-filter -->

	<?php if(function_exists('testimonials_plus_exists_check')){ 
		get_template_part( 'testimonials', 'template' );
	} ?>

	<?php if($options['fullwidth_message'] != ''){ ?>
		<!-- fullwidth-message -->
		<div class="fullwidth-message">
			<p><?php echo $options['fullwidth_message']; ?></p>
			<?php if($options['fullwidth_message_link'] != ''){
				echo '<a href="'. $options['fullwidth_message_link'] .'">' . $options['fullwidth_message_button'] . '</a>';
			} ?>
		</div> <!-- end fullwidth-message -->
	<?php } ?>

<?php get_footer(); ?>