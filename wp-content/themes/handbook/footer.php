
				<?php $options = get_option('handbook_theme_options'); ?>

				<div class="clear"></div>

				<div id="footer">

					<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<div class="widget-section first">
							<?php dynamic_sidebar( 'footer-1' ); ?>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<div class="widget-section">
							<?php dynamic_sidebar( 'footer-2' ); ?>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<div class="widget-section">
							<?php dynamic_sidebar( 'footer-3' ); ?>
						</div>
					<?php endif; ?>

					<div class="clear"></div>

					<div class="copyright">

						<p><?php echo $options['footer_text']; ?></p>

						<?php 

							$nav_args = array(
								'theme_location'  => 'footer',
								'menu'            => '',
								'container'       => false,
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => false,
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => '',
								'before'          => '',
								'after'           => '<p>&bull;</p>',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="footer-menu">%3$s</ul>',
								'depth'           => 1,
								'walker'          => ''
							);

							wp_nav_menu( $nav_args ); 
						?>

					</div>

					<div class="clear"></div>

				</div> <!-- end footer -->
			
				<div class="clear"></div>
				
			</div> <!-- end container -->

		</div> <!-- end wrapper -->

		<?php wp_footer(); ?>

	</body>
</html>