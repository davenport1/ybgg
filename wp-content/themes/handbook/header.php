<!DOCTYPE HTML>

<html <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>" />

		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, width=device-width">



		<title><?php wp_title( '|', true, 'right' ); ?></title>



		<!--[if lt IE 9]>

			<script src="<?php echo get_template_directory_uri(); ?>/script/html5shiv.js"></script>

		<![endif]-->



		<?php $options = get_option('handbook_theme_options');



		if(isset($options['title_text']) && $options['title_text'] != ''){

			$title = $options['title_text'];

		} else {

			$title = get_bloginfo('name');

		}



		if(isset($options['description_text']) && $options['description_text'] != ''){

			$description = $options['description_text'];

		} else {

			$description = get_bloginfo('description');

		}



		if(isset($options['logo']) && $options['logo'] != ''){

			$logo = $options['logo'];

		}



		?>
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>


		<?php wp_head(); ?>



	</head>



	<body <?php body_class(); ?> >

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TJNS78"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TJNS78');</script>
<!-- End Google Tag Manager -->

		<?php if($options['show_sidebar'] == true){ ?>



			<form action="#" method="post">

				<?php wp_nonce_field( 'save-sidebar-widgets', '_wpnonce_widgets', false ); ?>

			</form>



		<?php } ?>



		<a href="" class="menu-toggle show"><?php echo __('Menu', 'handbook'); ?></a>



		<!-- wrapper -->

		<div class="wrapper">



			<!-- container -->

			<div class="container">

			

				<!-- header -->

				<div id="header">



					<div class="site-title">

						<?php if(isset($logo)){ ?>

							<a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo $logo; ?>" alt="<?php echo $title; ?>"></a>

						<?php } ?>

						<div>

							<a href="<?php echo home_url(); ?>"><h1><?php echo $title; ?></h1></a>

							<p><?php echo $description; ?></p>

						</div>

					</div>



					<?php 



						$items_wrap = '<nav><ul id="nav-menu">%3$s</ul></nav>';



						$nav_args = array(

							'theme_location'  => 'header',

							'menu'            => '',

							'container'       => false,

							'container_class' => '',

							'container_id'    => '',

							'menu_class'      => false,

							'menu_id'         => '',

							'echo'            => true,

							'fallback_cb'     => '',

							'before'          => '',

							'after'           => '',

							'link_before'     => '',

							'link_after'      => '',

							'items_wrap'      => $items_wrap,

							'depth'           => 0,

							'walker'          => ''

						);



						wp_nav_menu( $nav_args );

					?>



				</div>

				<!-- end header -->